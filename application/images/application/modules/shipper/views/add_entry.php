<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Card</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('odv/update') : site_url('odv/insert'); ?>">
                        <input type="hidden" name="odvid" value="<?php echo isset($odv['id']) ? $odv['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1" for="client">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <h4>John Doe</h4>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="headline">Job Card No.<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="2" value="" name="" id="headline">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="headline">Headline<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="3" value="" name="" id="headline">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="protocol">Protocol No.<span class="required">*</span></label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="5" value="" name=""  id="protocol">
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1" for="version">Version No.<span class="required">*</span></label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="7" value="" name="" id="version">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="date1">Version Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> 
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="po">P.O No.<span class="required">*</span></label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="9" value="" name="" id="po">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="date2">P.O Informed Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date2" class="date-picker form-control col-md-7" required="required" type="text" tabindex="10" readonly> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1"  for="date3">Delivery Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date3" class="date-picker form-control col-md-7" required="required" type="text" tabindex="11" readonly> 
                                        </div>
                                    
                                        <label class="control-label col-md-1 col-sm-1" for="notice">Old Job No.<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="13" id="notice">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="remarks">Remarks<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <textarea id="remarks" class="form-control" tabindex="16"></textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 list-table">
                                <table class="table table-bordered">
                                    <tr>
                                        <th colspan="2">Language</th>
                                        <th>Unit</th>
                                        <th>Writer Code</th>
                                        <th>Employee Code</th>
                                        <th>PD</th>
                                        <th>CR</th>
                                        <th>C/NC</th>
                                        <th>DV</th>
                                        <th>F/QC</th>
                                        <th>Sent Date</th>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="4">Hindi</td>

                                        <td>T</td>
                                        <td><input type="text" class="form-control" tabindex="17"></td>
                                        <td><input type="text" class="form-control" tabindex="18"></td>
                                        <td><input type="text" class="form-control" tabindex="19"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="23"></td>
                                        <td><input type="text" class="form-control" tabindex="24"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>V</td>
                                        <td><input type="text" class="form-control" tabindex="26"></td>
                                        <td><input type="text" class="form-control" tabindex="27"></td>
                                        <td><input type="text" class="form-control" tabindex="28"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="32"></td>
                                        <td><input type="text" class="form-control" tabindex="33"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>BT</td>
                                        <td><input type="text" class="form-control" tabindex="35"></td>
                                        <td><input type="text" class="form-control" tabindex="36"></td>
                                        <td><input type="text" class="form-control" tabindex="37"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="41"></td>
                                        <td><input type="text" class="form-control" tabindex="42"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>BTV</td>
                                        <td><input type="text" class="form-control" tabindex="44"></td>
                                        <td><input type="text" class="form-control" tabindex="45"></td>
                                        <td><input type="text" class="form-control" tabindex="46"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="50"></td>
                                        <td><input type="text" class="form-control" tabindex="51"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="4">English</td>

                                        <td>T</td>
                                        <td><input type="text" class="form-control" tabindex="53"></td>
                                        <td><input type="text" class="form-control" tabindex="54"></td>
                                        <td><input type="text" class="form-control" tabindex="55"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="59"></td>
                                        <td><input type="text" class="form-control" tabindex="60"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>V</td>
                                        <td><input type="text" class="form-control" tabindex="62"></td>
                                        <td><input type="text" class="form-control" tabindex="63"></td>
                                        <td><input type="text" class="form-control" tabindex="64"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="68"></td>
                                        <td><input type="text" class="form-control" tabindex="69"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>BT</td>
                                        <td><input type="text" class="form-control" tabindex="71"></td>
                                        <td><input type="text" class="form-control" tabindex="72"></td>
                                        <td><input type="text" class="form-control" tabindex="73"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="77"></td>
                                        <td><input type="text" class="form-control" tabindex="78"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <tr>
                                        <td>BTV</td>
                                        <td><input type="text" class="form-control" tabindex="80"></td>
                                        <td><input type="text" class="form-control" tabindex="81"></td>
                                        <td><input type="text" class="form-control" tabindex="82"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on">
                                                <option value="NULL">Select</option>
                                                <option value="NULL">C</option>
                                                <option value="NULL">NC</option>
                                            </select></td>
                                        <td><input type="text" class="form-control" tabindex="86"></td>
                                        <td><input type="text" class="form-control" tabindex="87"></td>
                                        <td><input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" readonly> </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn pull-right" tabindex="87">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    $(document).ready(function () {
        $('.date-picker').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
        });


    });
</script>
