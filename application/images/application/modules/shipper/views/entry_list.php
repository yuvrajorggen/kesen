<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job Card</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('odv/odv_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-1 col-sm-1">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Handled By<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Writer's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Client's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Conatct Person Name<span class="required">*</span></label>
                            <label class="control-label col-md-1 col-sm-1">Protocol No.<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-1 col-sm-1">
                                <input type="text" class="form-control" tabindex="1" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2">                  
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" readonly> 
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="3">
                                    <option>Select Handled By</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="4">
                                    <option>Select Writer's Name</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="4">
                                    <option>Select Client's Name</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="4">
                                    <option>Select Conatct Person Name</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-1">
                                <input type="text" class="form-control" tabindex="1" autofocus="on">
                            </div>
                            <br />
                            <br />
                            <div class="pull-right">
                                <a tabindex="6" class="btn btn-default submit-btn" title="Search Job Card" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                                <a tabindex="7" class="btn btn-default view-btn" title="View All Job Card" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                                <a tabindex="8" class="btn btn-default add-btn"  title="Create Job Card" href="<?php echo site_url('shipper/entry'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Create Job Card</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table_content">
                    <h3>Job Card List</h3>
                    <div class="total">
                        <span>Total Records : 12</span>
                        <span>Total Display : 12</span>
                    </div>
                    <?php echo $this->session->flashdata('message'); ?>
                    <?php echo $this->pagination->create_links(); ?>
                    <div class="list-table">
                        <table class="table table-bordered view-table">
                            <thead>
                                <tr class="table_heading">
                                    <th>Job Card No.</th>
                                    <th>Protocol No.</th>
                                    <th>Client Name</th>
                                    <th>Description</th>
                                    <th>Ordered By</th>
                                    <th>Handled By</th>
                                    <th>Bill No.</th>
                                    <th>Bill Date</th>
                                    <th>Unit</th>
                                    <th>Amount</th>
                                    <th width="53%">Actions</th>
                                </tr>
                            </thead>
                            <?php
                            foreach ($list as $key => $value) {
                                ?>
                                <tbody>
                                    <tr>
                                        <td>1234</td>
                                        <td>998</td>
                                        <td>John Doe</td>
                                        <td>Lorem.</td>
                                        <td>John Doe</td>
                                        <td>Adam</td>
                                        <td>123456</td>
                                        <td>09/06/2017</td>
                                        <td>002</td>
                                        <td>5200</td>
                                        <td>
                                            <a title="View Job Card" href=""><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                            <a title="Update Job Card" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                            <a title="Delete Job Card" href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                                            <a title="Print Job Card" href="<?php echo site_url('shipper/job_card'); ?>"><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>
                                            <a title="SMS Job Card" href=""><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;SMS&nbsp;</a>
                                            <a title="Send Confirmation Letter" href=""><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Send Confirmation Letter&nbsp;</a>
                                            <a title="Send Follow Up Letter" href=""><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Send Follow Up Letter&nbsp;</a>
                                            <a title="View Payment" href="<?php echo site_url('shipper/payment'); ?>"><i class="fa fa-inr fa-lg" aria-hidden="true"></i>&nbsp;Payment</a>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            //"singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });
</script>
<!-- /Select2 -->