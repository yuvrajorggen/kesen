<?php

class Shipper extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['odvlist'] = $this->gm->get_data_list('odv', array('status' => 1), array(), array('id' => 'desc'), 'code,name,id', 0, array());
        $view = 'add_entry';
        $this->_display($view, $data);
    }
    
    function payment() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['odvlist'] = $this->gm->get_data_list('odv', array('status' => 1), array(), array('id' => 'desc'), 'code,name,id', 0, array());
        $view = 'add_entry_payment';
        $this->_display($view, $data);
    }
    
    function insert() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data = $this->input->post('shipper');
        $sessiondata = $this->session->userdata('user');
        $multiplecity = $this->input->post('shipper_city');

        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');

        $shipper_id = $this->gm->insert('shipper', $data);
        unset($data);
        $data['shipper_id'] = $shipper_id;
        if (isset($multiplecity) && count($multiplecity) > 0) {
            foreach ($multiplecity as $key => $value) {
                $data['city'] = $value;
                //print_r($data);
                $data['created_by'] = $sessiondata['id'];
                $data['created_date'] = date('Y-m-d H:i:s');
                $this->gm->insert('rel_shipper', $data);
            }
        }
        redirect(site_url('shipper/entry_list'));
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $this->load->model('Global_model', 'gm');
        //$data['list'] = $this->gm->get_data_list('shipper', array(), array(), array('id' => 'desc'), '*',30);
        //pagination (start)

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * 20;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $count = array();
        if ($_GET) {

            $name = $this->input->get('name');
            $name = strtoupper($name);

            $query = "SELECT * FROM `shipper` WHERE status='1' ";

            if ($name != '') {
                $query .= " AND ( name like '%" . $name . "%') ";
            }

            

            $query_exec = $this->db->query($query);
            $result = $query_exec->result_array();
            if (isset($result) && count($result) > 0) {
                $cquery = "SELECT count(id) as id FROM `shipper` WHERE status='1'";

                $cquery_exec = $this->db->query($cquery);
                $count = $cquery_exec->row_array();
            }
        } else {
            $query = "SELECT * FROM `shipper` WHERE status='1' ORDER BY `id`  DESC limit 20 offset " . $offset . "";
            $query_exec = $this->db->query($query);
            $result = $query_exec->result_array();

            $cquery = "SELECT count(id) as id FROM `shipper` WHERE status='1' ";
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        }
        $data['list'] = $result;
        $data['total'] =isset($count['id']) ? $count['id'] : 0;
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('shipper/entry_list');
        $config['total_rows'] = isset($count['id']) ? $count['id'] : 0;

        $config['per_page'] = 20;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';
        //pagination end

        $this->_display($view, $data);
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function update() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $sessiondata = $this->session->userdata('user');
        $this->load->model('Global_model', 'gm');

        $id = $this->input->post('shipperid');
        $data = $this->input->post('shipper');
        $multiplecity = $this->input->post('shipper_city');
        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');

        $query = 'delete from rel_shipper  where shipper_id=' . $id;
        $query_exec = $this->db->query($query);

        foreach ($multiplecity as $key => $value) {
            $city['shipper_id'] = $id;
            $city['modified_by'] = $sessiondata['id'];
            $city['modified_date'] = date('Y-m-d H:i:s');
            $city['city'] = $value;

            $this->gm->insert('rel_shipper', $city);
        }

        $message = '<b>' . $data['name'] . ' ' . 'updated successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("shipper/entry_list");
    }

    function edit($id) {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['odvlist'] = $this->gm->get_data_list('odv', array('status' => 1), array(), array('id' => 'desc'), 'code,name,id', 0, array());
        $data['shipper'] = $this->gm->get_selected_record('shipper', '*', $where = array('id' => $id));
        //$data['rel_shipper'] = $this->gm->get_selected_record('rel_shipper', '*', $where = array('shipper_id' => $id));
        $data['rel_shipper'] = $this->gm->get_data_list('rel_shipper', array('status' => 1, 'shipper_id' => $id), array(), array('id' => 'desc'), 'city,shipper_id,id', 0, array());
        $data['mode'] = "update";

        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function delete($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        //$shipper = $this->gm->get_selected_record('shipper', 'name', $where = array('id' => $id));
        $set = array(
            'modified_by' => $sessiondata['id'],
            'modified_date' => date('Y-m-d H:i:s'),
            'status' => 2
        );
        $this->gm->update('shipper', $set, $id, array());
        $this->gm->update('rel_shipper', $set, '', array('shipper_id' => $id));

        $message = '<b>' . 'Record deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("shipper/entry_list");
    }
    function job_card(){
        $this->load->view('job_card');
    }

}
