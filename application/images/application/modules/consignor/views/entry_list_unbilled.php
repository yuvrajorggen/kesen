<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job UnBilled</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignor/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name=""  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Job Billed" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="6" class="btn btn-default view-btn" title="View All" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Job UnBilled Details</h3>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1234</td>
                            <td>12/06/2017</td>
                            <td>
                                <a title="Update Job UnBilled" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Job UnBilled"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a></td>
                        </tr>
                    </tbody>                        
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });

</script>