<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Timeline</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignor/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name=""  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Job Billed" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="6" class="btn btn-default view-btn" title="View All" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default btn-info"  title="Print" href="<?php echo site_url('consignor/timeline'); ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Timeline List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Estimate Delivery Date</th>
                            <th>Actual Delivery Date</th>
                            <th>Difference</th>
                            <th>Client Name</th>
                            <th>Handled By</th>
                            <th>Delay Reason</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1234</td>
                            <td>12/06/2017</td>
                            <td>16/06/2017</td>
                            <td>1234</td>
                            <td>John Doe</td>
                            <td>Albert</td>
                            <td>Lorem ipsum dolor sit amet.</td>
                            <td>English</td>
                            <td>
                                <a title="Update Timeline" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Timeline"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                        </tr>
                    </tbody>                        
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });

</script>