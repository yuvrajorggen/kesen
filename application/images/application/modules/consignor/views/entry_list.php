<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job Billed</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignor/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Month<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Year<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Client Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Protocol No.<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name=""  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="3">
                                    <option value="NULL">Select Month</option>
                                    <option value="NULL">January</option>
                                    <option value="NULL">February</option>
                                    <option value="NULL">March</option>
                                    <option value="NULL">April</option>
                                    <option value="NULL">May</option>
                                    <option value="NULL">June</option>
                                    <option value="NULL">July</option>
                                    <option value="NULL">August</option>
                                    <option value="NULL">September</option>
                                    <option value="NULL">October</option>
                                    <option value="NULL">November</option>
                                    <option value="NULL">December</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="4">
                                    <option value="NULL">Select Year</option>
                                    <option value="NULL">2000</option>
                                    <option value="NULL">2001</option>
                                    <option value="NULL">2002</option>
                                    <option value="NULL">2003</option>
                                    <option value="NULL">2004</option>
                                    <option value="NULL">2005</option>
                                    <option value="NULL">2006</option>
                                    <option value="NULL">2007</option>
                                    <option value="NULL">2008</option>
                                    <option value="NULL">2009</option>
                                    <option value="NULL">2010</option>
                                    <option value="NULL">2011</option>
                                    <option value="NULL">2012</option>
                                    <option value="NULL">2013</option>
                                    <option value="NULL">2014</option>
                                    <option value="NULL">2015</option>
                                    <option value="NULL">2016</option>
                                    <option value="NULL">2017</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="" autofocus="on" id="client">
                                    <option value="NULL">Select Client Name</option>
                                    <option value="NULL">abc</option>
                                    <option value="NULL">xyz</option>
                                    <option value="NULL">pqr</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="">
                            </div>
                            <br />
                            <br />
                            <div class="pull-right">
                                <a tabindex="5" class="btn btn-default submit-btn" title="Search Job Billed" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                                <a tabindex="6" class="btn btn-default view-btn" title="View All" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                                <a tabindex="7" class="btn btn-default btn-info"  title="Print" href="<?php echo site_url('consignor/job_billed'); ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Job Billed Details</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Client Name</th>
                            <th>Client Contact Person Name</th>
                            <th>Protocol No.</th>
                            <th>Accountant</th>
                            <th>Handled By</th>
                            <th>Cell No.</th>
                            <th>Landline</th>
                            <th>Date</th>
                            <th>Month</th>
                            <th>Year</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1234</td>
                            <td>John Doe</td>
                            <td>Albert</td>
                            <td>345678</td>
                            <td>June Williams</td>
                            <td>Jacob</td>
                            <td>9876543210</td>
                            <td>02228965317</td>
                            <td>12/06/2017</td>
                            <td>June</td>
                            <td>2017</td>
                            <td>
                                <a title="Update Job Billed" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Job Billed"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                        </tr>
                    </tbody>                        
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });

</script>