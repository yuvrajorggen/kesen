<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Employee</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                        <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="employee">Employee Name<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="employee">
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="role">Role<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select class="chosen-select" tabindex="2" name="" id="role">
                                        <option>Select Role</option>
                                        <option>CEO</option>
                                        <option>Admin</option>
                                        <option>Accountant</option>
                                        <option>Operations</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="email">E-mail<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="email" class="form-control" id="email" tabindex="3">
                                </div>
                                <label class = "control-label col-md-1 col-sm-1" for="contact">Contact No.<span class = "required">*</span></label>
                                <div class = "col-md-2 col-sm-2">
                                    <input type = "text" class = "form-control" tabindex="4" id="contact">
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <?php
                            if (isset($mode) && $mode == 'update') {
                                ?>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-sm-offset-2">Change Password<span class="required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <input type="checkbox" class="form-control" id="pwdupdate_chkbox" tabindex="" name="employee[check_password]" >
                                    </div>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div id="password_wrapper" class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="pwd">Password<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="password" class="form-control" id="pwd" tabindex="5" name=""  id="password">
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="confirmpwd">Password (again)<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="password" class="form-control" id="confirmpwd" tabindex="6" name="" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="">Employee Code<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="number" class="form-control" id="" tabindex="3" max="3">
                                </div>
                                <label class = "control-label col-md-1 col-sm-1" for="contact">Language<span class = "required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select multiple class="chosen-select" tabindex="7" name="" id="language">
                                        <option>Search option</option>
                                        <option>Hindi</option>
                                        <option>Marathi</option>
                                        <option>English</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2 col-sm-offset-2" for="address">Address<span class = "required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <textarea id="address" class="form-control" tabindex="5"></textarea>
                                    </div>
                                    <label class = "control-label col-md-1 col-sm-1" for="country">Country<span class = "required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <select class="chosen-select" tabindex="6" id="country">
                                            <option>Select Country</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2 col-sm-offset-2" for="country">State<span class = "required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <select class="chosen-select" tabindex="7" id="country">
                                            <option>Select State</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-1 col-sm-1" for="city">City<span class="required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <select class="chosen-select" tabindex="8" id="city">
                                            <option>Select City</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        <div class = "row">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>

