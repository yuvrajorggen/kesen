<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Employee</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('employee/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Employee's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="role">Role<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="contact">Contact No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on" id="name">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="2" name="" id="role">
                                    <option>Select Role</option>
                                    <option>Manager</option>
                                    <option>Senior Manager</option>
                                    <option>Supervisor</option>
                                    <option>Staff</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="3" id="contact">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="email" class="form-control" tabindex="4" id="email">
                            </div>
                            <!--<input id="btn-search" tabindex="4" type="submit" class="btn btn-default submit-btn" value="Search">
                            <input tabindex="4" type="reset" value="Clear" class="btn btn-default clear-btn">-->
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Employee" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="6" class="btn btn-default view-btn" title="View All Employees" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default add-btn"  title="Add Employee" href="<?php echo site_url('employee'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Employee</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Employee List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Employee Name</th>
                            <th>Role</th>
                            <th>Email</th>
                            <th>Contact No.</th>
                            <th>Employee Code</th>
                            <th>Language</th>
                            <th>Address</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($list as $key => $value) {
                        ?>
                        <tbody>
                            <tr>
                                <td>John Doe</td>
                                <td>Staff</td>
                                <td>john@gmail.com</td>
                                <td>9985321470</td>
                                <td>1234</td>
                                <td>English</td>
                                <td>Ajanta Square</td>
                                <td>india</td>
                                <td>Maharashtra</td>
                                <td>Mumbai</td>
                                <td>
                                    <a title="Update Employee" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                    <a title="Delete Employee"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                                </td>
                            </tr>  <?php
                        }
                        ?>

                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

