<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Payment</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                        <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Writer's Name<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select class="chosen-select" tabindex="1" autofocus="on" id="writer-name">
                                        <option value="NULL">Select Writer's Name</option>
                                        <option value="NULL">abc</option>
                                        <option value="NULL">xyz</option>
                                        <option value="NULL">pqr</option>
                                    </select>
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="period">Period<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="period" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name=""  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly > 
                                </div>
                            </div>
                        </div>
                        
                        <div class = "row">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            //"singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });

</script>

