<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Payment</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignee/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2" for="payment-date">Payment Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="job-no">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="writer-name">Writer Name<span class="required">*</span></label>
                            <label class="control-label col-md-1 col-sm-1">Payment<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input id="payment-date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="1" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> autofocus="on" readonly> 
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="2" id="job-no">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="3" id="writer-name">
                                    <option value="NULL">Select Employee's Name</option>
                                    <option value="NULL">abc</option>
                                    <option value="NULL">xyz</option>
                                    <option value="NULL">pqr</option>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-1">
                                <input type="radio" name="payment" id="yes" value="yes" tabindex="4" checked><label for="yes">&nbsp;Yes</label>&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="payment" id="no" value="no" tabindex="5"><label for="no">&nbsp;No</label>
                            </div>

                            <a tabindex="6" class="btn btn-default submit-btn" title="Search Payment" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="7" class="btn btn-default view-btn" title="View All Clients" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="8" class="btn btn-default add-btn" href="<?php echo site_url('consignee'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Payment</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Payment List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">                                         
                    <thead>
                        <tr class="table_heading">
                            <th>Writer Name</th>
                            <th>Period</th>
                            <th>Job No.</th>
                            <th>Language</th>
                            <th>Unit Rate</th>
                            <th>Amount</th>
                            <th>Payment Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>John Doe</td>
                            <td>12/08/2017</td>
                            <td>1234</td>
                            <td>English</td>
                            <td>12</td>
                            <td>8800</td>
                            <td>08/08/2017</td>
                            <td>
                                <a href="<?php echo site_url('consignee/payment_card'); ?>"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                <a title="Update Payment" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Payment"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                                <a href=""><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>
                                <a href=""><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;SMS&nbsp;</a>
                                <a href="<?php echo site_url('consignee/error'); ?>"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Email&nbsp;</a>
                                </td>
                        </tr>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });
</script>

