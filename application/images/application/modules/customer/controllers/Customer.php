<?php

class Customer extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function insert() {
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data = $this->input->post('customer');
        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');
        /*
         * uppercase applied on data
         */

        $data['name'] = strtoupper($data['name']);
        $data['code'] = strtoupper($data['code']);
        $data['display_name'] = strtoupper($data['display_name']);
        $data['address'] = strtoupper($data['address']);

        if (isset($data['customer_status'])) {
            if ($data['customer_status'] == 'on') {
                $data['customer_status'] = 1;

                $this->gm->insert('customer', $data);
            }
        } else {
            $data['customer_status'] = 2;  //if client disactive  
            $this->gm->insert('customer', $data);
        }


        redirect(site_url('customer/entry_list'));
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();

        //pagination (start)

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * 5;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        if ($_GET) {// created for searching from the list of data 
            $sql = "SELECT * FROM `customer` where";
            $name = $this->input->get('name');
            $code = $this->input->get('code');
            if ($code == '' && $name != '') {
                $sql = $sql . " name like '%" . $name . "%'";
            } elseif ($name == '' && $code != '') {
                $sql = $sql . " code like '%" . $code . "%'";
            } else {
                $sql = $sql . " name like '%" . $name . "%' AND code like '" . $code . "%'";
            }
            $query_exec = $this->db->query($sql);
            $result = $query_exec->result_array();
            $cquery = "SELECT count(id) as id FROM `customer` WHERE status='1' AND  id=" . $result[0][id];
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        } else {


            $query = "SELECT * FROM `customer`  WHERE status= '1' ORDER BY `id` DESC limit 5 offset " . $offset . "";
            $query_exec = $this->db->query($query);
            $result = $query_exec->result_array();


            $cquery = "SELECT count(id) as id FROM `customer`  WHERE status='1' ";
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        }

        $data['customerlist'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('customer/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = 5;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';

        //pagination end
        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');
        $id = $this->input->post('customerid');
        $data = $this->input->post('customer');
        
        $data['modified_by']=$sessiondata['id'];
        $data['modified_date']=  date('Y-m-d H:i:s');
        /*
         * uppercase applied on data
         */

        $data['name'] = strtoupper($data['name']);
        $data['code'] = strtoupper($data['code']);
        $data['display_name'] = strtoupper($data['display_name']);
        $data['address'] = strtoupper($data['address']);

        if (isset($data['customer_status'])) {
            if ($data['customer_status'] == 'on') {
                $data['customer_status'] = 1;
               // print_r($data);exit;
                $this->gm->update('customer', $data, $id);
            }
        } else {
           
            $data['customer_status'] = 2;  //if client disactive             
            $this->gm->update('customer', $data, $id);
        }
        $message = '<b>' . $data['name'] . ' ' . 'updated successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("customer/entry_list");
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['customer'] = $this->gm->get_selected_record('customer', '*', $where = array('id' => $this->uri->segment(3)));
        $data['mode'] = "update";
        $view = 'add_entry';

        $this->_display($view, $data);
    }

    function delete($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user'); 
        $customer = $this->gm->get_selected_record('customer', 'name', $where = array('id' => $id));
        
        $set=array(
            'modified_by' => $sessiondata['id'],
            'modified_date'=> date('Y-m-d H:i:s'),
            'status' => 2
        );
        $data['customer'] = $this->gm->update('customer', $set, $id);
        $message = '<b>' . $customer['name'] . ' ' . 'deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("customer/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function upload_customer() {
        $this->load->helper('url');
        $data = array();
        $this->_display('customer_upload', $data);
    }

    function customer_upload() {
        $this->load->model('Global_model', 'gm');
        $file_name = 'csv_import_' . date('Y-m-d_H-i-s', time());
        $config['upload_path'] = './media/csv/bulk_import/';
        $config['allowed_types'] = 'application/octet-stream|CSV|csv';
        $config['file_name'] = $file_name;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());

            $row = 1;
            if (($handle = fopen($data['upload_data']['full_path'], "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $num = count($data);
                    //echo '<pre>';
                    //print_r($data);
                    unset($insert);
                    if ($row != 0) {
                        $insert = array(
                            "name" => $data[0],
                            "display_name" => $data[1],
                            "code" => $data[2],
                            "address" => $data[3],
                            "phone" => $data[4],
                            "email_account" => $data[5],
                            "pan_number" => $data[6],
                            "customer_status" => $data[7]
                        );

                        $this->gm->insert('customer', $insert);
                    }
                    $row++;
                }
                fclose($handle);
            }
        }

        $this->load->helper('url');
        redirect(site_url('customer/entry_list'));
    }
    function writer_list(){
        $this->load->view('writer_list');
    }

}
