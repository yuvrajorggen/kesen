<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job Register</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('odv/odv_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2" for="job">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="date">Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 hidden">Handled By<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="writer">Writer's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="client">Client Name<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" id="job" class="form-control" tabindex="1" name="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''; ?>" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2">                  
                                <input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <select class="chosen-select" tabindex="3" id="">
                                    <option>Select Handled By</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="4" id="writer">
                                    <option>Select Writer's Name</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="5" id="client">
                                    <option>Select Client</option>
                                    <option>Agartala</option>
                                    <option>Agatti</option>
                                    <option>Agra</option>
                                    <option>Ahemdabad</option>
                                    <option>Aizawal</option>
                                    <option>Allahbad</option>
                                    <option>Amritsar</option>
                                    <option>Anand</option>
                                    <option>Aurangabad</option>
                                    <option>Bagdogra</option>
                                    <option>Banglore</option>
                                    <option>Belgaum</option>
                                </select>
                            </div>

                            <a tabindex="6" class="btn btn-default submit-btn" title="Search Job Register" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="7" class="btn btn-default view-btn" title="View All Job Register" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="8" class="btn btn-default add-btn"  title="Create Job Register" href="<?php echo site_url('odv/add'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Create Job Register</a>
                            <a tabindex="9" class="btn btn-default btn-info"  title="Print" href="<?php echo site_url('odv/job_register'); ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Job Register List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Date</th>
                            <th>Handled By</th>
                            <th>Client's Name</th>
                            <th>Writer's Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($list as $key => $value) {
                        ?>
                        <tbody>
                            <tr>
                                <td>123456</td>
                                <td>09/06/2017</td>
                                <td>John Doe</td>
                                <td>Seema Parul</td>
                                <td>John Doe</td>
                               <td>
                                    <a title="View Job Register" href=""><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                    <a title="Update Job Register" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                    <a title="Cancel Job Register" href=""><i class="fa fa-times fa-lg" aria-hidden="true"></i>&nbsp;Cancel&nbsp;</a>
                                    <a title="Print Job Register" href=""><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>
                                    <a title="SMS Job Register" href=""><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;SMS&nbsp;</a>
                                    <a title="Email Reply Letter" href=""><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Email Reply Letter&nbsp;</a>
                                    
                                </td>

                            </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });
</script>

<!-- /Select2 -->