  <div class="form">
    <h1>Tariff Entry</h1>
    <div class="row">
      <div class="col-md-22 col-sm-22 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Form Design <small>different form elements</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Origin<span class="required">*</span></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <select id="origin_select_field" 
                    class="form control select2_single" 
                    autofocus="on" tabindex="1">
                    <option>Search option</option>
                    <option>Agartala</option>
                    <option>Agatti</option>
                    <option>Agra</option>
                    <option>Ahemdabad</option>
                    <option>Aizawal</option>
                    <option>Allahbad</option>
                    <option>Amritsar</option>
                    <option>Anand</option>
                    <option>Aurangabad</option>
                    <option>Bagdogra</option>
                    <option>Banglore</option>
                    <option>Belgaum</option>
                  </select>
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Destination<span class="required">*</span></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <select id="destination_select_field"  class="form-control select2_single" tabindex="2">
                    <option>Search option</option>
                    <option>Agartala</option>
                    <option>Agatti</option>
                    <option>Agra</option>
                    <option>Ahemdabad</option>
                    <option>Aizawal</option>
                    <option>Allahbad</option>
                    <option>Amritsar</option>
                    <option>Anand</option>
                    <option>Aurangabad</option>
                    <option>Bagdogra</option>
                    <option>Banglore</option>
                    <option>Belgaum</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Airline<span class="required">*</span></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <select id="airline_select_field" class="form-control select2_single" tabindex="3">
                    <option>Search option</option>
                    <option>Agartala</option>
                    <option>Agatti</option>
                    <option>Agra</option>
                    <option>Ahemdabad</option>
                    <option>Aizawal</option>
                    <option>Allahbad</option>
                    <option>Amritsar</option>
                    <option>Anand</option>
                    <option>Aurangabad</option>
                    <option>Bagdogra</option>
                    <option>Banglore</option>
                    <option>Belgaum</option>
                  </select>
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Flight<span class="required">*</span></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <select id="flight_select_field" class="form-control select2_single" tabindex="4">
                    <option>Search option</option>
                    <option>Agartala</option>
                    <option>Agatti</option>
                    <option>Agra</option>
                    <option>Ahemdabad</option>
                    <option>Aizawal</option>
                    <option>Allahbad</option>
                    <option>Amritsar</option>
                    <option>Anand</option>
                    <option>Aurangabad</option>
                    <option>Bagdogra</option>
                    <option>Banglore</option>
                    <option>Belgaum</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Commodity</label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <select id="commodity_select_field" class="form-control select2_single" tabindex="5">
                    <option>Search option</option>
                    <option>Agartala</option>
                    <option>Agatti</option>
                    <option>Agra</option>
                    <option>Ahemdabad</option>
                    <option>Aizawal</option>
                    <option>Allahbad</option>
                    <option>Amritsar</option>
                    <option>Anand</option>
                    <option>Aurangabad</option>
                    <option>Bagdogra</option>
                    <option>Banglore</option>
                    <option>Belgaum</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Min Rate<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" id="min_rate" tabindex="6">
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Normal Charge<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="8">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Q 45<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="9">
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Q 100<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="10">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Q 250<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="11">
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Q 500<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Q 1000<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="13">
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">P 100<span class="required">*</span>
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="14">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Surcharge
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input type="text" name="points" step="3" tabindex="15">
                </div>
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Surcharge Effective Date
                </label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <input id="birthday1" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" tabindex="16" readonly> 
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Effective Date <span class="required">*</span>
                  </label>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" tabindex="17">
                  </div>
                </div>
              </div>
              <div class="form-group submit">
                <button type="submit" class="btn btn-default submit-btn">Submit</button>
                <button type="submit" class="btn btn-default cancel-btn">Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {
      $(".select2_single").select2({
        placeholder: "Select a state",
        allowClear: true
      }); 
      $('#origin_select_field').select2('focus');
      $('#origin_select_field').select2('open');
    
      /* after selection origin option */
      $('#origin_select_field').on("select2:selecting", function(e) { 
        $('#destination_select_field').select2('focus');
        $('#destination_select_field').select2('open');
      });
    
      /* after selection destination option */
      $('#destination_select_field').on("select2:selecting", function(e) { 
        $('#airline_select_field').select2('focus');
        $('#airline_select_field').select2('open');
      });
    
      /* after selection airline option */
      $('#airline_select_field').on("select2:selecting", function(e) { 
        $('#flight_select_field').select2('focus');
        $('#flight_select_field').select2('open');
      });
    
      /* after selection flight option */
      $('#flight_select_field').on("select2:selecting", function(e) { 
        $('#commodity_select_field').select2('focus');
        $('#commodity_select_field').select2('open');
      });
    
      $('#commodity_select_field').on("select2:selecting", function(e) { 
          $('#min_rate').focus();
          //$('#birthday').data('daterangepicker').toggle();
      });
      
    
      $(".select2_group").select2({});
      $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
      });
    });
  </script>
  <!-- /Select2 -->
  <script>
    $(document).ready(function() {
      $('#birthday').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "showWeekNumbers": true,
        "showISOWeekNumbers": true,
        "autoApply": true,
        "alwaysShowCalendars": true,
        "calender_style": "picker_4",
      });
    });
  </script>    
  <script>
    $(document).ready(function() {
      $('#birthday1').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "showWeekNumbers": true,
        "showISOWeekNumbers": true,
        "autoApply": true,
        "alwaysShowCalendars": true,
        "calender_style": "picker_4",
      });
    });
  </script>    