<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Client</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('employee/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Client's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="contact">Contact No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="1" autofocus="on" id="name">
                                    <option value="NULL">Select Employee's Name</option>
                                    <option value="NULL">abc</option>
                                    <option value="NULL">xyz</option>
                                    <option value="NULL">pqr</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="2" id="contact">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="email" class="form-control" tabindex="3" id="email">
                            </div>
                            <!--<input id="btn-search" tabindex="4" type="submit" class="btn btn-default submit-btn" value="Search">
                            <input tabindex="4" type="reset" value="Clear" class="btn btn-default clear-btn">-->
                            <a tabindex="4" class="btn btn-default submit-btn" title="Search Client" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="5" class="btn btn-default view-btn" title="View All Clients" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="6" class="btn btn-default add-btn"  title="Add Client" href="<?php echo site_url('commodity'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Client</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Client List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">
                    <thead>
                        <tr class="table_heading">
                            <th>Client's Name</th>
                            <th>Client Contact Person Name</th>
                            <th>Email</th>
                            <th>Contact No.</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr>
                            <td>John Doe</td>
                            <td>Adam Willson</td>
                            <td>John@123.com</td>
                            <td>9968532400</td>
                            <td>Mumbai</td>
                            <td>India</td>
                            <td>Borivali (W), Mumbai</td>
                            <td>
                                <a class="" title="View Client"href=""><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View&nbsp;</a>
                                <a title="Update Client" href="" class=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Client"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                                
                            </td>
                        </tr>  

                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

