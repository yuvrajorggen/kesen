</div>
<footer class="main-footer1">
    <div class="pull-right hidden-xs powered-by">
        <a href="http://www.ogcrm.com/" title="" target="_blank">Powered By : <span>OrgGen Technologies</span></a>
    </div>
</footer>
<script src="<?php echo JS_PATH_FRONTEND; ?>chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>prism.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>init.js" type="text/javascript" charset="utf-8"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo JS_PATH_FRONTEND; ?>moment.min.js"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>daterangepicker.js"></script>

<!-- jQuery autocomplete -->
<!--script src="<?php echo JS_PATH_FRONTEND; ?>jquery.autocomplete.min.js"></script>-->

</div>
<?php $this->output->enable_profiler(False); ?>
<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
<script>
    $(function () {
        $('select[multiple].active.3col').multiselect({
            columns: 1,
            placeholder: 'Select Language',
            search: true,
            searchOptions: {
                'default': 'Search Language'
            },
            selectAll: true,
            showCheckbox: true,
            minSelect: 1

        });

    });
</script>
</body>
</html>