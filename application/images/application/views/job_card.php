<!DOCTYPE html>
<html lang="en">

    <head>

        <title>Invoice</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}

            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }
            table tr td p span{font-weight: 700;}
            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}
            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                
                
                <tr >
                    <td colspan="19" style="border-style: solid solid solid solid;padding: 5px 0 10px 30px;font-size: 25px;font-weight: bold;text-align: center;">
                        <img src="../../images/kesen-logo.png" width="220">
                    </td>

                </tr>
                <tr>
                    <td  style="padding: 5px 10px 0;margin: 0;background: #eee;font-size: 18px;font-weight: bold; text-align: center;border: 1px solid #000;border-bottom-style: none;" class="text-center" colspan="19">                        
                        <span style="text-transform:uppercase;">Job Card</span>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Client :<span> Novartis</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Headline :<span> Asthama Brochure Insert 2314 US</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Protocol No. :<span> CQAW039A2314</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Actioned By : <span> Ms. Ketaki</span></p>
                    </td>
                    <td colspan="6">
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Job No. : <span> 22421</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">P.O. No. :<span> 2314</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Contact No. :<span> 9969703676</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Mail By : <span> Ms. Ketaki</span></p>
                    </td>
                    <td colspan="7">
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Date <span>: 26/05/2017</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Date :<span> 05/05/2017</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Handled By : <span> Ms. Madhu</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">&nbsp;</p>

                    </td>
                </tr>
                <tr style="border-bottom: none;">

                    <td colspan="2" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Langs. </td>



                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Unit</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">T Wri</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">PD</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">CR  </td>



                    <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">LPR</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">ER </td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">DV</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">F </td>



                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Verif.</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">PD </td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">CR</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">LPR</td>



                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">ER</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">QC</td>

                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">DV</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">F  </td>



                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">T Sent</td>



                </tr>

                <tr style="border-bottom: none;">

                    <td colspan="1" rowspan="4">Hindi</td>

                    <td style="font-weight: 700;">T</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Dinesh</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">29/5</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">1/6  </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">AY</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">C</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">M </td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Ok </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">P</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">31/7</td>






                </tr>
                <tr style="border-bottom: none;">
                    <td style="font-weight: 700;">V</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Dinesh</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">29/5</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">1/6  </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">AY</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">C</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">M </td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Ok </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">P</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">31/7</td>
                </tr>
                <tr style="border-bottom: none;">
                    <td style="font-weight: 700;">BT</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Dinesh</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">29/5</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">1/6  </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">AY</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">C</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">M </td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">Ok </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">P</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">31/7</td>
                </tr>
                <tr style="border-bottom: 1px solid #000;">
                    <td style="font-weight: 700;">BTV</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">Dinesh</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">29/5</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">1/6  </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">AY</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">C</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">M </td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">Ok </td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>



                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">P</td>

                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;"></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 1px solid #000;">31/7</td>
                </tr>
            </tbody>
        </table>
    </body>

</html>
</html>