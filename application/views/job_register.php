<html lang="en"><head>

        <title>Job Register : <?php echo $result['id']; ?></title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}
            
            .table1{border:2px solid #000;}
                        
            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }

            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}
            table {page-break-after: auto;}

            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table table1" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                <tr>
                    <td style="border-bottom: none;padding: 10px 0;margin: 0;font-size: 40px;font-weight:bolder;text-align: center;border-top: 1px solid #000;/* background: rgba(0, 128, 0, 0.1); */color: green;text-decoration: underline;" class="text-center" colspan="14">
                        <?php
                        if ($result['estimatecompany'] == 1) {
                            echo 'Kesen Linguistic Services Pvt. Ltd.';
                        }
                        if ($result['estimatecompany'] == 2) {
                            echo 'Kesen Language Bureau';
                        }
                        if ($result['estimatecompany'] == 3) {
                            echo 'Kesen Communications Pvt. Ltd.';
                        }
                        if ($result['estimatecompany'] == 4) {
                            echo 'Linguistic Systems';
                        }
                        if ($result['estimatecompany'] == 5) {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #fff;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="14">Job Register <span style="font-size:28px;"> <?php echo $result['id']; ?></span></td>
                </tr>


                <tr style="background: #eee;">

                    <td colspan="1"  style="font-weight: bold;font-size: 13px;">Client Name</td>
                    <td colspan="2"  style="font-weight: 700;font-size: 13px;">Client Contact Person Name</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Contact No</td>
                    <td colspan="1"  style="font-weight: bold;font-size: 13px;">Estimate No. </td>
                    <td colspan="1"  style="font-weight: bold;font-size: 13px;">Po No. </td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Headline</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Protocol No.</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Handled By</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Admin</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Accountant</td>
                    <td colspan="1"  style="font-weight: 700;font-size: 13px;">Date</td>
                    <td colspan="2"  style="font-weight: 700;font-size: 13px;">Other Details</td>
                </tr>
                <tr style="vertical-align: top;">
                    <td colspan="1"  style="font-size: 13px;background-color: #fff;"><?php echo $client['name']; ?></td>
                    <td colspan="2" style="font-size: 13px;background-color: #fff;"><?php echo $clientcontacts['name']; ?></td>
                    <td colspan="1"  style="font-size: 13px;background-color: #fff;"><?php echo $clientcontacts['contactno']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $result['estimateno']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $result['pono']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $result['headline']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $result['protocolno']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $employee[$result['handledby']]['name']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $employee[$result['admin']]['name']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo $employee[$result['accountant']]['name']; ?></td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;"><?php echo date('d M Y', strtotime($result['date'])); ?></td>
                    <td colspan="2" style="font-size: 13px;background-color: #fff;"><?php echo $result['otherdetails']; ?></td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #fff;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="14">&nbsp;</td>
                </tr>
           

                <tr style="border-bottom: none;">
                    <td colspan="4" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Langs. </td>
                    <td colspan="5" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Unit</td>
                    <td colspan="5" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Writer Code</td>
                </tr>

                <?php
                foreach ($jobreglang as $key => $value) {
                    ?>
                    <tr style="border-bottom: none;">
                        <td colspan="3" ><?php echo $language[$value]; ?></td>
                        <td style="font-weight: 700;">T</td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['unit']; ?></td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['writer_id']; ?></td>
                    </tr>
                    <tr style="border-bottom: none;">
                        <td colspan="3" ></td>
                        <td style="font-weight: 700;">V</td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['unit']; ?></td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['writer_id']; ?></td>

                    </tr>
                    <tr style="border-bottom: none;">
                        <td colspan="3" ></td>
                        <td style="font-weight: 700;">BT</td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['unit']; ?></td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['writer_id']; ?></td>

                    </tr>
                    <tr style="">
                        <td colspan="3" ></td>
                        <td style="font-weight: 700;">BTV</td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;"><?php echo $jobcard[$value][4]['unit']; ?></td>
                        <td colspan="5" style="font-size: 13px;background-color: #fff;"><?php echo $jobcard[$value][4]['writer_id']; ?></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>



    </body></html>
