<html lang="en"><head>

        <title>Estimates</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}
            .table1{border:2px solid #000;}

            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }

            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}
            table {page-break-after: auto;}

            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table table1" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                <tr>
                    <td style="border-bottom: none;padding: 10px 0;margin: 0;font-size: 40px;font-weight:bolder;text-align: center;border-top: 1px solid #000;/* background: rgba(0, 128, 0, 0.1); */color: green;text-decoration: underline;" class="text-center" colspan="17"><img src="images/kesen-logo.png" style="width:170;"></td>
                </tr>
                <tr>
                    <td style="border-bottom: none;padding: 5px 0 0 0;    border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="17">
                        Phone : <strong>002 4034 8888,4034 8844 to 8865</strong>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 0 5px 0;    border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="17">
                        Fax : <strong>22674618</strong>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #fff;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="17">Estimates <span style="font-size:28px;"> </span></td>
                </tr>


                <tr>
                    <td colspan="2"><strong>Date</strong></td>
                    <td colspan="1"><strong>Estimate No.</strong></td>
                    <td colspan="1"><strong>Amount</strong></td>
                    <td colspan="1"><strong>Our Company Name</strong> </td>
                    <td colspan="2"><strong>Client Name</strong></td>
                    <td colspan="2"><strong>Client Contact Person Name</strong></td>
                    <th  colspan="2"><strong>Client Contact Person No.</strong></th>
                    <td colspan="2"><strong>Protocol No.</strong></td>
                    <td colspan="2"><strong>Created By</strong></td>
                    <td colspan="2"><strong>Status</strong></td>

                </tr>
                <?php
                $estimates = $this->config->item('estimate');
                foreach ($list as $key => $value) {
                    ?>
                    <tr>
                        <td colspan="2"><?php echo date('d M Y H:i a', strtotime($value['created_date'])); ?></td>
                        <td colspan="1"><?php echo $value['eno']; ?></td>
                        <td colspan="1"><?php echo $value['amount']; ?></td>
                        <td colspan="1"><?php echo $estimates[$value['metrix']]; ?></td>
                        <td colspan="2"><?php echo $client[$value['client_id']]; ?></td>
                        <td colspan="2"><?php echo isset($clientcontacts[$value['clientcontacts_id']]['name']) ? $clientcontacts[$value['clientcontacts_id']]['name'] : "Client contact was deleted."; ?></td>
                        <td colspan="2">
                            <span>M:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['contactno']) ? $clientcontacts[$value['clientcontacts_id']]['contactno'] : ""; ?><br>
                            <span>L:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['landline']) ? $clientcontacts[$value['clientcontacts_id']]['landline'] : ""; ?></td>

                        <td colspan="2"><?php echo $value['headline']; ?></td>
                        <td colspan="2"><?php echo $employee[$value['created_by']]; ?></td>

                        <td colspan="2"><?php
                            $status = 0;
                            if ($value['status'] == 1) {
                                echo 'ESTIMATES APPROVED';
                                $status = 2;
                            } elseif ($value['status'] == 2) {
                                $status = 1;
                                echo 'ESTIMATES REJECTED';
                                echo '<br>Reason:';
                                if ($value['reject_reason'] == 1) {
                                    echo 'Price High';
                                } elseif ($value['reject_reason'] == 2) {
                                    echo 'Quality Issue';
                                } elseif ($value['reject_reason'] == 3) {
                                    echo 'Delivery Issue';
                                } else {
                                    echo 'Other';
                                }
                            } else {
                                echo 'ESTIMATES PENDING APPROVAL';
                            }
                            ?>

                        </td>

                    </tr>  <?php
                }
                ?>
            </tbody>
        </table> 
    </body>
</html>