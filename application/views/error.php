<div class="container">
    <div class="row">
        <div class="not-found">
            <div class="col-md-6">
                <img src="<?php echo IMAGE_PATH_FRONTEND; ?>frontend/oops.png?>" class="img-responsive pull-right" width="300px"/>
            </div>
            <div class="col-md-6">
                <h2>Oops!</h2>
                <p>Looks like something went wrong!</p>
                <p>But dont worry - it can happen to the best of us,</p>
                <p>and it just happen to you.</p>
                <p></p>
                <p>Try this instead :</p>
                <p></p>
                <p></p>
                <p></p>
                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Home</button>
            </div>
        </div>
    </div>
</div>

