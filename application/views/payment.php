<html lang="en"><head>

        <title>Payment Card</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}
            .table1{border:2px solid #000;}

            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }

            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}


            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table table1" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                <tr>
                    <td style="padding: 10px 0;margin: 0;font-size: 40px;font-weight:bolder;text-align: center;border-top: 1px solid #000;border-bottom: none;/* background: rgba(0, 128, 0, 0.1); */color: green;text-decoration: underline;" class="text-center" colspan="7">
                        <?php
                        if ($writer_payment_period['estimatecompany'] == 1) {
                            echo 'Kesen Linguistic Services Pvt. Ltd.';
                        }
                        if ($writer_payment_period['estimatecompany'] == 2) {
                            echo 'Kesen Language Bureau';
                        }
                        if ($writer_payment_period['estimatecompany'] == 3) {
                            echo 'Kesen Communications Pvt. Ltd.';
                        }
                        if ($writer_payment_period['estimatecompany'] == 4) {
                            echo 'Linguistic Systems';
                        }
                        if ($writer_payment_period['estimatecompany'] == 5) {
                            echo 'FINDCREATIVE AVENUES LLP';
                        }

                        $metrixinfo = $this->config->item('metrixinfo');
                        ?>
                    </td>
                </tr>
                <tr>
                    <td  style="text-align: center; " class="text-center" colspan="7">
                        <p style="margin:2px;"><?php echo ucwords($metrixinfo[$writer_payment_period['estimatecompany']]['address1']); ?></p>
                        <p style="margin:2px;"><?php echo $metrixinfo[$writer_payment_period['estimatecompany']]['tel']; ?></p>
                        <p style="margin:2px;"><span style="font-weight:700;">PAN NO: </span> <?php echo $metrixinfo[$writer_payment_period['estimatecompany']]['panno']; ?>
                            <span style="font-weight:700;">GST NO: </span> <?php echo $metrixinfo[$writer_payment_period['estimatecompany']]['gstno']; ?></p>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #eee;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="7">Payment Advice</td>
                </tr>
                <tr>

                    <td colspan="4" rowspan="4">

                        <p style="padding-top:0px;margin: 0;font-size: 18px;font-weight:700;"><span style="
                                                                                                    color: #2f4f4f;
                                                                                                    "><?php echo ucwords($writer['name']); ?></span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">An Electronic payment has been done into your account being translation charges for the jobs listed below :</p>





                    </td>
                    <td style="vertical-align:middle;border-top:none;" colspan="3">
                        Payment Date <span style="font-weight: 700;margin-left: 18px;">: <?php echo date('d M Y'); ?></span>
                    </td>



                </tr>

                <tr style="border:1px solid #000;">

                    <td style="line-height: 1;vertical-align:middle;" colspan="3">
                        Payment Method<span style="font-weight: 700;margin-left: 15px;">:<?php
                            if ($writer_payment_period['payment_method'] == 1) {
                                echo ' NEFT/TPT';
                            } else {
                                echo ' Cheque';
                            }
                            ?></span>
                    </td>
                </tr>
                <tr style="border:1px solid #000;">
                    <td style="line-height: 1;vertical-align:middle;" colspan="3">
                        <?php
                        if ($writer_payment_period['payment_method'] == 1) {
                            echo ' NEFT Reference';
                        } else {
                            echo ' Cheque No';
                        }
                        ?> <span style="font-weight: 700;margin-left: 18px;">: <?php
                        echo $writer_payment_period['payment_meta1'];
                        ?> </span>
                    </td>
                </tr>
                <tr style="border:1px solid #000;">
                    <td style="line-height: 1;vertical-align:middle;" colspan="3">
                        <?php /* Amount <span style="font-weight: 700;margin-left: 21px;">: INR <?php echo $writer_payment_period['final_amount']; ?>.00</span> */ ?>
                        Amount <span style="font-weight: 700;margin-left: 21px;">: INR <?php echo $writer_payment_period['grandtotal']; ?></span>
                    </td>
                </tr>
                <tr style="border-bottom: none;background: #eee;">
                    <td colspan="1" width="20%" style="font-weight: bold;font-size: 13px;">Month</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Job No.</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Description</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Units</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Language</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Rate</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Amount</td>
                </tr>
                <?php
                $estimates = $this->config->item('estimate');
                $typearray = array(
                    1 => 'perunitcharges',
                    2 => 'checkingcharges',
                    3 => 'btcharges',
                    4 => 'btcheckingcharges',
                    5 => 'advertisingcharges',
                    6 => 'lumsumcharges'
                );
                foreach ($writer_payment_period_detail as $key => $value) {
                    if (isset($jobcard[$value['jobcard_id']]) && isset($writer['code']) && $writer['code'] == $jobcard[$value['jobcard_id']]['writer_id']) {
                        ?>
                        <tr style="vertical-align: top;">

                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo date('M Y', strtotime($jobregister[$value['jobregister_id']]['date'])); ?></td>

                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $value['jobregister_id']; ?></td>

                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php
                                if (isset($jobcard[$value['jobcard_id']]) && $jobcard[$value['jobcard_id']]['type'] == 1) {
                                    echo 'T';
                                } elseif (isset($jobcard[$value['jobcard_id']]) && $jobcard[$value['jobcard_id']]['type'] == 2) {
                                    echo 'V';
                                } elseif (isset($jobcard[$value['jobcard_id']]) && $jobcard[$value['jobcard_id']]['type'] == 3) {
                                    echo 'BT';
                                } else {
                                    echo 'BTV';
                                }
                                ?></td>
                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo isset($jobcard[$value['jobcard_id']]) && $jobcard[$value['jobcard_id']]['unit'] ? $jobcard[$value['jobcard_id']]['unit'] : 0; ?></td>
                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $language[$value['language_id']]; ?></td>

                            <td style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php
                                echo isset($jobcard[$value['jobcard_id']]) ? $writer_language_map[$value['language_id']][$typearray[$jobcard[$value['jobcard_id']]['type']]] : '';
                                ?></td>
                            <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $value['amount']; ?></td>
                        </tr>

                        <?php
                    }
                }
                ?>



                <tr style="border: none;">
                    <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border-bottom: none;"></td>
                    <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Total </td>
                    <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['final_amount']; ?></span></td>

                </tr>
                <?php if ($writer_payment_period['performance_charges'] > 0) { ?>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Performance  Charges </td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['performance_charges']; ?></span></td>

                    </tr>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Total + Performance  Charges </td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo ($writer_payment_period['final_amount'] + $writer_payment_period['performance_charges']); ?></span></td>

                    </tr>
                <?php } ?>
                <?php if ($writer_payment_period['gst'] == 'on') { ?>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">GST <?php echo $writer_payment_period['gstpercentage'] . '%'; ?></td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['gst_amount']; ?></span></td>

                    </tr>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Total </td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo ($writer_payment_period['final_amount'] + $writer_payment_period['performance_charges'] + $writer_payment_period['gst_amount']); ?></span></td>

                    </tr>

                <?php } ?>

                <?php if ($writer_payment_period['tds'] == 'on') { ?>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">TDS <?php echo $writer_payment_period['tdspercentage'] . '%'; ?></td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['tds_amount']; ?></span></td>
                    </tr>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Total </td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo ($writer_payment_period['deductable'] + $writer_payment_period['grandtotal']); ?></span></td>
                    </tr>
                <?php } ?>
                <?php
                if ($writer_payment_period['deductable'] > 0) {
                    ?>
                    <tr >
                        <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                        <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Deductable</td>
                        <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['deductable']; ?></span></td>

                    </tr>
                <?php }
                ?>
                <tr >
                    <td colspan="5" style="font-weight: 700;font-size: 13px;background-color: #fff;border: none;"></td>
                    <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Grand Total</td>
                    <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;"><?php echo $writer_payment_period['grandtotal']; ?></span></td>

                </tr>
                <tr>
                    <td colspan="7  " style="border-right-style: solid;font-style: italic;font-weight: bold;font-size: 15px;color: #2c6d2c;text-align: left;">Rupees : <span style="font-size:14px;"><?php echo convert_number_to_words($writer_payment_period['grandtotal']) . ' Only'; ?></span></td>

                </tr>


            </tbody>
        </table>



    </body></html>