<html lang="en"><head>

        <title>Invoice</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}

            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }

            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}


            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                <tr>
                    <td style="border-bottom: none;padding: 10px 0;margin: 0;font-size: 40px;font-weight:bolder;text-align: center;border-top: 1px solid #000;/* background: rgba(0, 128, 0, 0.1); */color: green;text-decoration: underline;" class="text-center" colspan="15">KeSen Language Bureau</td>
                </tr>
                <tr>
                    <td style="border-bottom: none;padding: 5px 0 0 0;margin: 0;font-size: 13px;text-align: center;border-top: none;" class="text-center" colspan="15">101-102 Darshan Apartments, 21 Raghunath Dadaji Street, Fort, Mumbai 400 001</td>
                </tr>

                <tr>
                    <td style="border-bottom: none;padding: 5px 0 0 0;    border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="8">
                        Phone : <strong>002 4034 8888,4034 8844 to 8865</strong>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 0 5px 0;    border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="8">
                        Fax : <strong>22674618</strong>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #fff;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="8">Writer List</td>
                </tr>


                <tr style="background: #eee;">

                    <td colspan="1" width="20%" style="font-weight: bold;font-size: 13px;">Name</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Code</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Email</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Contact No.</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Landline</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Address</td>
                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Language</td>
                    <td colspan="1" width="20%" style="font-weight: bold;font-size: 13px;">Charges</td>

                </tr>
                <?php
                foreach ($result as $key => $value) {
                    ?>
                    <tr style="vertical-align: top;">

                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['name']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['code']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['email']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['contactno']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['landline']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['address']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;"><?php echo $value['language']; ?></td>
                        <td style="font-size: 13px;background-color: #fff;">
                            <p>perunitcharges <?php echo $value['perunitcharges']; ?></p>
                            <p>checkingcharges <?php echo $value['checkingcharges']; ?></p>
                            <p>btcharges <?php echo $value['btcharges']; ?></p>
                            <p>btcheckingcharges <?php echo $value['btcheckingcharges']; ?></p>
                            <p>advertisingcharges <?php echo $value['advertisingcharges']; ?></p>
                            <p>lumsumcharges <?php echo $value['lumsumcharges']; ?></p>
                            <p>perunitcharges <?php echo $value['perunitcharges']; ?></p>
                        </td>
                    </tr>
                    <?php
                }
                ?>




            </tbody>
        </table>



    </body></html>
