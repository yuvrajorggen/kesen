</div>
<footer class="main-footer1">
    <div class="pull-right hidden-xs powered-by">
        <a href="http://www.ogcrm.com/" title="" target="_blank">Powered By : <span>OrgGen Technologies</span></a>
    </div>
</footer>



<!-- bootstrap-daterangepicker -->
<script src="<?php echo JS_PATH_FRONTEND; ?>moment.min.js"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>daterangepicker.js"></script>


</div>
<?php $this->output->enable_profiler(FALSE); ?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>

<script src="<?php echo JS_PATH_FRONTEND; ?>select2.min.js"></script>
<link href="<?php echo CSS_PATH_FRONTEND; ?>jquery-ui.css" rel="stylesheet" />

<link rel="stylesheet" href="<?php echo CSS_PATH_FRONTEND; ?>jquery-confirm.min.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery-confirm.min.js"></script>

<script>
    $(function () {

        $('select.active.3col').multiselect({
            columns: 1,
            placeholder: 'Search',
            search: true,
            searchOptions: {
                'default': 'Search'
            },
            selectAll: true,
            showCheckbox: true,
            minSelect: 1,
        });
    });</script>
<script>
    $(document).ready(function () {
        $(".searchselected").select2({
            placeholder: "",
            allowClear: true
        });
        $('#clientname').on('change', function () {
            //alert($(this).val());return;
            var id = $(this).val();

            $.ajax({
                url: "<?php echo base_url() . 'odv/getclientcontactlist'; ?>",
                type: "POST",
                data: {q: id},
                beforeSend: function () {
                    $('#clientcontactname').empty();
                    // clear all option
                    $('#clientcontactname').html('').select2({data: [{id: '', text: ''}]});
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    location.reload();
                },
                success: function (response) {

                    var check = jQuery.isEmptyObject({response});
                    if (check) {
                        del('Close', 'btn-red', 'Error', 'No Client contacts found. First add them.', '<?php echo base_url('commodity/entry_list'); ?>');
                    } else {
                        $("#clientcontactname").html('');
                        $.each($.parseJSON(response), function (key, value) {
                            $("#clientcontactname").select2({data: [{id: key, text: value}]});
                        });
                    }
                },
                complete: function () {

                },
                timeout: 1000000
            });
        });
        /* btnClass: 'btn-red',
         * 
         */
        $('#clientname').on('change', function () {
            
            var id = $(this).val();
            
            $("#language").val(arrayFromPHP[id]);
        });

    });
    function del(buttontext, buttonclass, title, content, url) {
        $.confirm({
            title: title,
            content: content,
            theme: 'material', // 'material', 'bootstrap',
            type: 'blue',
            typeAnimated: true,
            closeIcon: true,
            columnClass: 'col-md-4 col-md-offset-4',
            autoClose: 'cancelAction|8000',
            buttons: {
                deleteUser: {
                    text: buttontext,
                    btnClass: buttonclass,
                    action: function () {
                        $(location).attr('href', url);
                    }
                },
                cancelAction: function () {

                }
            }
        });
    }



    function reject(buttontext, buttonclass, title, content, url, id) {
        $.confirm({
            title: title,
            theme: 'material', // 'material', 'bootstrap',
            type: 'blue',
            typeAnimated: true,
            closeIcon: true,
            columnClass: 'col-md-4 col-md-offset-4',
            autoClose: 'cancelAction|8000',
            content: '' + content + '<form id="reasonform" method="get" action="<?php echo base_url() . 'estimates/statuschange'; ?>" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Select Reason</label>' +
                    '<select name="reason" class="reason" required ><option value=""></option><option value="1">Price High</option><option value="2">Quality Issue</option><option value="3">Delivery Issue</option><option value="4">Other</option></select>' +
                    '</div><input type="hidden" name="id" value="' + id + '">' +
                    '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var reasonvalue = $('.reason').val();
                        if (!reasonvalue) {
                            $.alert('Kindly Select Reason');
                            return false;
                        } else {
                            $("#reasonform").submit();
                        }
                    }
                },
                cancel: function () {
                    //close
                    //alert('dfs');
                },
            },
            onContentReady: function () {

                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    //alert('sfdsd');
                    // if the user submits the form by pressing enter in the field.
                    //e.preventDefault();
                    //jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }

    function email_reply_letter(buttontext, buttonclass, title, content, url, id) {
        $.confirm({
            title: title,
            theme: 'material', // 'material', 'bootstrap',
            type: 'blue',
            typeAnimated: true,
            closeIcon: true,
            columnClass: 'col-md-4 col-md-offset-4',
            autoClose: 'cancelAction|8000',
            content: '' + content + '<form id="reasonform" method="POST" action="<?php echo base_url() . 'odv/savedate'; ?>" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Select Reference Email Dated</label>' +
                    '<input class="reason" type="date" name="date" id="redate">' +
                    '</div><input type="hidden" name="id" value="' + id + '">' +
                    '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var reasonvalue = $('#redate').val();

                        if (!reasonvalue) {
                            $.alert('Kindly Select Date');
                            return false;
                        } else {
                            $("#reasonform").submit();
                        }
                    }
                },
                cancel: function () {
                    //close
                    //alert('dfs');
                },
            },
            onContentReady: function () {

                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    //alert('sfdsd');
                    // if the user submits the form by pressing enter in the field.
                    //e.preventDefault();
                    //jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    }
	
    $(document).keypress(function (e) {
        if (e.which == 13) {
            // enter pressed
            $('form').submit();
        }
    });
</script>
</body>
</html>