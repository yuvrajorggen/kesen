<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <meta http-equiv="Cache-Control" content="no-store" />
        <meta name="robots" content="NoArchive">
        <meta name="robots" content="NoSnippet">
        <meta name="robots" content="NoODP">
        <meta name="robots" content="NoImageIndex">
        <meta name="robots" content="NoTranslate">
        <title>Kesen</title>
        <noscript>
        <meta http-equiv="refresh" content="0; url="<?php echo base_url(); ?>/page_management/no_js" />

              </noscript>
              <?php /* Bootstrap */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>bootstrap.min.css" rel="stylesheet">
        <?php /* Font Awesome */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>font-awesome.min.css" rel="stylesheet">
        <?php /* custom */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>custom.min.css" rel="stylesheet">
        <?php /* Select2 */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>select2.min.css" rel="stylesheet">
        <?php /* bootstrap-daterangepicker */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>daterangepicker.css" rel="stylesheet">

        <?php /* jQuery */ ?>
        <script  src="<?php echo JS_PATH_FRONTEND; ?>jquery.min.js"></script>
        <?php /* Bootstrap */ ?>
        <script src="<?php echo JS_PATH_FRONTEND; ?>bootstrap.min.js"></script>

        <?php /* Timepicker */ ?>


        <?php /* Chosen(select plugin) */ ?>
        <link href="https://fonts.googleapis.com/css?family=Cairo|Ubuntu" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo CSS_PATH_FRONTEND; ?>prism.css">


    </head>
    <body>
        <div class="" id="loading"></div>
        <div class="container">
            <div class="row">
                <div class="top-header">
                    <div class="navbar-brand col-sm-2 sol-md-2">
                        <a href="<?php echo site_url('odv'); ?>"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>kesen-logo.png" class="logo img-responsive"></a>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <span id="crm_ctrl_panel_invoice_edit_search" class="crm-search-block col-md-3 pull-left" data-toggle="tooltip" data-placement="bottom" >
                            <form class="crm-search" id="crm-search" action="<?php echo site_url('odv/odv_list'); ?>" method="GET">
                                <span class="crm-search-btn fa fa-search" id="search_get_input" onclick="$('form#crm-search').submit();"></span> <span class="crm-search-inp-wrap">
                                    <input id="crm_ctrl_panel_invoice_edit_search_input" class="crm-search-inp" name="jobno" type="text"  autocomplete="off" placeholder="Search Job Card No: 1234" value="<?php echo isset($_GET['jobno']) ? $_GET['jobno'] : ""; ?>"/>
                                </span>

                            </form>
                        </span> 

                        <nav class="nav navbar-nav tab">
                            <ul>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="active">
                                        <a href="">Job Register</a>
                                        <ul class=" employee_submenu">
                                            <div class="row submenu">                                        
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('odv/add'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Create Job Register</a></li>
                                                    <li><a href="<?php echo site_url('odv/odv_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Register List</a></li>
                                                </div>

                                            </div> 

                                        </ul>
                                    </li>
                                <?php } ?>

                                <li class="menu">
                                    <a href="">Job Card</a>
                                    <ul class=" employee_submenu">
                                        <div class="row submenu">
                                            <div class="col-sm-12 col-md-12">
                                                <!-- <li><a href="<?php echo site_url('shipper/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Create Job Card</a></li>-->
                                                <li><a href="<?php echo site_url('shipper/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Card List</a></li>
                                                 <!--<li><a href="<?php echo site_url('shipper/payment'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Payment</a></li>-->
                                            </div>
                                        </div>
                                    </ul>
                                </li>

                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Estimates</a>
                                        <ul class="employee_submenu">
                                            <div class="row submenu">
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('estimates/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add estimates</a></li>
                                                    <li><a href="<?php echo site_url('estimates/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All estimates</a></li>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Employee</a>
                                        <ul class="employee_submenu">
                                            <div class="row submenu">
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('employee/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Employee</a></li>
                                                    <li><a href="<?php echo site_url('employee/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Employees</a></li>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Language</a>
                                        <ul class="employee_submenu">
                                            <div class="row submenu">
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('language/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Language</a></li>
                                                    <li><a href="<?php echo site_url('language/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Language</a></li>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Writer</a>
                                        <ul class="employee_submenu">
                                            <div class="row submenu">
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('customer/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Writer</a></li>
                                                    <li><a href="<?php echo site_url('customer/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Writers</a></li>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Client</a>
                                        <ul class=" employee_submenu">
                                            <div class="row submenu">                                        
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('commodity/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Client</a></li>
                                                    <li><a href="<?php echo site_url('commodity/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Clients</a></li>
                                                </div>

                                            </div> 

                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Writer's Payment</a>
                                        <ul class=" employee_submenu">
                                            <div class="row submenu">                                        
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href="<?php echo site_url('consignee/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Payment</a></li>
                                                    <li><a href="<?php echo site_url('consignee/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Payment List</a></li>
                                                </div>

                                            </div> 

                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] != 4) { ?>
                                    <li class="menu">
                                        <a href="">Reports</a>
                                        <ul class="employee_submenu">
                                            <div class="row submenu">
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i> Job Card</a> </li>
                                                    <li><a href="<?php echo site_url('consignor/entry_list?bill=1'); ?>">&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i> Job Billed</a></li>
                                                    <li><a href="<?php echo site_url('consignor/entry_list?bill=0'); ?>">&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i> Job Unbilled</a></li>
                                                    <li><a href="<?php echo site_url('consignor/timeline_report'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Timeline</a></li>
                                                    <li><a href="<?php echo site_url('customer/report'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Writer Work Done</a></li>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($_SESSION['user']['role'] == 5) { ?>
                                    <li class="menu">
                                        <a href="">DB Backup</a>
                                        <ul class=" employee_submenu">
                                            <div class="row submenu">                                        
                                                <div class="col-sm-12 col-md-12">
                                                    <li><a target="_blank" href="<?php echo site_url('commodity/db_backup'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> DB Backup</a></li>
                                                    
                                                </div>

                                            </div> 

                                        </ul>
                                    </li>
                                <?php } ?>
                                <li class="menu">
                                    <a><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Hi,<?php echo $_SESSION['user']['name']; ?></a>
                                </li>
                                <li class="menu" style="">
                                    <a href="<?php echo site_url('login/logout'); ?>"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;Logout</a>
                                </li>  
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <?php
                    $danger = $this->session->flashdata('danger');
                    if ($danger != '') {
                        ?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $danger; ?>
                        </div>
                    <?php } ?>
                    <?php
                    $success = $this->session->flashdata('success');
                    if ($success != '') {
                        ?>
                        <div class="alert alert-success">
                            <strong>Success!</strong> <?php echo $success; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>