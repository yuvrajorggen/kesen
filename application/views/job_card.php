<!DOCTYPE html>
<html lang="en">

    <head>

        <title>Job Card </title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}
            .table1{border:2px solid #000;border-bottom: none;}
            .table2{border:2px solid #000;border-top: none;}
            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }
            table tr td p span{font-weight: 700;}
            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}
            table { page-break-inside:always; }
            table tr td, table tr th {
                page-break-inside: avoid;page-break-after: avoid;
            }
            li{padding: 5px 0 5px 0;}
            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table table1" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>



                <tr >
                    <td colspan="12" style="border-style: solid solid solid solid;padding: 5px 0 10px 30px;font-size: 25px;font-weight: bold;text-align: center;">
                        <img src="<?php echo FCPATH; ?>images/kesen-logo.png" width="220">
                    </td>

                </tr>
                <tr >
                    <td  colspan="12"  style="font-size: 13px;background-color: #fff;font-size: 25px;font-weight: bold;text-align: center;"><?php
                        if ($register['estimatecompany'] == 1) {
                            echo 'Kesen Linguistic Services Pvt. Ltd.';
                        }
                        if ($register['estimatecompany'] == 2) {
                            echo 'Kesen Language Bureau';
                        }
                        if ($register['estimatecompany'] == 3) {
                            echo 'Kesen Communications Pvt. Ltd.';
                        }
                        if ($register['estimatecompany'] == 4) {
                            echo 'Linguistic Systems';
                        }
                        if ($register['estimatecompany'] == 5) {
                            echo '';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 10px 0;margin: 0;background: #eee;font-size: 18px;font-weight: bold; text-align: center;border: 1px solid #000;border-bottom-style: none;" class="text-center" colspan="12">                        
                        <span style="text-transform:uppercase;">Job Card</span>

                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Client :<span  style="font-size: 28px;font-weight: 700;"> <?php echo $client['name']; ?></span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Headline :<span>  <?php echo $register['headline']; ?></span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Protocol No. :<span>   <?php echo $register['protocolno']; ?></span></p>
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Estimate No. :<span>   <?php echo ($register['estimateno'] > 0) ? $register['estimateno'] : ''; ?></span></p>

                    </td>
                    <td colspan="4">
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Job No. : <span style="font-size: 28px;font-weight: 700;"> <?php echo $register['id']; ?></span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 16px;">Client Contact Person Name:<span> <?php echo $clientcontacts['name']; ?></span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 16px;">Client Contact Person Number :<span> <?php echo $clientcontacts['contactno']; ?></span></p>

                    </td>
                    <td colspan="4">
                        <p style="padding-top:10px;margin: 0;font-size: 16px;">Date : <span  style="font-size: 28px;font-weight: 700;"> <br><?php echo date('d-m-y', strtotime($register['date'])); ?></span></p>
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">P.O. No. :<span> <?php echo $register['pono']; ?></span></p>
                        <p style="padding-top:10px;margin: 0;font-size: 13px;">Handled By : <span> <?php echo $employee[$register['handledby']]['name']; ?></span></p>



                    </td>
                </tr>
            </tbody>
        </table>
        <table width="100%" class="table goods-table table2" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody> 

                <tr style="">
                    <td colspan="2" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Langs. </td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Unit</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Writer Code</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Employee Code</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">PD  </td>
                    <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">CR</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">C/NC </td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">DV</td>
                    <td colspan="1" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">F/QC</td>
                    <td colspan="2" style="font-size: 13px;background-color: #f0f0f0;border-bottom: none;font-weight: 700;">Sent Date</td>
                </tr>

                <?php
                foreach ($jobreglang as $key => $value) {
                    ?>


                    <tr style="border-bottom: none;">
                        <td colspan="1" style="border-bottom:none;">&nbsp;<?php
                            echo $language[$value];
                            $bgcolor = "";
                            $span = "";
                            if ($jobcard[$value][1]['task'] == 1) {
                                $bgcolor = "background:#5a5a5a;";
                                $span = "color:#FFF;";
                            }
                            ?></td>
                        <td  colspan="1" style="font-weight: 700;<?php echo $bgcolor; ?> "><span style="<?php echo $span; ?>">T</span></td>

                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][1]['unit'] != 0) ? $jobcard[$value][1]['unit'] : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['writer_id']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['employeecode']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][1]['pd'] != NULL) ? date('d M Y', strtotime($jobcard[$value][1]['pd'])) : ''; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][1]['cr'] != NULL) ? date('d M Y', strtotime($jobcard[$value][1]['cr'])) : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">
                            <?php
                            if ($jobcard[$value][1]['cnc'] != 0) {
                                if ($jobcard[$value][1]['cnc'] == 1) {
                                    echo 'C';
                                } else {
                                    echo 'NC';
                                }
                            };
                            ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['dv']; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][1]['fqc']; ?> </td>
                        <td colspan="2" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][1]['sentdate'] != NULL) ? date('d M Y', strtotime($jobcard[$value][1]['sentdate'])) : ''; ?></td>
                    </tr>
                    <tr style="border-bottom: none;">
                        <td colspan="1" style="border:none;">&nbsp;</td>
                        <?php
                        $bgcolor = "";
                        $span = "";

                        if ($jobcard[$value][2]['task'] == 1) {
                            $bgcolor = "background:#5a5a5a;";
                            $span = "color:#FFF;";
                        }
                        ?>
                        <td colspan="1" style="font-weight: 700;<?php echo $bgcolor; ?> "><span style="<?php echo $span; ?>">V</span></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][2]['unit'] != 0) ? $jobcard[$value][2]['unit'] : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['writer_id']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['employeecode']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][2]['pd'] != NULL) ? date('d M Y', strtotime($jobcard[$value][2]['pd'])) : ''; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][2]['cr'] != NULL) ? date('d M Y', strtotime($jobcard[$value][2]['cr'])) : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">
                            <?php
                            if ($jobcard[$value][2]['cnc'] != 0) {
                                if ($jobcard[$value][2]['cnc'] == 1) {
                                    echo 'C';
                                } else {
                                    echo 'NC';
                                }
                            };
                            ?>

                        </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['dv']; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][2]['fqc']; ?> </td>
                        <td colspan="2" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][2]['sentdate'] != NULL) ? date('d M Y', strtotime($jobcard[$value][2]['sentdate'])) : ''; ?></td>

                    </tr>
                    <tr style="border-bottom: none;">
                        <td colspan="1" style="border:none;">&nbsp;</td>
                        <?php
                        $bgcolor = "";
                        $span = "";

                        if ($jobcard[$value][3]['task'] == 1) {
                            $bgcolor = "background:#5a5a5a;";
                            $span = "color:#FFF;";
                        }
                        ?>
                        <td colspan="1" style="font-weight: 700;<?php echo $bgcolor; ?> "><span style="<?php echo $span; ?>">BT</span></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][3]['unit'] != 0) ? $jobcard[$value][3]['unit'] : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['writer_id']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['employeecode']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][3]['pd'] != NULL) ? date('d M Y', strtotime($jobcard[$value][3]['pd'])) : ''; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][3]['cr'] != NULL) ? date('d M Y', strtotime($jobcard[$value][3]['cr'])) : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">
                            <?php
                            if ($jobcard[$value][3]['cnc'] != 0) {
                                if ($jobcard[$value][3]['cnc'] == 1) {
                                    echo 'C';
                                } else {
                                    echo 'NC';
                                }
                            };
                            ?>

                        </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['dv']; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo $jobcard[$value][3]['fqc']; ?> </td>
                        <td colspan="2" style="font-size: 13px;background-color: #fff;border-bottom: none;"><?php echo ($jobcard[$value][3]['sentdate'] != NULL) ? date('d M Y', strtotime($jobcard[$value][3]['sentdate'])) : ''; ?></td>

                    </tr>
                    <tr style="border-bottom: none;">
                        <td colspan="1" style="border: none;border-bottom: 2px solid #000;">&nbsp;</td>
                        <?php
                        $bgcolor = "";
                        $span = "";

                        if ($jobcard[$value][4]['task'] == 1) {
                            $bgcolor = "background:#5a5a5a;";
                            $span = "color:#FFF;";
                        }
                        ?>
                        <td colspan="1" style="font-weight: 700;border-bottom: 2px solid #000;<?php echo $bgcolor; ?> "><span style="<?php echo $span; ?>">BTV</span></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo ($jobcard[$value][4]['unit'] != 0) ? $jobcard[$value][4]['unit'] : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo $jobcard[$value][4]['writer_id']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;;"><?php echo $jobcard[$value][4]['employeecode']; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo ($jobcard[$value][4]['pd'] != NULL) ? date('d M Y', strtotime($jobcard[$value][4]['pd'])) : ''; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo ($jobcard[$value][4]['cr'] != NULL) ? date('d M Y', strtotime($jobcard[$value][4]['cr'])) : ''; ?></td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;">

                            <?php
                            if ($jobcard[$value][4]['cnc'] != 0) {
                                if ($jobcard[$value][4]['cnc'] == 1) {
                                    echo 'C';
                                } else {
                                    echo 'NC';
                                }
                            };
                            ?>
                        </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo $jobcard[$value][4]['dv']; ?> </td>
                        <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo $jobcard[$value][4]['fqc']; ?> </td>
                        <td colspan="2" style="font-size: 13px;background-color: #fff;border-bottom: 2px solid #000;"><?php echo ($jobcard[$value][4]['sentdate'] != NULL) ? date('d M Y', strtotime($jobcard[$value][4]['sentdate'])) : ''; ?></td>

                    </tr>
                <?php } ?>
                <tr style="">
                    <td colspan="6" rowspan="" style="font-size: 13px;vertical-align: top;">
                        <p>Delivery Date : <span style="font-weight: 700;"><?php echo ($register['deliverydate'] != '0000-00-00') ? date('d M Y', strtotime($register['deliverydate'])) : ''; ?></span></p>  
                        <p>Words / Units : <span style="font-weight: 700;"><?php echo $register['wordsunit']; ?></span></p>   
                        <p>Old Job No : <span style="font-size: 28px;font-weight: 700;"><?php echo $register['oldjobno']; ?></span></p>
                        <p>Checked with Operator : <span style="font-weight: 700;"><?php
                                if ($register['checkedwithoperator'] == 1) {
                                    echo 'Yes';
                                };
                                if ($register['checkedwithoperator'] == 2) {
                                    echo 'No';
                                };
                                ?></span></p>
                        <p>Remarks: <?php echo $register['remarks']; ?></p>
                    </td>
                    <td colspan="6" style="font-size: 13px;">
                        <p>Bill No : <span style="font-weight: 700;"><?php echo $register['billno']; ?></span></p> 
                        <p>Bill Date : <span style="font-weight: 700;"><?php echo ($register['invoicedate'] != '0000-00-00') ? date('d M Y', strtotime($register['invoicedate'])) : ''; ?></span> </p> 
                        <p>Bill sent on : <span style="font-weight: 700;"><?php echo ($register['billsentdate'] != '0000-00-00') ? date('d M Y', strtotime($register['billsentdate'])) : ''; ?></span></p> 
                        <p>Informed To : <span style="font-weight: 700;"><?php echo $register['informedto']; ?></span></p> 
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>