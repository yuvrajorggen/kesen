<html>
    <head>
        <style>
            body{color:#000;}
            p{margin:3px;}
            tr, td{font-size: 14px;}
            th{color:#795548;padding:5px;}
            h1{margin: 0;text-align:center;font-size:25px;color:#607D8B;}
            h2{font-size:16px;}
            button{font-size: 16px;
                   display: block;
                   margin: 20px 0;
                   background-color: #F44336;
                   color: #fff;
                   padding: 10px;
                   border: none;
                   border-radius: 5px;}
            .main-table{background-color:#fafafa;font-family: 'Helvetica' , sans-serif; font-size:14px; margin:0; padding:0;}
            .report-table{border-collapse: collapse;display:block;text-align:center;}
            .report-table tr{border-bottom: 2px solid #795548;}
            .report-table tr td{padding:5px;}
        </style>
    </head>
    <body>
        <table class="main-table" cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
            <tbody><tr>
                    <td align="center" valign="top" style="padding:20px 0 20px 0">
                        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="600" style="border:1px solid #dddddd;">
                            <tbody>
                                <tr>
                                    <td colspan="1" style="padding:0px;background-color: #0e9046;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding:10px;text-align:left;color: #fff;">
                                                        Call Us : <a href="tel:9821022327" style="text-decoration:none;color:#fff;font-weight:500;">98210 22327</a>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                    <td colspan="1" style="padding:0px;background-color: #00913f;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding:10px;text-align:right;color:#fff;">
                                                        Mail Us : <a href="mailto:klanguagebureau@gmail.com" style="text-decoration:none;color: #fff;font-weight:500;">klanguagebureau@gmail.com</a>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr>

                                </tr><tr>
                                    <td colspan="2" style="padding:0px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="background-color: #fff;padding: 20px 10px;text-align: center;">
                                                        <img src="http://kesenlanguagebureau.in/wp-content/uploads/2018/04/kesen-logo.png" style="width:170;">

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr style="
                                    /* background: #edf7f0; */
                                    ">
                                    <td colspan="2" style="padding:0px;padding-left: 22px;padding-right: 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               background: #edf7f0;
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding: 5px 20px;text-align:center;font-weight:700;border-left: 2px solid green;/* margin-left: 16px; */">
                                                        <p>FINALIZED JOB CONFIRMATION LETTER</p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr style="
                                    /* background: #edf7f0; */
                                    ">
                                    <td colspan="2" style="padding:0px;padding-left: 22px;padding-right: 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="

                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding: 5px 20px;text-align:right;font-weight:700;border-left: 2px solid green;/* margin-left: 16px; */">
                                                        <p>Date : <?php echo date('d F Y'); ?></p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding:0px;padding-left: 22px;padding-right: 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="
                                               /* background: #edf7f0; */
                                               ">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding:0 20px 10px;text-align:left;font-weight:700;border-left: 2px solid rgba(25, 107, 25, 0.99);/* margin-left: 16px; */">
                                                        
                                                        <p><?php echo ucwords($clientcontacts['name']); ?></p>
                                                        <p><?php echo $client['name']; ?></p>
                                                        <p>Protocol No. : <?php echo $register['protocolno']; ?></p>
                                                        <p>Job Code : <?php echo $register['id']; ?></p>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding:0px;background-color:#fff;padding: 0 22px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding:0 20px;text-align:left;padding: 0 0 0 20px;text-align: left;border-left: 2px solid rgba(25, 107, 25, 0.99);">
                                                        <p><strong>Dear <?php echo $clientcontacts['name']; ?>,</strong></p>
                                                        <p>With reference to the above document, kindly send us a mail, confirming that the task undertaken is complete and satisfactory.</p>
                                                        <p style="margin:10px 0;font-style:italic;font-weight:700;color: #2fa260;">Thanking You in advance for your early reply.</p>
                                                        <p>If we do not receive a communication from you within 7 working days, we will presume that the job is accepted.</p>
                                                        <p style="margin-top:10px;">Kindly complete the feedback form to help us, to serve you better.</p>
                                                        <p style="margin-top:10px;">Looking forward to work with you in the near future.</p>
                                                        <p style="height:20px;"></p>
                                                        <p style="margin-top:15px;">Warm Regards</p>

                                                        <p><strong>Keith Myers</strong></p>

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px;background-color:#fff;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="padding:0 20px;text-align:left;padding: 0 20px 15px;text-align: left;font-weight: 700;/* border-left: 2px solid green; */">

                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="2" style="padding: 0px;background: #0e9046;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td colspan="2" style="background-color: #0e9046;padding: 15px 0;text-align: center;color: #fff;">
                                                        <p class="text-center" style="font-size:14px;">©2017 Kesen. All rights reserved.</p>
                                                        <p style="color: #fff;">Powered by <a href="http://www.orggen.com" target="_blank" style="color: #fff;text-decoration:none;font-weight:700;">OrgGen Technologies </a></p></td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                    </td>
                </tr>
            </tbody></table>

    </body></html>