<?php
$roles = $this->config->item('roles');
?>
<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Language</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('language/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="role">Role<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="contact">Contact No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="email">Email<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input required="" type="text" value="<?php echo $this->input->get('name'); ?>" class="form-control" tabindex="1" name="name" id="name">
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <select class="chosen-select" tabindex="2" name="" id="role">
                                    <option>Select Role</option>
                                    <option>Manager</option>
                                    <option>Senior Manager</option>
                                    <option>Supervisor</option>
                                    <option>Staff</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <input type="text" class="form-control" value="" tabindex="3" id="contact">
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <input type="email" class="form-control" tabindex="4" id="email">
                            </div>
                            <input id="btn-search" tabindex="4" type="submit" class="btn btn-default submit-btn" value="Search">
                            <!--<input tabindex="4" type="reset" value="Clear" class="btn btn-default clear-btn">
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Employee" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>-->
                            <a tabindex="6" class="btn btn-default view-btn" title="View All Employees" href="<?php echo site_url('language/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default add-btn"  title="Add Employee" href="<?php echo site_url('language'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Language</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Language List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Language : <?php echo $total; ?></span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>No</th>
                            <th>Name</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php
                        $estimates = $this->config->item('estimate');
                        foreach ($list as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo ucwords($value['id']); ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $employee[$value['created_by']]; ?></td>
                                <td><?php echo date('d M Y H:i A',  strtotime($value['created_date']) ) ; ?></td>

                            </tr>  <?php
                        }
                        ?>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

