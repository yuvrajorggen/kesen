<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Language</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('language/update') : site_url('language/insert') ?>"> 
                        <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="employee">Language Name<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type = "text" class = "form-control" tabindex="1" value="<?php echo isset($employee['name']) ? $employee['name'] : ''; ?>" autofocus="on" id="employee" name="language[name]">
                                </div>
                                
                            </div>
                        </div>

                       
                        
                       
                        
                        
                        <div class = "row">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    var input = document.getElementById('ecode');

    input.onkeyup = function () {
        this.value = this.value.toUpperCase();
    }
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#employee_add").validate({
        ignore: [],
        rules: {
            'employee[name]': {
                required: true,
                rangelength: [3, 50],
                lettersonly: true
            },
            'employee[email]': {
                required: true,
                email: true,
            },
            'employee[contactno]': {
                required: true,
                digits: true,
                rangelength: [10, 10],
            },
            'employee[password]': {
                required: true,
                rangelength: [3, 7]
            }, 'employee[samepassword]': {
                equalTo: "#pwd"
            },
            'employee[code]': {
                required: true,
                rangelength: [1, 5],
                digits: true,
            }, 'employee[address]': {
                required: true,
                rangelength: [3, 500],
            }, 'language[]': {
                required: true,
            }
        },
        messages: {
            'employee[name]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        },
        submitHandler: function (form, event) {

            $.ajax({
                url: '<?php echo base_url() . 'employee/get_code'; ?>',
                type: 'POST',
                data: $(form).serialize(),
                success: function (response) {
                    form.submit();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    event.preventDefault();
                    $.dialog({
                        title: 'Error Message',
                        content: 'Code already assign to employee. Kindly select new code',
                        closeIcon: true,
                        buttons: {
                            cancelAction: function () {

                            }
                        }
                    });
                }
            });
        }
    });
    $("#pwdupdate_chkbox").change(function () {
        if (this.checked) {
            $("#password_wrapper").show();
            $("#pwd").val('');
            $("#confirmpwd").val('');
        } else {
            $("#password_wrapper").hide();
            $("#pwd").val('123');
            $("#confirmpwd").val('123');
        }
    });

</script>