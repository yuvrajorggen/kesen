<?php

class Language extends MX_Controller {

    function __construct() {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);       
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['employee'] = array();
        $lang = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());

        foreach ($lang as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $data['langemplist'] = array();
        $view = 'add_entry';

        $this->_display($view, $data);
    }

    function insert() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $sessiondata = $this->session->userdata('user');
        $data = $this->input->post('language');
        $data['name'] = ucwords($data['name']);
        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');

        $eid = $this->gm->insert('language', $data);

        redirect(site_url('language/entry_list'));
    }

    function entry_list() {

        $this->load->helper('url');
        $page = $this->uri->segment(3);
        $this->load->model('Global_model', 'gm');

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $clause = '';
        if (isset($_GET['name'])) {
            $clause = " AND name LIKE '%" . $_GET['name'] . "%'";
        }

        $query = "SELECT * FROM `language` WHERE status='1' " . $clause . " ORDER BY `id`  DESC limit " . PER_PAGE . " offset " . $offset . "";

        $query_exec = $this->db->query($query);

        if ($query_exec != NULL && is_object($query_exec)) {
            $result = $query_exec->result_array();
        }


        $cquery = "SELECT count(id) as id FROM `language` WHERE status='1' " . $clause;
        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->row_array();


        $data['list'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;


        $this->load->library('pagination');

        $config['base_url'] = site_url('language/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);

        $lang = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $elm = $this->gm->get_data_list('employee_language_map', array(), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($elm as $key => $value) {
            $data['langemplist'][$value['employee_id']][] = $value['language_id'];
        }
        $lang = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['employee'][$value['id']] = $value['name'];
        }
        $view = 'entry_list';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');

        $id = $this->input->post('empid');
        $data = $this->input->post('employee');
        $language = $this->input->post('language');


        if (isset($data['check_password']) && $data['check_password'] == 'on') {

            $data['password'] = md5($data['password']);
        } else {
            unset($data['password']);
        }

        unset($data['samepassword']);
        unset($data['check_password']);
        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');

        $this->gm->update('employee', $data, 0, array('id' => $id));

        $query = "DELETE FROM `employee_language_map` WHERE `employee_id` = " . $id . ";";
        $this->db->query($query);

        foreach ($language as $key => $value) {
            $langdata['language_id'] = $value;
            $langdata['employee_id'] = $id;
            $this->gm->insert('employee_language_map', $langdata);
        }
        redirect("employee/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $data['employee'] = $this->gm->get_selected_record('employee', '*', $where = array('id' => $this->input->get('id')), array());

        $lang = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $elm = $this->gm->get_data_list('employee_language_map', array('employee_id' => $_GET['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($elm as $key => $value) {
            $data['langemplist'][$value['language_id']] = $value['language_id'];
        }
        $data['mode'] = "update";
        $view = 'add_entry';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function delete() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $id = $_GET['id'];
        //$employee = $this->gm->get_selected_record('employee', 'firstname,lastname', $where = array('id' => $id), array());
        $set = array(
            'modified_by' => $sessiondata['id'],
            'modified_date' => date('Y-m-d H:i:s'),
            'status' => 2
        );
        $data['employee'] = $this->gm->update('employee', $set, $id);

        //$message = '<b>' . $employee['firstname'] . ' ' . $employee['lastname'] . ' ' . 'deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("employee/entry_list");
    }

    function get_city() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $state_id = $this->input->post('state_id');
        $city_data = $this->gm->get_data_list('cities', array('state_id' => $state_id), array(), array('id' => 'desc'), 'id,name', 0, array());

        echo json_encode($city_data);
    }

    function get_code() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $employee = $this->input->post('employee');
        $result = $this->gm->get_selected_record('employee', '*', $where = array('code' => strtoupper($employee['code'])), array());

        if (isset($result['id'])) {
            http_response_code(401);
        } else {
            http_response_code(200);
        }
    }

    function get_employee() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $query = "SELECT * FROM `employee` WHERE `code` LIKE '%" . $_GET['query'] . "%' ORDER BY id desc";
        $query_exec = $this->db->query($query);
        $result = $query_exec->result_array();
        $writers = array();
        if (isset($result) && is_array($result) && count($result) > 0) {
            foreach ($result as $key => $value) {
                $writers[$key]['value'] = $value['code'];
                $writers[$key]['data'] = $value['name'];
            }

            $data['query'] = $_GET['query'];
            $data['suggestions'] = $writers;
            echo json_encode($data);
        } else {
            http_response_code(501);
        }
    }

}
