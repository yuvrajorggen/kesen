<?php

class Estimates extends MX_Controller {

    function __construct() {
        parent::__construct();
        // $this->output->enable_profiler(TRUE);       
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['employee'] = array();
        $data['language'] = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
            $data['metrix'][$value['id']] = $value['estimatecompany'];
        }
        $view = 'add_entry';

        $this->_display($view, $data);
    }

    function insert() {
//        echo '<pre>';
//        print_r($_POST);
//        exit;
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $sessiondata = $this->session->userdata('user');

        $data = $this->input->post('client_estimates');
        unset($data['samepassword']);

        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');
        unset($data['confirmpassword']);
        $eid = $this->gm->insert('client_estimates', $data);
//        echo '<pre>';
//        print_r($this->db->last_query());
//        exit;
        redirect(site_url('estimates/entry_list'));
    }

    function prints() {
        $this->load->helper('url');
        $page = $this->uri->segment(3);
        $this->load->model('Global_model', 'gm');
        $clause = ' WHERE id > 0 ';
        if (isset($_GET['code']) && !empty($_GET['code'])) {
            $clause = "  AND `eno` = '" . $_GET['code'] . "'";
        }
        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $clause .= " AND `status` IN (" . implode(',', $_GET['status']) . ") ";
        }

        $date = $this->input->get('date');
        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            if ($date != '') {
                $clause .= " AND `created_date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
            }
        }

        $query = "SELECT * FROM `client_estimates`  " . $clause . " ORDER BY `id`  DESC ";
        //echo $query;exit;
        $query_exec = $this->db->query($query);
        $result = $query_exec->result_array();
        $data['list'] = $result;
        $lang = $this->gm->get_data_list('client', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }
        $lang = $this->gm->get_data_list('clientcontacts', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }
        $lang = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['employee'][$value['id']] = $value['name'];
        }
        $this->load->helper('pdf_helper');
        $this->load->helper('url');
        $pdfname = 'media/pdfs/estimates--prints-' . '---' . rand(000, 999) . '.pdf';
        $path = save_pdf('estimates', $data, $pdfname, 'landscape');
        redirect(base_url() . $path);
    }

    function entry_list() {
//        echo '<pre>';
//        print_r($_GET);
//        
        $this->load->helper('url');
        $page = $this->uri->segment(3);
        $this->load->model('Global_model', 'gm');

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $clause = ' WHERE id > 0 ';
        if (isset($_GET['code']) && !empty($_GET['code'])) {
            $clause .= "  AND `eno` = '" . $_GET['code'] . "'";
        }
        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $clause .= " AND `status` IN (" . implode(',', $_GET['status']) . ") ";
        }

        $date = $this->input->get('date');
        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            if ($date != '') {
                $clause .= " AND `created_date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
            }
        }


        $query = "SELECT * FROM `client_estimates`  " . $clause . " ORDER BY `id`  DESC limit " . PER_PAGE . "  offset " . $offset . "";
        //echo $query;exit;
        $query_exec = $this->db->query($query);
        $result = $query_exec->result_array();



        $cquery = "SELECT count(id) as id FROM `client_estimates`  " . $clause;
        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->row_array();


        $data['list'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;


        $this->load->library('pagination');

        $config['base_url'] = site_url('estimates/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);

        $lang = $this->gm->get_data_list('client', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }
        $lang = $this->gm->get_data_list('clientcontacts', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }
        $lang = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($lang as $key => $value) {
            $data['employee'][$value['id']] = $value['name'];
        }


        $view = 'entry_list';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function update() {


        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');


        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');

        $id = $this->input->post('id');
        $data = $this->input->post('client_estimates');

        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');

        $this->gm->update('client_estimates', $data, 0, array('id' => $id));

        $message = 'Estimates updated successfully';
        $this->session->set_flashdata('success', $message);

        redirect("estimates/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function edit() {


        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');




        $data['client_estimates'] = $this->gm->get_selected_record('client_estimates', '*', $where = array('id' => $_GET['id']), array());

        $data['jobregister'] = $this->gm->get_selected_record('jobregister', '*', $where = array('estimatecompany' => $data['client_estimates']['metrix'], 'estimateno' => $data['client_estimates']['eno']), array());

        if (isset($data['jobregister']['id']) && $data['jobregister']['id'] > 0) {
            $this->session->set_flashdata('danger', 'You cannot modify estimates ' . $data['client_estimates']['eno'] . ' because  job register ' . $data['jobregister']['id'] . ' is already linked with estimate no. ');
            redirect('estimates/entry_list');
        }

        $result = $this->gm->get_data_list('clientcontacts', $where = array('client_id' => $data['client_estimates']['client_id']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }

        $data['mode'] = "update";
        $view = 'add_entry';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function delete() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $id = $_GET['id'];
        //$employee = $this->gm->get_selected_record('employee', 'firstname,lastname', $where = array('id' => $id), array());
        $set = array(
            'modified_by' => $sessiondata['id'],
            'modified_date' => date('Y-m-d H:i:s'),
            'status' => 2
        );
        $data['employee'] = $this->gm->update('employee', $set, $id);

        //$message = '<b>' . $employee['firstname'] . ' ' . $employee['lastname'] . ' ' . 'deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("employee/entry_list");
    }

    function get_city() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $state_id = $this->input->post('state_id');
        $city_data = $this->gm->get_data_list('cities', array('state_id' => $state_id), array(), array('id' => 'desc'), 'id,name', 0, array());

        echo json_encode($city_data);
    }

    function statuschange() {

        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');
        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');
        $data['status'] = $_GET['status'];
        if (isset($_GET['reason'])) {
            $data['reject_reason'] = $_GET['reason'];
        }
        if (!isset($_GET['status'])) {
            $data['status'] = 2;
        }

        $this->gm->update('client_estimates', $data, $_GET['id']);
        redirect("estimates/entry_list");
    }

    function get_staus() {
        $this->load->model('Global_model', 'gm');
        //echo '<pre>';
        //print_r($_POST);
        $jobregister = $this->input->post('jobregister');
        $result = $this->gm->get_selected_record('client_estimates', '*', $where = array('eno' => $jobregister['estimateno'], 'metrix' => $jobregister['estimatecompany'], 'status' => 1), array());
        //print_r($this->db->last_query());
        $data['result'] = $result;
        if (isset($result['id']) && $result['id'] > 0) {
            echo json_encode($data);
        } else {
            http_response_code(401);
        }
    }

    function check_unique() {
        $this->load->model('Global_model', 'gm');

        $client_estimates = $this->input->post('client_estimates');
        $result = $this->gm->get_selected_record('client_estimates', '*', $where = array('eno' => $client_estimates['eno'], 'metrix' => $client_estimates['metrix']), array());

        if (isset($result['id']) && $result['id'] > 0) {
            http_response_code(401);
        } else {
            echo json_encode($data);
        }
    }

    function details()
    {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $data['client_estimates'] = $this->gm->get_selected_record('client_estimates', '*', $where = array('id' => $_GET['id']), array());

        $data['jobregister'] = $this->gm->get_selected_record('jobregister', '*', $where = array('estimatecompany' => $data['client_estimates']['metrix'], 'estimateno' => $data['client_estimates']['eno']), array());

        if (isset($data['jobregister']['id']) && $data['jobregister']['id'] > 0) {
            $this->session->set_flashdata('danger', 'You cannot modify estimates ' . $data['client_estimates']['eno'] . ' because  job register ' . $data['jobregister']['id'] . ' is already linked with estimate no. ');
            redirect('estimates/entry_list');
        }

        $result = $this->gm->get_data_list('clientcontacts', $where = array('client_id' => $data['client_estimates']['client_id']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }

        $data['mode'] = "update";
        $view = 'preview_estimates';
       // echo '<pre>';
       // print_r($data);
       // exit;
        $this->_display($view, $data);
    }

}
