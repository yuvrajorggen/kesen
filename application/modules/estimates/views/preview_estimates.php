<style type="text/css">
    .details
    {
        padding-top: 8px;
    }
</style>
<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <h2>Details of Estimates</h2>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="pwd">Estimate No. : </label>
                                <div class="col-md-8 col-sm-8">
                                    <p class="details">
                                        <?php echo isset($client_estimates['eno']) ? $client_estimates['eno'] : ""; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="employee">Client Name : </label>
                                <div class="col-md-8 col-sm-8">
                                    <p class="details">
                                        <?php echo isset($client[$client_estimates['client_id']]) && $client[$client_estimates['client_id']] !='' ? $client[$client_estimates['client_id']] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4" for="role">Client Contact Person Name : </label>
                                <div class="col-md-8 col-sm-8">
                                    <p class="details">
                                        <?php echo isset($clientcontacts[$client_estimates['clientcontacts_id']]) && $clientcontacts[$client_estimates['clientcontacts_id']] !='' ? $clientcontacts[$client_estimates['clientcontacts_id']] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class = "control-label col-md-4 col-sm-4" for="contact">Headline : </label>
                                <div class = "col-md-8 col-sm-8">
                                    <p class="details">
                                        <?php echo isset($client_estimates['headline']) ? $client_estimates['headline'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class = "control-label col-md-4 col-sm-4" for="contact">Amount : </label>
                                <div class = "col-md-8 col-sm-8">
                                    <p>
                                        <?php echo isset($client_estimates['amount']) ? $client_estimates['amount'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <!-- <div class = "form-group">
                                <div class = "submit">
                                    <button type = "submit" class = "btn btn-default submit-btn text-center"tabindex="9">Save</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    var arrayFromPHP = <?php echo json_encode($metrix); ?>;



    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#employee_add").validate({
        ignore: [],
        rules: {
            'client_estimates[eno]': {
                required: true,
                rangelength: [1, 50],
                digitonly: true
            },
            'client_estimates[metrix]': {
                required: true,
            },
            'client_estimates[clientcontacts_id]': {
                required: true,
            },
            'client_estimates[client_id]': {
                required: true,
            },
            'client_estimates[amount]': {
                required: true,
                digitonly: true
            }, 'client_estimates[protocolno]': {
                required: true,
            },
            'employee[code]': {
                required: true,
                rangelength: [5, 5]
            }, 'employee[address]': {
                required: true,
                rangelength: [3, 500]
            }
        },
        messages: {
            'client_estimates[eno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        },
        submitHandler: function (form, event) {
            $("#loading").addClass("loading");

            $.ajax({
                url: '<?php echo base_url() . 'estimates/check_unique'; ?>',
                type: 'POST',
                data: $(form).serialize(),
                success: function (response) {
                    form.submit();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#loading").removeClass("loading");
                    event.preventDefault();
                    $.dialog({
                        title: 'Error Message',
                        content: 'Estimate number is already present.',
                        closeIcon: true,
                        buttons: {
                            cancelAction: function () {
                                alert('fsd');
                            }
                        }
                    });
                }
            });
        }
    });
</script>