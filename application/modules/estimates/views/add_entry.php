<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Estimates</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('estimates/update') : site_url('estimates/insert') ?>"> 
                        <input type="hidden" name="id" value="<?php echo isset($client_estimates['id']) ? $client_estimates['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-4" for="pwd">Estimate No.<span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8">
                                        <input 
                                        <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?>
                                            type="number" class="form-control" id="pwd" tabindex="5" name="client_estimates[eno]"  id="password" value="<?php echo isset($client_estimates['eno']) ? $client_estimates['eno'] : ""; ?>">
                                    </div>
                                    <label class="control-label col-md-1 col-sm-1 hidden" for="email">Metrix <span class="required">*</span></label>
                                    <div class="col-md-2 col-sm-2 hidden">
                                        <select name="client_estimates[metrix]" class="" tabindex="4" id="language">
                                            <?php
                                            $estimates = $this->config->item('estimate');
                                            foreach ($estimates as $key => $value) {
                                                ?>
                                                <option 
                                                <?php
                                                if (isset($client_estimates['metrix']) && $client_estimates['metrix'] == $key) {
                                                    echo 'selected';
                                                }
                                                ?>
                                                    value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                            

                           
                                <div class="form-group">

                                    <label class="control-label col-md-4 col-sm-4" for="employee">Client Name<span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8">
                                        <select <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> id="clientname" class="searchselected" tabindex="1" autofocus="on" name="client_estimates[client_id]" id="client">
                                            <option></option>
                                            <?php
                                            foreach ($client as $key => $value) {
                                                ?>
                                                <option  <?php
                                                if (isset($client) && $client_estimates['client_id'] == $key) {
                                                    echo 'selected';
                                                }
                                                ?>
                                                    value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-sm-4" for="role">Client Contact Person Name<span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8">
                                        <select id="clientcontactname" class="searchselected" tabindex="1" autofocus="on" id="client" name="client_estimates[clientcontacts_id]">
                                            <?php
                                            if (isset($clientcontacts) && is_array($clientcontacts) && count($clientcontacts) > 0) {
                                                foreach ($clientcontacts as $key => $value) {
                                                    ?>
                                                    <option  <?php
                                                    if (isset($client) && $client_estimates['clientcontacts_id'] == $key) {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                        value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>


                                        </select>
                                    </div>
                                </div>
                            

                            <div id="password_wrapper" class="">
                                <div class="form-group">
                                    <label class = "control-label col-md-4 col-sm-4" for="contact">Headline<span class = "required">*</span></label>
                                    <div class = "col-md-8 col-sm-8">
                                        <input type="text" name="client_estimates[headline]" class="form-control" tabindex="14" id="billno" value="<?php echo isset($client_estimates['headline']) ? $client_estimates['headline'] : ''; ?>">
                                    </div>

                                </div>
                            </div>
                            <div id="password_wrapper" class="">
                                <div class="form-group">
                                    <label class = "control-label col-md-4 col-sm-4" for="contact">Amount<span class = "required">*</span></label>
                                    <div class = "col-md-8 col-sm-8">
                                        <input type="text" name="client_estimates[amount]" class="form-control" tabindex="14" id="billno" value="<?php echo isset($client_estimates['amount']) ? $client_estimates['amount'] : ''; ?>">
                                    </div>

                                </div>
                            </div>
                            <div class = "form-group">
                                <div class = "submit">
                                    <button type = "submit" class = "btn btn-default submit-btn text-center"tabindex="9">Save</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    var arrayFromPHP = <?php echo json_encode($metrix); ?>;



    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#employee_add").validate({
        ignore: [],
        rules: {
            'client_estimates[eno]': {
                required: true,
                rangelength: [1, 50],
                digitonly: true
            },
            'client_estimates[metrix]': {
                required: true,
            },
            'client_estimates[clientcontacts_id]': {
                required: true,
            },
            'client_estimates[client_id]': {
                required: true,
            },
            'client_estimates[amount]': {
                required: true,
                digitonly: true
            }, 'client_estimates[protocolno]': {
                required: true,
            },
            'employee[code]': {
                required: true,
                rangelength: [5, 5]
            }, 'employee[address]': {
                required: true,
                rangelength: [3, 500]
            }
        },
        messages: {
            'client_estimates[eno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        },
        submitHandler: function (form, event) {
            $("#loading").addClass("loading");

            $.ajax({
                url: '<?php echo base_url() . 'estimates/check_unique'; ?>',
                type: 'POST',
                data: $(form).serialize(),
                success: function (response) {
                    form.submit();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#loading").removeClass("loading");
                    event.preventDefault();
                    $.dialog({
                        title: 'Error Message',
                        content: 'Estimate number is already present.',
                        closeIcon: true,
                        buttons: {
                            cancelAction: function () {
                                alert('fsd');
                            }
                        }
                    });
                }
            });
        }
    });
</script>