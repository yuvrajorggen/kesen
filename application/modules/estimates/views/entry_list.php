<?php
$roles = $this->config->item('roles');
?>
<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search estimates</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('estimates/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Estimate No</label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="role">Status</label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="contact">Date</label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="email">Email<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" value="<?php echo $this->input->get('code'); ?>" class="form-control" tabindex="1" name="code" id="name">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="active 3col" tabindex="2" name="status[]" id="role" multiple="">
                                    <option value="0">No Action</option>
                                    <option value="1">Approved</option>
                                    <option value="2">Rejected</option>

                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2 date-picker">                  
                                <input id="date" class="form-control"  type="text" tabindex="2" name="date" 
                                       value="<?php echo isset($_GET['date']); ?>"   
                                       <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> >       
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <input type="email" class="form-control" tabindex="4" id="email">
                            </div>
                            <input id="btn-search" tabindex="4" type="submit" class="btn btn-default submit-btn" value="Search">
                            <!--<input tabindex="4" type="reset" value="Clear" class="btn btn-default clear-btn">
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Employee" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>-->
                            <a tabindex="6" class="btn btn-default view-btn" title="View All Employees" href="<?php echo site_url('estimates/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default add-btn"  title="Add Employee" href="<?php echo site_url('estimates'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add estimates</a>
                            <?php if ($_GET['code'] == '') { ?>
                                <a tabindex="7" target="_blank" class="btn btn-default add-btn"  title="Add Employee" href="<?php echo base_url() . 'estimates/prints?' . $_SERVER['QUERY_STRING']; ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Print</a>
                            <?php } ?>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>estimates List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Estimates : <?php echo $total; ?></span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Date</th>
                            <th>Estimate No</th>
                            <th>Our Company Name </th>
                            <th>Client Name</th>
                            <th>Client Contact Person Name</th>
                            <th>Client Contact Person No.</th>
                            <th>Protocol No</th>
                            <th>Created By</th>
                            <th>Status</th>
                            <th>Status Actions</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php
                    $estimates = $this->config->item('estimate');
                    foreach ($list as $key => $value) {
                        ?>
                        <tbody> 

                            <tr>
                                <td><?php echo date('d M Y H:i a', strtotime($value['created_date'])); ?></td>
                                <td><?php echo $value['eno']; ?></td>
                                <td><?php echo $estimates[$value['metrix']]; ?></td>
                                <td><?php echo $client[$value['client_id']]; ?></td>
                                <td><?php echo isset($clientcontacts[$value['clientcontacts_id']]['name']) ? $clientcontacts[$value['clientcontacts_id']]['name'] : "Client contact was deleted."; ?></td>
                                <td>
                                    <span>M:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['contactno']) ? $clientcontacts[$value['clientcontacts_id']]['contactno'] : ""; ?><br>
                                    <span>L:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['landline']) ? $clientcontacts[$value['clientcontacts_id']]['landline'] : ""; ?></td>
                                <td><?php echo $value['headline']; ?></td>
                                <td><?php echo $employee[$value['created_by']]; ?></td>

                                <td><?php
                                    $status = 0;
                                    if ($value['status'] == 1) {
                                        echo 'Approved';
                                        $status = 2;
                                    }
                                    if ($value['status'] == 2) {
                                        $status = 1;
                                        echo 'Disapproved';
                                        echo '<br>Reason:';
                                        if ($value['reject_reason'] == 1) {
                                            echo 'Price High';
                                        } elseif ($value['reject_reason'] == 2) {
                                            echo 'Quality Issue';
                                        } elseif ($value['reject_reason'] == 3) {
                                            echo 'Delivery Issue';
                                        } else {
                                            echo 'Other';
                                        }
                                    }
                                    ?>

                                </td>
                                <td>
                                    <?php
                                    if ($value['status'] == 2 || $value['status'] == 0) {
                                        ?>
                                        <a title="Delete Employee" href="javascript:void(0)" onclick="del('Approved', 'btn-green', 'Kindly Confirm', 'Are you sure you want to Approved ?', '<?php echo base_url('estimates/statuschange?id=') . $value['id'] . '&status=1'; ?>');"><i class="fa fa-check fa-lg" aria-hidden="true"></i>&nbsp;Approved&nbsp;</a>
                                        <?php
                                    }
                                    if ($value['status'] == 1 || $value['status'] == 0) {
                                        ?>
                                        <a title="Delete Employee" href="javascript:void(0)" onclick="reject('Rejected', 'btn-red', 'Kindly Confirm', 'Are you sure you want to Reject ?', '<?php echo base_url('estimates/statuschange?id=') . $value['id']; ?>', '<?php echo $value['id']; ?>');"><i class="fa fa-times fa-lg" aria-hidden="true"></i>&nbsp;Rejected&nbsp;</a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <a title="Update Estimates <?php echo $value['eno']; ?>" href="<?php echo site_url('estimates/edit?id=') . $value['id']; ?>"><i class="fa fa-pencil fa-sm" aria-hidden="true"></i>&nbsp;Update&nbsp;</a>
                                    <a title="Preview Estimates <?php echo $value['eno']; ?>" href="<?php echo site_url('estimates/details?id=') . $value['id']; ?>"><i class="fa fa-file fa-sm" aria-hidden="true"></i>&nbsp;Details&nbsp;</a>
                                </td>
                            </tr>  <?php
                        }
                        ?>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#date').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
<?php if (isset($_GET['date'])) { ?>
            $('#date').val('');
<?php } ?>

    });
</script>