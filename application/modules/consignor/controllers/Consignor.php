<?php

class Consignor extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function insert() {
        $this->load->helper('url');
        //echo '<pre>';
        //print_r($_POST);
        //exit;
        $this->load->model('Global_model', 'gm');
        $data = $this->input->post('consignor');
        $this->gm->insert('consignor', $data);
        redirect(site_url('consignor/entry_list'));
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $this->load->model('Global_model', 'gm');

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }

        $query = " SELECT * FROM `jobregister` WHERE `status` = 1 ";
        $where = "";
        if ($_GET['bill'] == 1) {
            $where .= " AND `billno` > 0  ";
        } else {
            $where .= " AND `billno` = 0 ";
        }

        if (isset($_GET['jobno']) && $_GET['jobno']) {
            $where .= " AND `id` = " . $_GET['jobno'];
        }
        if (isset($_GET['client']) && $_GET['client']) {
            $where .= " AND `client_id` IN( " . implode(',', $_GET['client']) . ")";
        }
        $date = $this->input->get('date');
        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
        }
        if ($date != '') {
            $where .= " AND `date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
        }

        $order = " ORDER BY ID DESC  LIMIT " . PER_PAGE . " OFFSET " . $offset;
        $final = $query . $where . $order;
        $query_exec = $this->db->query($final);
        $data['result'] = $query_exec->result_array();

        $query_exec = $this->db->query($query . $where);
        $count['result'] = $query_exec->num_rows();
        

        $view = 'entry_list';
        

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('clientcontacts', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {

            $data['employeeall'][$value['id']] = $value;
        }
        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 0, array());

        $this->load->library('pagination');

        $config['base_url'] = site_url('consignor/entry_list');
        $config['total_rows'] = $count['result'];

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);

//        echo '<pre>';
//        print_r($data);
//        exit;

        $this->_display($view, $data);
    }

    function entry_list_unbilled() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $this->load->model('Global_model', 'gm');
        $data['list'] = $this->gm->get_data_list('consignor', array(), array(), array('id' => 'desc'), '*', 30, array());
        $view = 'entry_list_unbilled';
        $this->load->library('pagination');
        $this->_display($view, $data);
    }

    function timeline_report() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        if (isset($_GET['date'])) {

            $basequery = "SELECT * FROM `jobcard` jb LEFT OUTER JOIN `jobregister` jr on jb.jobregister_id = jr.id WHERE jb.sentdate > jr.deliverydate OR jb.sentdate IS NULL AND jb.task IS NOT NULL ";

            $date = $this->input->get('date');
            $datearray = explode('-', $date);

            if (isset($datearray[1]) && is_array($datearray)) {
                $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
                $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
            }
            if ($date != '') {
                $where .= " AND jr.date BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
            }
            $final = $basequery . $where . ' order by jb.jobregister_id desc ';

            $query_exec = $this->db->query($final);
            $data['result'] = $query_exec->result_array();


            $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
            foreach ($result as $key => $value) {
                $data['client'][$value['id']] = $value;
            }

            $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 30, array());
            foreach ($result as $key => $value) {
                $data['employeeall'][$value['id']] = $value;
            }
            $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
            foreach ($result as $key => $value) {
                $data['language'][$value['id']] = $value['name'];
            }

            $this->load->helper('pdf_helper');
            $pdfname = 'media/pdfs/timereport-' . rand(0000, 9999) . '.pdf';
            $path = save_pdf('timeline', $data, $pdfname, 'landscape');
            redirect(base_url() . $path);
        } else {

            $data = array();
            $this->_display('timeline_report', $data);
        }
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function prints() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $this->load->model('Global_model', 'gm');

        $query = " SELECT * FROM `jobregister` WHERE `status` = 1 ";
        $where = "";
        if ($_GET['bill'] == 1) {
            $where .= " AND `billno` > 0  ";
        } else {
            $where .= " AND `billno` = 0 ";
        }

        if (isset($_GET['jobno']) && $_GET['jobno']) {
            $where .= " AND `id` = " . $_GET['jobno'];
        }
        if (isset($_GET['client']) && $_GET['client']) {
            $where .= " AND `client_id` IN( " . implode(',', $_GET['client']) . ")";
        }
        $date = $this->input->get('date');
        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
        }
        if ($date != '') {
            $where .= " AND `date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
        }

        $order = " ORDER BY ID DESC  ";
        $final = $query . $where . $order;
        $query_exec = $this->db->query($final);
        $data['result'] = $query_exec->result_array();

        $view = 'entry_list';
        $this->load->library('pagination');

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('clientcontacts', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 3000, array());
        foreach ($result as $key => $value) {

            $data['employeeall'][$value['id']] = $value;
        }
        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 3000, array());

//        echo '<pre>';
//        print_r($data);
//        exit;
        //$this->load->view('job_billed');
        $this->load->helper('pdf_helper');
        $this->load->helper('url');

        $pdfname = 'media/pdfs/report-' . rand(0000, 9999) . '.pdf';
        if ($_GET['bill'] == 1) {
            $view = "job_billed";
        } else {
            $view = "job_billed";
        }

        $path = save_pdf($view, $data, $pdfname, 'landscape');
        redirect(base_url() . $path);
    }

    function timeline() {
        $this->load->view('timeline');
    }

}
