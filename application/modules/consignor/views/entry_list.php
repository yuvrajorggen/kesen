<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignor/entry_list'); ?>" method="GET">
                        <input type="hidden" value="<?php echo $_GET['bill']; ?>" name="bill">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Client Name<span class="required">*</span></label>

                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="id" class="form-control" tabindex="1" name="jobno">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name="date"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select class="active 3col " name="client[]" multiple="" tabindex="" autofocus="on" id="client">

                                    <?php
                                    foreach ($client as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <br />
                            <br />
                            <div class="pull-right">
                                <input type="submit" value="Search">
                                <a tabindex="6" class="btn btn-default view-btn" title="View All" href="<?php echo site_url('consignor/entry_list?bill=') . $_GET['bill']; ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                                <a tabindex="7" class="btn btn-default btn-info"  title="Print" href="<?php
                                echo site_url('consignor/prints?') . $_SERVER['QUERY_STRING'];
                                ;
                                ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3><?php
                    if ($_GET['bill'] == 1) {
                        echo 'Job Billed';
                    } else {
                        echo 'Job Unbilled';
                    }
                    ?></h3>
                <div class="total">
                </div>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Date</th>
                            <th>Job No.</th>
                            <th>Client Name</th>
                            <th>Client Contact Person Name</th>
                            <th>Client Contact Person No.</th>
                            <th>Handled By</th>
                            <th>Our Company Name</th>
                            <th>Accountant</th>
                            <th>Bill No</th>
                            <th>Bill Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $estimates = $this->config->item('estimate');
                        foreach ($result as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo date('d M Y', strtotime($value['date'])); ?></td>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $client[$value['client_id']]['name']; ?></td>
                                <td><?php echo $clientcontacts[$value['clientcontacts_id']]['name']; ?></td>
                                <td><span>M:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['contactno']) ? $clientcontacts[$value['clientcontacts_id']]['contactno'] : ""; ?><br>
                                    <span>L:</span><?php echo isset($clientcontacts[$value['clientcontacts_id']]['landline']) ? $clientcontacts[$value['clientcontacts_id']]['landline'] : ""; ?></td>
                                <td><?php echo $employeeall[$value['handledby']]['name']; ?></td>
                                <td><?php echo $estimates[$value['estimatecompany']]; ?></td>
                                <td><?php echo $employeeall[$value['accountant']]['name']; ?></td>
                                <td><?php echo $value['billno']; ?></td>
                                <td><?php
                                    if ($value['invoicedate'] != '0000-00-00') {
                                        echo date('d M Y', strtotime($value['invoicedate']));
                                    }
                                    ?></td>                                
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>                        
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('.date-picker').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $('.date-picker').val('');

    });

</script>