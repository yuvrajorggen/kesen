<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Timeline</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignor/timeline_report'); ?>" method="GET">
                        <div class="form-group label_fields">
                            
                            <label class="control-label col-md-2 col-sm-2">Date<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            
                            <div class="col-md-2 col-sm-2">
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" name="date"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                            </div>
                            
                            <input type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content hidden">
                <h3>Timeline List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Estimate Delivery Date</th>
                            <th>Actual Delivery Date</th>
                            <th>Difference</th>
                            <th>Client Name</th>
                            <th>Handled By</th>
                            <th>Delay Reason</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1234</td>
                            <td>12/06/2017</td>
                            <td>16/06/2017</td>
                            <td>1234</td>
                            <td>John Doe</td>
                            <td>Albert</td>
                            <td>Lorem ipsum dolor sit amet.</td>
                            <td>English</td>
                            <td>
                                <a title="Update Timeline" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Timeline"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                        </tr>
                    </tbody>                        
                </table> 
         
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        $('.date-picker').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });

</script>