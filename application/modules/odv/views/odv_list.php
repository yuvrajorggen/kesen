<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job Register</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('odv/odv_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2" for="job">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2" for="date">Date<span class="required">*</span></label>
                            <label class="control-label col-md-1 col-sm-1"></label>

                            <label class="control-label col-md-2 col-sm-2 hidden">Handled By<span class="required">*</span></label>
                          <!--  <label class="control-label col-md-2 col-sm-2" for="writer">Writer's Name<span class="required">*</span></label>-->
                            <label class="control-label col-md-2 col-sm-2" for="client">Client Name<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input type="number" id="job" class="form-control" tabindex="1" name="jobno" value="<?php echo isset($_GET['jobno']) ? $_GET['jobno'] : ''; ?>" autofocus="on">
                            </div>
                            <div class="col-md-2 col-sm-2 date-picker">                  
                                <input id="date" class="form-control"  type="text" tabindex="2" name="date" 
                                       value="<?php echo isset($_GET['date']); ?>"   
                                       <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> >       


                                <i class="glyphicon glyphicon-calendar fa fa-calendar caret"></i>
                            </div>



                            <!--<div class="col-md-2 col-sm-2">
                                <select class=" 3col active" tabindex="4" id="writer" name="writer[]" multiple="">
                            <?php
                            $temp = array_flip($_GET['writer']);
                            foreach ($writer as $key => $value) {
                                ?>
                                                                                            <option 
                                <?php
                                if (is_array($temp) && array_key_exists($key, $temp)) {
                                    echo 'selected';
                                }
                                ?>
                                                                                                value="<?php echo $key; ?>"><?php echo ucwords($value['name']); ?></option>>
                                <?php
                            }
                            ?>
                                </select>
                            </div>-->
                            <div class="col-md-2 col-sm-2">
                                <select class=" 3col active" tabindex="5" id="client" name="client[]" multiple="">
                                    <?php
                                    $temp = array_flip($_GET['client']);
                                    foreach ($client as $key => $value) {
                                        ?>
                                        <option <?php
                                        if (is_array($temp) && array_key_exists($key, $temp)) {
                                            echo 'selected';
                                        }
                                        ?>
                                            value="<?php echo $key; ?>"><?php echo ucwords($value['name']); ?></option>>
                                            <?php
                                        }
                                        ?>

                                </select>
                            </div>
                            <input type="submit" value="Search" class="btn btn-default submit-btn">

                            <a tabindex="7" class="btn btn-default view-btn" title="View All Job Register" href="<?php echo site_url('odv/odv_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="8" class="btn btn-default add-btn"  title="Create Job Register" href="<?php echo site_url('odv/add'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Create Job Register</a>

                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Job Register List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Job : <?php echo $total; ?></span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">
                    <thead>
                        <tr class="table_heading">
                            <th>Job No.</th>
                            <th>Date</th>
                            <th>Word/Unit</th>
                            <th>Handled By</th>
                            <th>Client's Name</th>

                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($list as $key => $value) {
                        ?>
                        <tbody>
                            <tr <?php
                            if ($value['status'] == 2) {
                                echo 'style="background: #ff6a5c;"';
                            }
                            ?>>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo date('d M Y', strtotime($value['date'])); ?></td>
                                <td><?php echo $value['wordsunit']; ?></td>
                                <td><?php echo $employee[$value['handledby']]['name']; ?></td>
                                <td><?php echo $client[$value['client_id']]['name']; ?></td>

                                <td>
                                    <a target="_blank" title="View Job Register" href="<?php echo base_url('odv/printjobregister') . '?id=' . $value['id']; ?>"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                    <a title="Update Job Register" href="<?php echo base_url('odv/edit') . '?id=' . $value['id']; ?>"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                    <?php if ($_SESSION['user']['role'] == 1 || $_SESSION['user']['role'] == 2 || $_SESSION['user']['role'] == 5) { ?>
                                        <a title="Cancel Job Register" onclick="del('Cancel', 'btn-red', 'Kindly Confirm', 'Are you sure you want to Cancel job no: <?php echo $value['id']; ?>?', '<?php echo base_url('odv/delete?id=') . $value['id']; ?>');"><i class="fa fa-times fa-lg" aria-hidden="true"></i>&nbsp;Cancel&nbsp;</a>
                                    <?php } ?>
                                    <a target="_blank" title="Print Job Register" href="<?php echo base_url('odv/printjobregister') . '?id=' . $value['id']; ?>"><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>
                                    <?php
                                    if ($clientcontacts[$value['clientcontacts_id']]['contactno'] != '') {
                                        $ordresms = "Thank you for your valued order. Your job code " . $value['id'] . " will be handled by " . $employee[$value['handledby']]['name'] . " available on " . $employee[$value['handledby']]['landline'] . ".";
                                        $completionsms = "Job No. " . $value['id'] . " completed. Kindly confirm. Thanking you.";
                                        $billsms = "Job No. " . $value['id'] . " Bill sent. Kindly revert in case of query within 7 working days. Thank You.";
                                        $billsms = "Invoice No. " . $value['billno'] . " dated " . date('d M Y', strtotime($value['invoicedate'])) . " for Job No. " . $value['id'] . " sent. Kindly revert in case of query within 7 working days. Thank you.";
                                        ?>
                                        <a title="SMS Order SMS" href="javascript:void(0);"  onclick="del('Send SMS', 'btn-green', 'Are you sure you want to send SMS?', '<?php echo $ordresms; ?>', '<?php echo base_url('odv/sms?sms=order&id=') . $value['id'] . '&content=' . urlencode($ordresms) . '&contactno=' . $clientcontacts[$value['clientcontacts_id']]['contactno']; ?>');"><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;Order SMS&nbsp;</a>
                                        <a title="SMS Job Register" href="javascript:void(0);"   onclick="del('Send SMS', 'btn-green', 'Are you sure you want to send SMS?', '<?php echo $completionsms; ?>', '<?php echo base_url('odv/sms?sms=order&id=') . $value['id'] . '&content=' . urlencode($completionsms) . '&contactno=' . $clientcontacts[$value['clientcontacts_id']]['contactno']; ?>');"><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;Completion SMS&nbsp;</a>
                                        <a title="SMS Job Register" href="javascript:void(0);"   onclick="del('Send SMS', 'btn-green', 'Are you sure you want to send SMS?', '<?php echo $billsms; ?>', '<?php echo base_url('odv/sms?sms=order&id=') . $value['id'] . '&content=' . urlencode($billsms) . '&contactno=' . $clientcontacts[$value['clientcontacts_id']]['contactno']; ?>');"><i class="fa fa-comment fa-lg" aria-hidden="true"></i>&nbsp;Bill SMS&nbsp;</a>
                                        <?php
                                    }
                                    if ($clientcontacts[$value['clientcontacts_id']]['email'] != '') {
                                        ?>
                                        <a title="Email Reply Letter" 
                                        <?php if ($value['reference_email_status'] == 2) { ?>
                                               href="<?php echo base_url('odv/emailreplyletter?id=') . $value['id']; ?>"
                                           <?php } else { ?>
                                               onclick="email_reply_letter('Send Email', 'btn-Green', 'Kindly Confirm', 'Are you sure you want to Send ?', '<?php echo base_url('odv/printjobregister?id=') . $value['id']; ?>', '<?php echo $value['id']; ?>');"
                                           <?php } ?>      ><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Email Reply Letter&nbsp;</a>  
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#date').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
<?php if (!isset($_GET['date'])) { ?>
            $('#date').val('');
<?php } ?>


       
    });
</script>

<!-- /Select2 -->