<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Register</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('odv/update') : site_url('odv/insert'); ?>">
                        <input type="hidden" name="id" value="<?php echo isset($jobregister['id']) ? $jobregister['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <p class="note2">NOTE : Job Card No. will be automatically created.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="estimate">Metrix 
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select name="jobregister[estimatecompany]" class="" tabindex="4" id="language">
                                                <?php
                                                $estimates = $this->config->item('estimate');
                                                foreach ($estimates as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobregister['estimatecompany']) && $key == $jobregister['estimatecompany']) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="number" name="jobregister[estimateno]" class="form-control" tabindex="14" id="estimateno" placeholder="Estimate No" value="<?php echo (isset($jobregister['estimateno']) && ($jobregister['estimateno'] > 0)) ? $jobregister['estimateno'] : ''; ?>">
                                        </div>
                                        <a href="javascript:void(0);" class="btn btn-default submit-btn" onclick="pull_details()">Pull Details</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="clientname" class="control-label col-md-2 col-sm-2 col-sm-offset-6" form="client">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> id="clientname" class="searchselected" tabindex="1" autofocus="on" name="jobregister[client_id]" id="client">
                                                <option></option>
                                                <?php
                                                foreach ($client as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobregister['client_id']) && $key == $jobregister['client_id']) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="clientname" class="control-label col-md-2 col-sm-2 col-sm-offset-6" form="client">Client Contact Person Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select id="clientcontactname" class="searchselected" tabindex="1" autofocus="on" id="client" name="jobregister[clientcontacts_id]">
                                                <?php
                                                foreach ($clientcontacts as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobregister['clientcontacts_id']) && $key == $jobregister['clientcontacts_id']) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                    ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="handled">Handled By<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="searchselected" tabindex="3" id="handled" name="jobregister[handledby]">

                                                <?php
                                                foreach ($employeeall as $key => $value) {
                                                    ?>
                                                    <option 
                                                    <?php
                                                    if (isset($jobregister['handledby']) && $key == $jobregister['handledby']) {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                        value="<?php echo $key; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="handled">Admin<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="searchselected" tabindex="3" id="handled" name="jobregister[admin]">

                                                <?php
                                                foreach ($employee[2] as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobregister['admin']) && $key == $jobregister['admin']) {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                        value="<?php echo $key; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="handled">Accountant<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="searchselected" tabindex="3" id="handled" name="jobregister[accountant]">

                                                <?php
                                                foreach ($employee[3] as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobregister['accountant']) && $key == $jobregister['accountant']) {
                                                        echo 'selected';
                                                    }
                                                    ?> value="<?php echo $key; ?>"><?php echo $value['name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="details">Other Details
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <textarea class="form-control" tabindex="9" id="details" name="jobregister[otherdetails]">
                                                <?php echo isset($jobregister['otherdetails']) ? $jobregister['otherdetails'] : ''; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2" for="language">Language<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select multiple="multiple" class=" form-control 3col active" tabindex="4" id="languages" name="jobreglang[]">
                                                <?php
                                                foreach ($language as $key => $value) {
                                                    ?>
                                                    <option <?php
                                                    if (isset($jobreglang)) {
                                                        if (array_key_exists($key, $jobreglang)) {
                                                            echo 'selected';
                                                        }
                                                    }
                                                    ?>
                                                        value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="date" name="jobregister[date]" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> readonly value="<?php echo isset($jobregister['date']) ? date('m/d/Y', strtotime($jobregister['date'])) : ''; ?>"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Headline<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" name="jobregister[headline]" class="form-control" tabindex="14" id="headline"  value="<?php echo isset($jobregister['headline']) ? $jobregister['headline'] : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Protocol No.
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" name="jobregister[protocolno]" class="form-control" tabindex="14" id="billno"  value="<?php echo isset($jobregister['protocolno']) ? $jobregister['protocolno'] : ''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn" tabindex="10">Create Job Card</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>

<script>
                                            $(document).ready(function () {

                                                $('.date-picker').daterangepicker({
                                                    "singleDatePicker": true,
                                                    "showDropdowns": true,
                                                    "showWeekNumbers": true,
                                                    "showISOWeekNumbers": true,
                                                    "autoApply": true,
                                                    "alwaysShowCalendars": true,
                                                    "calender_style": "picker_4",
                                                });


                                            });


                                            function pull_details() {
                                                $.ajax({
                                                    url: '<?php echo base_url() . 'estimates/get_staus'; ?>',
                                                    type: 'POST',
                                                    data: $("#odv_add").serialize(),
                                                    beforeSend: function () {
                                                        $("#loading").addClass("loading");
                                                    },
                                                    success: function (response) {
                                                        var obj = JSON.parse(response);
                                                        $("#clientname").val(obj.result.client_id).trigger('change');
                                                        $("#headline").val(obj.result.headline);
                                                        setTimeout(function () {
                                                            $("#loading").removeClass("loading");
                                                            $("#clientcontactname").val(obj.result.clientcontacts_id).trigger('change');
                                                        }, 3000);
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        $("#loading").removeClass("loading");
                                                        $.dialog({
                                                            title: 'Error Message',
                                                            content: 'Estimate number is not present or it is unapproved.',
                                                            closeIcon: true,
                                                            buttons: {
                                                                cancelAction: function () {
                                                                    alert('fsd');
                                                                }
                                                            }
                                                        });
                                                    },
                                                    complete: function (response) {

                                                    }
                                                });
                                            }

</script>


<script>
    var arrayFromPHP = <?php echo json_encode($metrix); ?>;
    
    
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#odv_add").validate({
        ignore: [],
        rules: {
            'jobregister[client_id]': {
                required: true,
            },
            'jobregister[clientcontacts_id]': {
                required: true,
            },
            'jobregister[handledby]': {
                required: true,
            },
            'jobregister[writer_id]': {
                required: true,
            }, 'jobregister[otherdetails]': {
            },
            'jobreglang[]': {
                required: true,
            }, 'jobregister[estimatecompany]': {
                required: true,
            }, 'jobregister[estimateno]': {
            },
            'jobregister[headline]': {
                required: true,
                rangelength: [3, 500],
            },
            'jobregister[protocolno]': {
                rangelength: [3, 500]
            },
            'jobregister[admin]': {
                required: true,
            },
            'jobregister[accountant]': {
                required: true,
            }
        },
        messages: {
            'employee[name]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        },
        submitHandler: function (form, event) {
            $("#loading").addClass("loading");

            var estimateno = $("#estimateno").val();
            if (estimateno == '') {
                form.submit();
            } else {
                $.ajax({
                    url: '<?php echo base_url() . 'estimates/get_staus'; ?>',
                    type: 'POST',
                    data: $(form).serialize(),
                    success: function (response) {
                        form.submit();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#loading").removeClass("loading");
                        event.preventDefault();
                        $.dialog({
                            title: 'Error Message',
                            content: 'Estimate number is not present or it is unapproved.',
                            closeIcon: true,
                            buttons: {
                                cancelAction: function () {
                                    alert('fsd');
                                }
                            }
                        });
                    }
                });
            }
        }
    });
</script>