<?php

class Odv extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->add();
    }

    function add() {
        $this->load->helper('url');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
            $data['metrix'][$value['id']] = $value['estimatecompany'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 0, array());

        foreach ($result as $key => $value) {
            $data['employee'][$value['role']][$value['id']] = $value;
            $data['employeeall'][$value['id']] = $value;
        }
        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 0, array());

        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }

        $view = 'odv_entry_add';
        $this->_display($view, $data);
    }

    function insert() {
//        echo '<pre>';
//        print_r($_POST);
//        exit;
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        $this->load->model('Global_model', 'gm');

        $data = $this->input->post('jobregister');
        $data['date'] = date('Y-m-d', strtotime($data['date']));
        $data['created_date'] = date('Y-m-d H:i:s');
        $data['created_by'] = $sessiondata['id'];
//        echo '<pre>';
//        print_r($data);
//        exit;
        $id = $this->gm->insert('jobregister', $data);

        $jobreglang = $this->input->post('jobreglang');

        foreach ($jobreglang as $key => $value) {
            $temp = array(
                'jobregister_id' => $id,
                'language_id' => $value
            );
            $this->gm->insert('jobreglang', $temp);
        }
        $url = base_url() . 'shipper/entry?id=' . $id;

        redirect($url);
    }

    function odv_list() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();

        $this->load->model('Global_model', 'gm');

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        // created for searching from the list of data 
        $sql = "SELECT * FROM `jobregister` where (`status` = 1 OR `status` = 2) ";
        $jobno = $this->input->get('jobno');
        $date = $this->input->get('date');
        $writer = $this->input->get('writer');
        $client = $this->input->get('client');

        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
        }

        $where = "";
        if ($jobno != '') {
            $where .= " AND `id` = '" . $jobno . "' ";
        }
        if ($date != '') {
            $where .= " AND `date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
        }
        if ($writer != '') {
            $where .= " AND  writer_id IN (" . implode(',', $writer) . ")";
        }
        if ($client != '') {
            $where .= " AND  client_id IN (" . implode(',', $client) . ")";
        }
        $order = "ORDER BY `id`  DESC limit " . PER_PAGE . " offset " . $offset . "";
        $finalsql = $sql . $where . $order;

        $query_exec = $this->db->query($finalsql);
        $result = $query_exec->result_array();


        if (isset($result) && count($result) > 0) {
            $cquery = "SELECT count(id) as id FROM `jobregister`  where (status = 1 or status = 2) " . $where;
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        }


        $data['list'] = $result;

        $data['total'] = isset($count['id']) ? $count['id'] : 0;
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('odv/odv_list');
        $config['total_rows'] = isset($count['id']) ? $count['id'] : 0;

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);


        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('clientcontacts', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }

//        echo '<pre>';
//        print_r($data);
//        exit;
        $view = 'odv_list';
        //pagination end
        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');

        $id = $this->input->post('id');

        $data = $this->input->post('jobregister');
        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');
        $data['date'] = date('Y-m-d', strtotime($data['date']));
        $this->gm->update('jobregister', $data, 0, array('id' => $id));

        $query = "DELETE FROM `jobreglang` WHERE `jobregister_id` = " . $id;
        $query_exec = $this->db->query($query);

        $jobreglang = $this->input->post('jobreglang');

        foreach ($jobreglang as $key => $value) {
            $temp = array(
                'jobregister_id' => $id,
                'language_id' => $value
            );
            $this->gm->insert('jobreglang', $temp);
        }




        redirect("odv/odv_list");
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['jobregister'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']), array());
        $data['mode'] = "update";

        $result = $this->gm->get_data_list('client', $where = array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('clientcontacts', $where = array('client_id' => $data['jobregister']['client_id']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('employee', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['role']][$value['id']] = $value;
            $data['employeeall'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['jobregister']['id']), array(), array('id' => 'desc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['jobreglang'][$value['language_id']] = $value;
        }

//        echo '<pre>';
//        print_r($data);
//        exit;
        $view = 'odv_entry_add';
        $this->_display($view, $data);
    }

    function delete() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');

        $set = array(
            'modified_by' => $sessiondata['id'],
            'modified_date' => date('Y-m-d H:i:s'),
            'status' => 2
        );
        $this->gm->update('jobregister', $set, 0, array('id' => $_GET['id']));

        if ($sessiondata['email'] != '') {
            $this->load->helper('email');
            $body = "Job Register No: " . $_GET['id'] . " is cancel by " . $sessiondata['name'] . ' ' . date('d M Y H:i A');
            send_email($sessiondata['email'], 'Job No ' . $_GET['id'] . ' cancel', $body, 'kesen@kesen.in', $reply_email = "", $cc_email = "klanguagebureau@gmail.com", $attachment = array());
        }

        $message = '<b>' . 'Record deleted successfully' . '</b>';
        $this->session->set_flashdata('w', $message);

        redirect("odv/odv_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function upload_odv() {
        $this->load->helper('url');
        $data = array();
        $this->_display('odv_upload', $data);
    }

    function getclientcontactlist() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $result = $this->gm->get_data_list('clientcontacts', $where = array('client_id' => $_POST['q']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());

        if (isset($result) && is_array($result) && count($result) > 0) {
            foreach ($result as $key => $value) {
                $data[$value['id']] = $value['name'];
            }
            echo json_encode($data);
        } else {
            http_response_code(204);
        }
        exit;
    }

    function odv_upload() {
        $this->load->model('Global_model', 'gm');
        $file_name = 'csv_import_' . date('Y-m-d_H-i-s', time());
        $config['upload_path'] = './media/csv/bulk_import/';
        $config['allowed_types'] = 'application/octet-stream|CSV|csv';
        $config['file_name'] = $file_name;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());

            $row = 1;
            if (($handle = fopen($data['upload_data']['full_path'], "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $num = count($data);
                    //echo '<pre>';
                    //print_r($data);
                    unset($insert);
                    if ($row != 1) {
                        $insert = array(
                            "name" => $data[0],
                            "city" => $data[1],
                            "state" => $data[2],
                            "country" => $data[3],
                            "pincode" => $data[4],
                            "code" => $data[5]
                        );
                        $this->gm->insert('odv', $insert);
                    }


                    $row++;
                }
                fclose($handle);
            }
        }

        $this->load->helper('url');
        redirect(site_url('odv/odv_list'));
    }

    function printjobregister() {
        $this->load->model('Global_model', 'gm');
        $data['result'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']), array());
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['result']['client_id']), array());
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['result']['clientcontacts_id']), array());
        $result = $this->gm->get_data_list('employee', array(), array(), array(), '*', 0, array());
        $data['admin'] = $this->gm->get_selected_record('employee', '*', $where = array('id' => $data['result']['admin']), array());
        $data['accountant'] = $this->gm->get_selected_record('employee', '*', $where = array('id' => $data['result']['accountant']), array());

        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
        }


        $query = "SELECT * FROM `jobreglang` WHERE `jobregister_id` = " . $_GET['id'] . " ORDER BY `id` ASC";
        $query_exec = $this->db->query($query);
        $jobreglang = $query_exec->result_array();


        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $data['jobcard'] = array();
        $jobcard = $this->gm->get_data_list('jobcard', array('jobregister_id' => $data['result']['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($jobcard as $key => $value) {
            $data['jobcard'][$value['language_id']][$value['type']] = $value;
        }
        $this->load->helper('pdf_helper');
        $this->load->helper('url');

        $pdfname = 'media/pdfs/job-register-' . $data['result']['id'] . '.pdf';
        $path = save_pdf('job_register', $data, $pdfname, 'landscape');
        redirect(base_url() . $path);
    }

    function savedate() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        if ($_POST['id'] > 0 && $_POST['date'] != '') {
            $this->gm->update('jobregister', array('reference_email_dated' => $_POST['date']), 0, array('id' => $_POST['id']));
            redirect(base_url('odv/emailreplyletter?id=' . $_POST['id']));
        } else {
            redirect(base_url('odv/odv_list'));
        }
    }

    function emailreplyletter() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['result'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']), array());
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['result']['client_id']), array());
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['result']['clientcontacts_id']), array());
        $data['employee'] = $this->gm->get_selected_record('employee', '*', $where = array('id' => $data['result']['handledby']), array());


        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $data['jobreglang'] = $this->gm->get_data_list('jobreglang', $where = array('jobregister_id' => $_GET['id']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
//        echo '<pre>';
//        print_r($data);
//        exit;
        if (isset($_GET['send']) && $_GET['send']) {
            $this->load->helper('email');
            $body = $this->load->view('job_register_reply_letter', $data, TRUE);
            $response = send_email($data['clientcontacts']['email'], 'Email Reply Letter' . ' : ' . $data['client']['name'], $body, 'kesen@kesen.in', $reply_email = "", $cc_email = "", $attachment = array());
            if ($response) {
                $this->gm->update('jobregister', array('reference_email_status' => 2), 0, array('id' => $data['result']['id']));
            }
            $message = 'Email send successfully on email id : ' . $data['clientcontacts']['email'];
            $this->session->set_flashdata('success', $message);
            redirect('odv/odv_list');
        } else {
            echo $this->load->view('job_register_reply_letter', $data, TRUE);
            if ($data['result']['reference_email_status'] == 1) {
                echo '<a style="
    text-align: center;
    font-size: 24px;
    background: aliceblue;
    display: block;
    text-decoration: none;
    border: 1px solid;
" href="' . base_url('odv/emailreplyletter?id=' . $_GET['id'] . '&send=1') . '">Send Email</a>';
            }
        }
    }

    function sms() {
        $this->load->model('Global_model', 'gm');
        $this->load->helper('sms', 'url');
        $id = send_sms($_GET['contactno'], urldecode($_GET['content']));

        $data['result'] = $this->gm->get_selected_record('sms_log', '*', $where = array('id' => $id), array());
        //echo '<pre>';
        //print_r($data);
        //exit;
        //echo '<pre>';
        //print_r($id);
        //exit;
        //exit;
        if ($data['result']['status'] == 'failure') {

            $temp = urldecode($_GET['content']);
            $message = 'SMS " ' . $temp . ' " send fail on contact no : ' . $_GET['contactno'];
            $this->session->set_flashdata('danger', $message);
        } else {
            $temp = urldecode($_GET['content']);
            $message = 'SMS " ' . $temp . ' " send successfully on contact no : ' . $_GET['contactno'];
            $this->session->set_flashdata('success', $message);
        }

        redirect('odv/odv_list');
    }

}
