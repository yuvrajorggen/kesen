<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Employee</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                        <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="employee">Employee Name<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type = "text" class = "form-control" tabindex="1" value="<?php echo isset($employee['name']) ? $employee['name'] : ''; ?>" autofocus="on" id="employee" name="employee[name]">
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="role">Role<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select  class="searchselected" tabindex="2" id="role" name="employee[role]">

                                        <?php
                                        $roles = $this->config->item('roles');
                                        foreach ($roles as $key => $value) {
                                            ?><option
                                            <?php
                                            if (isset($employee['role'])) {
                                                if ($key == $employee['role']) {
                                                    echo 'selected';
                                                }
                                            }
                                            ?>
                                                value="<?php echo $key; ?>"><?php echo $value; ?></option> <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="email">E-mail<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="email" class="form-control" id="email" value="<?php echo isset($employee['email']) ? $employee['email'] : ''; ?>" tabindex="3" name="employee[email]">
                                </div>
                                <label class = "control-label col-md-1 col-sm-1" for="contact">Contact No.<span class = "required">*</span></label>
                                <div class = "col-md-2 col-sm-2">
                                    <input type = "number" class = "form-control" tabindex="4" value="<?php echo isset($employee['contactno']) ? $employee['contactno'] : ''; ?>" id="contact" maxlength="10" name="employee[contactno]">
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <?php
                            if (isset($mode) && $mode == 'update') {
                                ?>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-sm-offset-2">Change Password<span class="required">*</span></label>
                                    <div class="col-md-2 col-sm-2">
                                        <input type="checkbox" class="form-control" id="pwdupdate_chkbox" tabindex="" name="employee[check_password]" >
                                    </div>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div id="password_wrapper" class="row" <?php echo (isset($mode) && $mode == 'update') ? 'style="display:none;"' : ''; ?>>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="pwd">Password<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="password" class="form-control" id="pwd" tabindex="5" value="<?php echo (!isset($mode)) ? '' : '123'; ?>" name="employee[password]"  id="password">
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="confirmpwd">Password (again)<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="password" class="form-control" id="confirmpwd" tabindex="6" value="<?php echo (!isset($mode)) ? '' : '123'; ?>" name="employee[samepassword]" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="">Employee Code<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">

                                    <input type="text" class="form-control" id="ecode" tabindex="3" value="<?php echo isset($employee['code']) ? $employee['code'] : ''; ?>" maxlength="5" name="employee[code]" <?php
                                    if (isset($mode) && $mode == 'update') {
                                        echo 'disabled';
                                    }
                                    ?>>
                                </div>
                                <label class = "control-label col-md-1 col-sm-1" for="contact">Language<span class = "required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select multiple class="3col active" tabindex="7"  name="language[]" id="language">

                                        <?php
                                        foreach ($language as $key => $value) {
                                            ?><option 
                                            <?php
                                            if (isset($langemplist)) {
                                                if (array_key_exists($key, $langemplist)) {
                                                    echo 'selected';
                                                }
                                            }
                                            ?>
                                                value="<?php echo $key; ?>"><?php echo ucwords($value); ?></option><?php
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class = "control-label col-md-2 col-sm-2 col-sm-offset-2" for="address">Address<span class = "required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <textarea id="address" class="form-control"  tabindex="5" name="employee[address]"><?php echo isset($employee['address']) ? $employee['address'] : ''; ?></textarea>
                                </div>
                                <label class = "control-label col-md-1 col-sm-1" for="country">Landline Number<span class = "required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input type = "text" class = "form-control" tabindex="4" value="<?php echo isset($employee['landline']) ? $employee['landline'] : ''; ?>" id="contact" maxlength="12" name="employee[landline]">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group hidden">
                                <label class = "control-label col-md-2 col-sm-2 col-sm-offset-2" for="country">State<span class = "required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select class="chosen-select" tabindex="7" id="country">
                                        <option>Select State</option>
                                        <option>Agartala</option>
                                        <option>Agatti</option>
                                        <option>Agra</option>
                                        <option>Ahemdabad</option>
                                        <option>Aizawal</option>
                                        <option>Allahbad</option>
                                        <option>Amritsar</option>
                                        <option>Anand</option>
                                        <option>Aurangabad</option>
                                        <option>Bagdogra</option>
                                        <option>Banglore</option>
                                        <option>Belgaum</option>
                                    </select>
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="city">City<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select class="chosen-select" tabindex="8" id="city">
                                        <option>Select City</option>
                                        <option>Agartala</option>
                                        <option>Agatti</option>
                                        <option>Agra</option>
                                        <option>Ahemdabad</option>
                                        <option>Aizawal</option>
                                        <option>Allahbad</option>
                                        <option>Amritsar</option>
                                        <option>Anand</option>
                                        <option>Aurangabad</option>
                                        <option>Bagdogra</option>
                                        <option>Banglore</option>
                                        <option>Belgaum</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class = "row">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    var input = document.getElementById('ecode');

    input.onkeyup = function () {
        this.value = this.value.toUpperCase();
    }
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#employee_add").validate({
        ignore: [],
        rules: {
            'employee[name]': {
                required: true,
                rangelength: [3, 50],
                lettersonly: true
            },
            'employee[email]': {
                required: true,
                email: true,
            },
            'employee[contactno]': {
                required: true,
                digits: true,
                rangelength: [10, 10],
            },
            'employee[password]': {
                required: true,
                rangelength: [3, 7]
            }, 'employee[samepassword]': {
                equalTo: "#pwd"
            },
            'employee[code]': {
                required: true,
                rangelength: [3, 3],
                lettersdigitonly: true,
            }, 'employee[address]': {
                required: true,
                rangelength: [3, 500],
            }, 'language[]': {
                required: true,
            }
        },
        messages: {
            'employee[name]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        },
        submitHandler: function (form, event) {

            $.ajax({
                url: '<?php echo base_url() . 'employee/get_code'; ?>',
                type: 'POST',
                data: $(form).serialize(),
                success: function (response) {
                    form.submit();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    event.preventDefault();
                    $.dialog({
                        title: 'Error Message',
                        content: 'Code already assign to employee. Kindly select new code',
                        closeIcon: true,
                        buttons: {
                            cancelAction: function () {

                            }
                        }
                    });
                }
            });
        }
    });
    $("#pwdupdate_chkbox").change(function () {
        if (this.checked) {
            $("#password_wrapper").show();
            $("#pwd").val('');
            $("#confirmpwd").val('');
        } else {
            $("#password_wrapper").hide();
            $("#pwd").val('123');
            $("#confirmpwd").val('123');
        }
    });

</script>