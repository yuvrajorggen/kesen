<div class="container">
    <div class="article">
        <h2><span>Managing Editor Board</span></h2>
        <div class="col-sm-4 left-grid">
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?php echo IMAGE_PATH_FRONTEND;?>userpic.gif" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8 user-name">
                    <p class="name">Rajesh R (Education), MA education, MBA, LLB, BSc (physics),Dip. Mass. Comm</p>
                    <p>Editor in Chief   </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="personal-info">Email <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="personal-info">editor@iorj.org, editor.iorj@gmail.com</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 left-grid">
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?php echo IMAGE_PATH_FRONTEND;?>userpic.gif" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8 user-name">
                    <p class="name">RAJESH R Director,  THIRTYONE VENTURES OPC PVT. LTD.</p>
                    <p>PUBLISHER</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <p class="personal-info">Email <span>:</span></p>
                </div>
                <div class="col-sm-8">
                    <p class="personal-info">thirtyoneventures@gmail.com<br> rajesh@thirtyoneventures.com</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4 left-grid">
            <div class="row">
                <div class="col-sm-4">
                    <img src="<?php echo IMAGE_PATH_FRONTEND;?>userpic.gif" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8 user-name">
                    <p class="name">Dr. RAMA KRISHNA CHITTAJALLU</p>
                    <p>M.Com.,NET,SET.,M.Phil.,M.Sc.,M.Ed.,M.C.S.,M.B.A.,NET.,Ph.D.,</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="personal-info">Affiliation <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="personal-info">G.D.College, MANDAPETA, EG Dist. - 533308 (AP) INDIA</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="personal-info">Position <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="personal-info">Asst.Professor in Commerce,</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="personal-info">Experience <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="personal-info">   4 Years.</p>
                </div>
            </div>
            <div class="row journal">
                <div class="col-sm-12">
                    <p class="journal-info">Paper Published</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="journal-info">Journal <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="journal-info">   07</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="journal-info">National Journal <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="journal-info">   07</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <p class="journal-info">Awards <span>:</span></p>
                </div>
                <div class="col-sm-7">
                    <p class="journal-info">     N/A </p>
                </div>
            </div>
        </div>
    </div>
</div>