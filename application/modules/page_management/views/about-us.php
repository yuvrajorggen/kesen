<div class="container">
    <div class="row ">
        <div class="col-sm-4 col-md-4 left-banner">
            <div class="search-wrapper banner_all">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#menu1">Articles</a></li>
                    <li><a data-toggle="tab" href="#menu2">Journals</a></li>
                </ul>
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                        <p>80,251 articles from 417 journals</p>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Enter search terms">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <select class="dropdown">
                                        <option value="volvo">All fields</option>
                                        <option value="saab">Title</option>
                                        <option value="opel">Author</option>
                                        <option value="audi">Abstract</option>
                                        <option value="opel">Keywords</option>
                                        <option value="opel">Author Address</option>
                                        <option value="opel">Journal Title</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn button btn-go" value="Go" type="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <p>Search terms can be either <br>1) any word from the journal title or <br>2) the journal acronym</p>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Enter search terms">
                                </div>
                                <div class=" col-sm-2">
                                    <input class="btn button btn-go" value="Go" type="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-8 right-banner">
            <img src="<?php echo IMAGE_PATH_FRONTEND;?>dictionary-1619740_1280.jpg" class="img-responsive banner" >
        </div>
    </div>
</div>