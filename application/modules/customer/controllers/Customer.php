<?php

class Customer extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $data['language'] = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        $this->_display($view, $data);
    }

    function insert() {

        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        $this->load->model('Global_model', 'gm');
        $data = array();

        $data = $this->input->post('writer');
        $data['code'] = strtoupper($data['code']);
        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');

        $wid = $this->gm->insert('writer', $data);

        $writer_language_map = $this->input->post('writer_language_map');
        $language_id = $this->input->post('language_id');
        $perunitcharges = $this->input->post('perunitcharges');
        $checkingcharges = $this->input->post('checkingcharges');
        $btcharges = $this->input->post('btcharges');
        $btcheckingcharges = $this->input->post('btcheckingcharges');
        $advertisingcharges = $this->input->post('advertisingcharges');


        foreach ($language_id as $key => $value) {
            $temp = array(
              'writer_id' => $wid,
              'language_id' => $language_id[$key],
              'perunitcharges' => $perunitcharges[$key],
              'checkingcharges' => $checkingcharges[$key],
              'btcharges' => $btcharges[$key],
              'btcheckingcharges' => $btcheckingcharges[$key],
              'advertisingcharges' => $advertisingcharges[$key]
            );

            $this->gm->insert('writer_language_map', $temp);
        }

        redirect(site_url('customer/entry_list'));
    }

    function entry_list() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();

        //pagination (start)

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }

        $language = $this->input->get('language');
        if (isset($language) && is_array($language) && count($language) > 0) {
            $sql = "SELECT * FROM `writer_language_map` where language_id  IN (" . implode(',', $language) . " ) ";

            $query_exec = $this->db->query($sql);
            $result = $query_exec->result_array();

            if (isset($result) && is_array($result) && count($result) > 0) {
                foreach ($result as $key => $value) {
                    $writer_ids[] = $value['writer_id'];
                }
            } else {
                $where = ' AND id = 0';
            }
        }



        $sql = "SELECT * FROM `writer`  where status = 1 ";
        if (isset($result) && is_array($result) && count($result) > 0) {
            $where = " AND id IN (" . implode(',', $writer_ids) . " ) ";
        }
        /*
         * name
         */
        $name = $this->input->get('name');
        if ($name != '') {
            $where .= " AND `name` LIKE '%" . $name . "%'  ";
        }

        $order = " ORDER BY id desc  LIMIT " . PER_PAGE . " OFFSET " . $offset;

        $finalquery = $sql . $where . $order;
        $query_exec = $this->db->query($finalquery);
        $result = $query_exec->result_array();

        $cquery = "SELECT count(id) as id FROM `writer` WHERE status='1' " . $where;
        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->row_array();

        $data['list'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('customer/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';

        //pagination end

        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $result = $this->gm->get_data_list('writer_language_map', array(), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer_language_map'][$value['writer_id']][$value['language_id']] = $value;
        }

        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');


        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');
        $id = $this->input->post('id');
        $data = $this->input->post('writer');

        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');
        /*
         * uppercase applied on data
         */
        $this->gm->update('writer', $data, 0, array('id' => $id));
        $wid = $id;


        $sql = " DELETE FROM `writer_language_map` WHERE `writer_language_map`.`writer_id`  =" . $wid;

        $this->db->query($sql);





        $writer_language_map = $this->input->post('writer_language_map');
        $language_id = $this->input->post('language_id');
        $perunitcharges = $this->input->post('perunitcharges');
        $checkingcharges = $this->input->post('checkingcharges');
        $btcharges = $this->input->post('btcharges');
        $btcheckingcharges = $this->input->post('btcheckingcharges');
        $advertisingcharges = $this->input->post('advertisingcharges');


        foreach ($language_id as $key => $value) {
            $temp = array(
              'writer_id' => $wid,
              'language_id' => $language_id[$key],
              'perunitcharges' => $perunitcharges[$key],
              'checkingcharges' => $checkingcharges[$key],
              'btcharges' => $btcharges[$key],
              'btcheckingcharges' => $btcheckingcharges[$key],
              'advertisingcharges' => $advertisingcharges[$key]
            );

            $this->gm->insert('writer_language_map', $temp);
        }


        redirect("customer/entry_list");
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $_GET['id']));
        $data['language'] = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        $result = $this->gm->get_data_list('writer_language_map', array('writer_id' => $data['writer']['id']), array(), array('id' => 'desc'), '*', 3000, array());

        foreach ($result as $key => $value) {
            $data['writer_language_map'][$value['language_id']] = $value;
        }


        $data['mode'] = "update";
        $view = 'add_entry';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function delete() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');


        $set = array(
          'modified_by' => $sessiondata['id'],
          'modified_date' => date('Y-m-d H:i:s'),
          'status' => 2
        );
        $data['customer'] = $this->gm->update('writer', $set, 0, array('id' => $_GET['id']));

        $this->session->set_flashdata('success', 'writer successfully deleted');

        redirect("customer/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function upload_customer() {
        $this->load->helper('url');
        $data = array();
        $this->_display('customer_upload', $data);
    }

    function customer_upload() {
        $this->load->model('Global_model', 'gm');
        $file_name = 'csv_import_' . date('Y-m-d_H-i-s', time());
        $config['upload_path'] = './media/csv/bulk_import/';
        $config['allowed_types'] = 'application/octet-stream|CSV|csv';
        $config['file_name'] = $file_name;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());

            $row = 1;
            if (($handle = fopen($data['upload_data']['full_path'], "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $num = count($data);
                    //echo '<pre>';
                    //print_r($data);
                    unset($insert);
                    if ($row != 0) {
                        $insert = array(
                          "name" => $data[0],
                          "display_name" => $data[1],
                          "code" => $data[2],
                          "address" => $data[3],
                          "phone" => $data[4],
                          "email_account" => $data[5],
                          "pan_number" => $data[6],
                          "customer_status" => $data[7]
                        );

                        $this->gm->insert('customer', $insert);
                    }
                    $row++;
                }
                fclose($handle);
            }
        }

        $this->load->helper('url');
        redirect(site_url('customer/entry_list'));
    }

    function writer_list() {
        $sql = "SELECT * FROM `writer` where status = 1 ";
        $language = $this->input->get('language');

        if (count($language) > 0) {
            $sql = $sql . " AND language IN (" . implode(',', $language) . " )";
        }
        $sql .= " ORDER BY id desc  LIMIT " . PER_PAGE . " ";

        $query_exec = $this->db->query($sql);
        $data['result'] = $query_exec->result_array();

        //$this->load->view('writer_list', $data);

        $this->load->helper('pdf_helper');
        $this->load->helper('url');

        $pdfname = 'media/pdfs/writer-list' . rand(0000, 9999) . '.pdf';
        $path = save_pdf('writer_list', $data, $pdfname);
        redirect(base_url() . $path);
    }

    /*
     * check code while adding writer
     */

    function get_code() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $writer = $this->input->post('writer');

        if (isset($writer['code']) && !empty($writer['code'])) {
            $code = array('code' => strtoupper($writer['code']));
            $result = $this->gm->get_selected_record('writer', '*', $where = $code, array());
            if (isset($result['id'])) {
                http_response_code(401);
            } else {
                http_response_code(200);
            }
        } else {
            http_response_code(401);
        }
    }

    /*
     * get write list when adding job card
     */

    function get_writers() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $result = $this->gm->get_data_list('writer_language_map', array('language_id' => $_GET['lang']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($result as $key => $value) {
            $writerid[] = $value['writer_id'];
        }

        $sql = "SELECT * FROM `writer` where id IN (" . implode(',', $writerid) . ") ";
        $query_exec = $this->db->query($sql);

        $result = $query_exec->result_array();

        $writers = array();
        if (isset($result) && is_array($result) && count($result) > 0) {
            foreach ($result as $key => $value) {
                $writers[] = $value['code'];
            }

            echo json_encode($writers);
        } else {
            http_response_code(501);
        }
    }

    /*function report() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        if ($_GET['date'] != '') {
            $period = $this->input->get('date');
            $datearray = explode('-', $period);

            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            /*
             * check date should 1st of every month
             */
            /*$sd = $sdate->format("Y-m-d");
            $ed = $edate->format("Y-m-d");

            $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
            $last_day_this_month = date('Y-m-t');

            if (($sd == $first_day_this_month) && ($ed == $last_day_this_month)) {
                
            } else {
                $this->session->set_flashdata('danger', 'date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
                redirect(base_url('customer/report'));
            }

            $query = "SELECT * FROM `jobregister` WHERE `date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'";
            $query_exec = $this->db->query($query);
            $jobregister = $query_exec->result_array();

            if (isset($jobregister) && is_array($jobregister) && count($jobregister) > 0) {
                foreach ($jobregister as $key => $value) {
                    $jobids[] = $value['id'];
                }

                $query = "SELECT * FROM `jobcard` WHERE `jobregister_id` IN (" . implode(',', $jobids) . ") ";
                $query_exec = $this->db->query($query);
                $jobcardlist = $query_exec->result_array();

                if (isset($jobcardlist) && is_array($jobcardlist) && count($jobcardlist) > 0) {
                    foreach ($jobcardlist as $key => $value) {
                        if ($value['writer_id'] != '') {
                            $writer_ids[] = "'" . $value['writer_id'] . "'";
                        }
                    }
                    if (isset($writer_ids) && count($writer_ids) > 0) {
                        $query = "SELECT * FROM `writer` WHERE `code` IN (" . implode(',', $writer_ids) . ") ";
                        $query_exec = $this->db->query($query);
                        $writerlist = $query_exec->result_array();
                        $data['sdate'] = $sdate->format("d-m-Y");
                        $data['edate'] = $edate->format("d-m-Y");
                        $data['writerlist'] = $writerlist;

                        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                        $last_day_this_month = date('Y-m-t');

                        $query = "SELECT * FROM `writer_payment_period` WHERE `start_date` = '" . $first_day_this_month . "' AND `end_date` = '" . $last_day_this_month . "'";
                        $query_exec = $this->db->query($query);
                        $payment_period = $query_exec->result_array();

                        if (isset($payment_period) && is_array($payment_period) && count($payment_period)) {
                            foreach ($payment_period as $key => $value) {
                                $data['payment_data'][$value['writer_id']] = $value;
                            }
                        }

                        $this->load->helper('pdf_helper');
                        $this->load->helper('url');

                        $pdfname = 'media/pdfs/workdone' . rand(000, 999) . '.pdf';

                        $path = save_pdf('writerworkdone', $data, $pdfname);
                        redirect(base_url() . $path);
                    } else {
                        redirect('customer/report');
                    }
                }
            }
        } else {
            $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 30, array());
            foreach ($result as $key => $value) {
                $data['writer'][$value['id']] = $value;
            }
        }
        $this->_display('reportview', $data);
    }*/
	function report() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        if ($_GET['date'] != '') {
            $period = $this->input->get('date');

            $datearray = explode('-', $period);

            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            /*
             * check date should 1st of every month
             */
            $sd = $sdate->format("Y-m-d");

            $firstday = $sdate->format("d");
            if ($firstday == 01) {
                $ed = $edate->format("d");
                $lastday = date('t', strtotime($sd));
                if ($ed == $lastday) {

                    /* if (($sd == $first_day_this_month) && ($ed == $last_day_this_month)) {

                      } else {
                      $this->session->set_flashdata('danger', 'date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
                      redirect(base_url('customer/report'));
                      } */

                    $query = "SELECT * FROM `jobregister` WHERE `date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'";
                    $query_exec = $this->db->query($query);
                    $jobregister = $query_exec->result_array();

                    if (isset($jobregister) && is_array($jobregister) && count($jobregister) > 0) {
                        foreach ($jobregister as $key => $value) {
                            $jobids[] = $value['id'];
                        }

                        $query = "SELECT * FROM `jobcard` WHERE `jobregister_id` IN (" . implode(',', $jobids) . ") ";
                        $query_exec = $this->db->query($query);
                        $jobcardlist = $query_exec->result_array();

                        if (isset($jobcardlist) && is_array($jobcardlist) && count($jobcardlist) > 0) {
                            foreach ($jobcardlist as $key => $value) {
                                if ($value['writer_id'] != '') {
                                    $writer_ids[] = "'" . $value['writer_id'] . "'";
                                }
                            }
                            if (isset($writer_ids) && count($writer_ids) > 0) {
                                $query = "SELECT * FROM `writer` WHERE `code` IN (" . implode(',', $writer_ids) . ") ";
                                $query_exec = $this->db->query($query);
                                $writerlist = $query_exec->result_array();
                                $data['sdate'] = $sdate->format("d-m-Y");
                                $data['edate'] = $edate->format("d-m-Y");
                                $data['writerlist'] = $writerlist;

                                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                                $last_day_this_month = date('Y-m-t');

                                $query = "SELECT * FROM `writer_payment_period` WHERE `start_date` = '" . $first_day_this_month . "' AND `end_date` = '" . $last_day_this_month . "'";
                                $query_exec = $this->db->query($query);
                                $payment_period = $query_exec->result_array();

                                if (isset($payment_period) && is_array($payment_period) && count($payment_period)) {
                                    foreach ($payment_period as $key => $value) {
                                        $data['payment_data'][$value['writer_id']] = $value;
                                    }
                                }

                                $this->load->helper('pdf_helper');
                                $this->load->helper('url');

                                $pdfname = 'media/pdfs/workdone' . rand(000, 999) . '.pdf';

                                $path = save_pdf('writerworkdone', $data, $pdfname);
                                redirect(base_url() . $path);
                            } else {
                                redirect('customer/report');
                            }
                        }
                    }
                } else {
                    $this->session->set_flashdata('danger', 'date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
                    redirect(base_url('customer/report'));
                }
            } else {
                $this->session->set_flashdata('danger', 'date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
                redirect(base_url('customer/report'));
            }
        } else {
            $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 30, array());
            foreach ($result as $key => $value) {
                $data['writer'][$value['id']] = $value;
            }
        }
        $this->_display('reportview', $data);
    }

}
