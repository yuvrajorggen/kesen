<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Writer</h2>
                </div>
                <div class="x_content">
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                        </div>
                    </div>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('customer/update') : site_url('customer/insert') ?>"> 
                        <div class="col-md-5 col-sm-5">

                            <input type="hidden" name="id" value="<?php echo isset($writer['id']) ? $writer['id'] : ''; ?>">
                            <div class="row">
                                <div class="text-center">
                                    <h5 class="pull-left">Information</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="name">Writer's Name<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="name" name="writer[name]" value="<?php echo isset($writer['name']) ? $writer['name'] : ''; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="code">Code<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input  <?php
                                        if (isset($mode) && $mode == 'update') {
                                            echo 'disabled';
                                        }
                                        ?> type = "text" class = "form-control" tabindex="2" id="code" name="writer[code]" maxlength="3" value="<?php echo isset($writer['code']) ? $writer['code'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="email">E-mail</label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="email" class="form-control" id="email" tabindex="4" name="writer[email]" value="<?php echo isset($writer['email']) ? $writer['email'] : ''; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="contact">Contact No.</label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="number" class="form-control" id="contact" tabindex="5" name="writer[contactno]" value="<?php echo isset($writer['contactno']) ? $writer['contactno'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="landline">Landline No.</label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "number" class = "form-control" tabindex="6" id="landline" name="writer[landline]" value="<?php echo isset($writer['landline']) ? $writer['landline'] : ''; ?>">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="address">Address<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <textarea id="address" class="form-control" tabindex="5" name="writer[address]"><?php echo isset($writer['address']) ? $writer['address'] : ''; ?></textarea>
                                    </div>
                                    <label class = "control-label col-md-2 col-sm-2 hidden" for="country">Country<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4 hidden">

                                    </div>
                                </div>
                            </div>
                            <div class="row hidden">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="country">State<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">

                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="city">City<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-md-7 col-sm-7">


                            <div class="row">
                                <div class="text-center">
                                    <h5 class="pull-left">Charges</h5>
                                </div>
                            </div>
                            <table class="table table-bordered" id="table-data">
                                <tr>
                                    <th  width="18%">Language</th>
                                    <th>Per Unit Charges<span class="required">*</span></th>
                                    <th>Checking Charges.<span class="required">*</span></th>
                                    <th>BT Charges.</th>
                                    <th>BT Checking Charges</th>
                                    <th>Advertising Charges</th>
                                    <th>Action</th>
                                </tr>
                                <?php
                                if (isset($writer_language_map) && count($writer_language_map) && is_array($writer_language_map)) {
                                    foreach ($writer_language_map as $key => $value) {
                                        ?>
                                        <tr class="tr_clone">
                                            <td><select class=" form-control" tabindex="7" id="language" name="language_id[]">
                                                    <?php
                                                    foreach ($language as $k => $v) {
                                                        ?><option 
                                                        <?php
                                                        if ($v['id'] == $key) {
                                                            echo 'selected';
                                                        }
                                                        ?>
                                                            value="<?php echo $v['id']; ?>"><?php echo ucwords($v['name']); ?></option><?php
                                                        }
                                                        ?>
                                                </select>
                                            </td>
                                            <td><input type = "number" class = "form-control" tabindex="1" autofocus="on" id="name" name="perunitcharges[]"  value="<?php echo isset($value['perunitcharges']) ? $value['perunitcharges'] : ''; ?>"></td>
                                            <td><input type="number" class="form-control" id="email" tabindex="4" name="checkingcharges[]"  value="<?php echo isset($value['checkingcharges']) ? $value['checkingcharges'] : ''; ?>"></td>
                                            <td><input type="number" class="form-control" id="email" tabindex="4" name="btcharges[]"  value="<?php echo isset($value['btcharges']) ? $value['btcharges'] : ''; ?>"></td>
                                            <td><input type="number" class="form-control" id="contact" tabindex="5" name="btcheckingcharges[]"  value="<?php echo isset($value['btcheckingcharges']) ? $value['btcheckingcharges'] : ''; ?>"></td>
                                            <td><input type = "number" class = "form-control" tabindex="6" id="landline" name="advertisingcharges[]"  value="<?php echo isset($value['advertisingcharges']) ? $value['advertisingcharges'] : ''; ?>"></td>
                                            <td>
                                                <a title="Update Job Card" href="javascript:void(0);" id="add" class="tr_clone_add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>&nbsp;Add</a>

                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>

                                    <tr class="tr_clone">
                                        <td><select class=" form-control check" tabindex="7" id="language" name="language_id[]">
                                                <?php
                                                foreach ($language as $key => $value) {
                                                    ?><option 
                                                    <?php
                                                    if (isset($writer['language']) && $value['id'] == $writer['language']) {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                        value="<?php echo $value['id']; ?>"><?php echo ucwords($value['name']); ?></option><?php
                                                    }
                                                    ?>
                                            </select>
                                        </td>
                                        <td><input type = "number" class = "form-control" tabindex="1" autofocus="on" id="name" name="perunitcharges[]"  value="<?php echo isset($writer['perunitcharges']) ? $writer['perunitcharges'] : ''; ?>"></td>
                                        <td><input type="number" class="form-control" id="email" tabindex="4" name="checkingcharges[]"  value="<?php echo isset($writer['checkingcharges']) ? $writer['checkingcharges'] : ''; ?>"></td>
                                        <td><input type="number" class="form-control" id="email" tabindex="4" name="btcharges[]"  value="<?php echo isset($writer['btcharges']) ? $writer['btcharges'] : ''; ?>"></td>
                                        <td><input type="number" class="form-control" id="contact" tabindex="5" name="btcheckingcharges[]"  value="<?php echo isset($writer['btcheckingcharges']) ? $writer['btcheckingcharges'] : ''; ?>"></td>
                                        <td><input type = "number" class = "form-control" tabindex="6" id="landline" name="advertisingcharges[]"  value="<?php echo isset($writer['advertisingcharges']) ? $writer['advertisingcharges'] : ''; ?>"></td>
                                        <td>
                                            <a title="Update Job Card" href="javascript:void(0);" id="add" class="tr_clone_add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>&nbsp;Add</a>
                                            <a title="Delete Job Card" href="javascript:void(0);" onclick="$(this).closest('tr').remove();"><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>



                        </div>
                        <div class = "row">
                            <div class = "col-md-12 pull-right">
                                <div class = "submit">
                                    <button type = "submit" class = "btn btn-default submit-btn pull-right" tabindex="11">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>
<script>
                                            var table = $('#table-data')[0];

                                            $(table).delegate('.tr_clone_add', 'click', function () {
                                                var thisRow = $(this).closest('tr')[0];
                                                $(thisRow).clone().insertAfter(thisRow).find('input').val('');
                                            });

</script>
<script>
    jQuery(document).ready(function () {

        var input = document.getElementById('code');
        input.onkeyup = function () {
            this.value = this.value.toUpperCase();
        }

        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("digitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("codeonly", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        });
        jQuery("#employee_add").validate({
            ignore: [],
            rules: {
                'writer[name]': {
                    required: true,
                    lettersonly: true,
                    rangelength: [3, 50],
                },
                'writer[code]': {
                    required: true,
                    lettersonly: true,
                    codeonly: true,
                    rangelength: [3, 3]
                },
                'writer[email]': {
                    email: true

                },
                'writer[contactno]': {
                    digits: true,
                    rangelength: [10, 15],
                },
                'writer[landline]': {
                    rangelength: [6, 15],
                    digits: true,
                },
                'writer[language]': {
                    required: true,
                },
                'writer[address]': {
                    required: true
                },
                'perunitcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'checkingcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'btcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'btcheckingcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'advertisingcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'lumsumcharges[]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                }
            },
            messages: {
                'writer[name]': {
                    required: "Name Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'writer[code]': {
                    required: "Contact No Required",
                    lettersonly: "Please enter correct Name without space",
                    codeonly: "Space not allowed"
                },
                'writer[email]': {
                    required: "Email Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'contact_no': {
                    required: "Contact No Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'txtmessage': {
                    required: "Message Required",
                    lettersonly: "Please enter correct Name without space"
                },
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            },
            submitHandler: function (form, event) {
                checklang = false;
                var lang = [];
                $('select').each(function () {

                    if ($.inArray($(this).val(), lang)) {
                        lang.push($(this).val());

                    } else {
                        checklang = true;
                        $.dialog({
                            title: 'Error Message',
                            content: 'You have selected same language.',
                            closeIcon: true,
                            buttons: {
                                cancelAction: function () {

                                }
                            }
                        });


                    }

                });
                if (checklang) {
                    return false;
                }

<?php
if (!isset($mode) && $mode != 'update') {
    ?>

                    $.ajax({
                        url: '<?php echo base_url() . 'customer/get_code'; ?>',
                        type: 'POST',
                        data: $(form).serialize(),
                        success: function (response) {
                            form.submit();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            event.preventDefault();
                            $.dialog({
                                title: 'Error Message',
                                content: 'Code already assign to writer. Kindly select new code',
                                closeIcon: true,
                                buttons: {
                                    cancelAction: function () {

                                    }
                                }
                            });
                        }
                    });
<?php } else { ?>
                    form.submit();
<?php } ?>
                /*
                 * 
                 */

            }
        });
    });
</script>

