<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Writer</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('customer/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Language<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <select multiple class=" 3col active" tabindex="2" name="language[]">
                                    <?php
                                    $lang = $this->input->get('language');

                                    foreach ($language as $key => $value) {
                                        ?><option value="<?php echo $key; ?>"
                                        <?php
                                        if (in_array($key, $lang)) {
                                            echo 'selected';
                                        }
                                        ?>
                                                ><?php echo $value; ?></option><?php
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group label_fields">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Name</label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" placeholder="Name" name="name" value="<?php echo isset($_GET['name']) ? $_GET['name'] : ''; ?>">
                            </div>

                            <input type="submit" value="Search" class="btn btn-default submit-btn">
                            <a tabindex="6" class="btn btn-default view-btn" title="View All Writers" href="<?php echo site_url('customer/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default add-btn"  title="Add Writer" href="<?php echo site_url('customer/entry'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Writer</a>
                            <!--<a tabindex="8" class="btn btn-default btn-info"  title="Print" target="_blank" href="<?php echo base_url() . 'customer/writer_list?' . $_SERVER['QUERY_STRING']; ?>"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>-->
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Writer List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Writers : <?php echo $total; ?></span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Name</th>
                            <th>Code</th>
                            <th>email</th>
                            <th>contact no</th>
                            <th>address</th>
                            <th>language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($list as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['code']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['contactno']; ?></td>
                                <td><?php echo $value['address']; ?></td>
                                <td>
                                    <table class="table table-bordered">
                                        <tr  class="table_heading">
                                            <td>language_id</td>
                                            <td>perunitcharges</td>
                                            <td>checkingcharges</td>
                                            <td>btcharges</td>
                                            <td>btcheckingcharges</td>
                                            <td>advertisingcharges</td>
                                        </tr>
                                        <?php
                                        if (isset($writer_language_map[$value['id']])) {
                                            foreach ($writer_language_map[$value['id']] as $k => $v) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $language[$v['language_id']]; ?></td>
                                                    <td><?php echo $v['perunitcharges']; ?></td>
                                                    <td><?php echo $v['checkingcharges']; ?></td>
                                                    <td><?php echo $v['btcharges']; ?></td>
                                                    <td><?php echo $v['btcheckingcharges']; ?></td>
                                                    <td><?php echo $v['advertisingcharges']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td>
                                    <a title="Update Writer" href="<?php echo base_url('customer/edit?id=') . $value['id']; ?>"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                    <a title="Delete Writer" href="javascript:void(0)"  onclick="del('Delete', 'btn-red', 'Kindly Confirm', 'Are you sure you want to delete <?php echo ucwords($value['name']); ?>?', '<?php echo base_url('customer/delete?id=') . $value['id']; ?>');"><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a> 
                                </td>
                            </tr> 
                            <?php
                        }
                        ?>


                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

