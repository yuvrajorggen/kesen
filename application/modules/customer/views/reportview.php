<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Writer</h3>
                </div>
                <div class="x_content search_content">
                    <?php /* <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('customer/report'); ?>" method="GET"> */ ?>
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignee/get_writer_amount'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Period<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <input autocomplete="off" id="date" class="form-control"  type="text" tabindex="2" name="date" 
                                       value="<?php echo isset($_GET['date']); ?>"   
                                       <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> >    
                            </div>
                            <h3 style="color:red">This Amount column in this report is not valid if you generate report dated before 31/12/2018 </h3>
                            <input type="submit" value="Print" class="btn btn-default submit-btn">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#date').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
<?php if (!isset($_GET['date'])) { ?>
            $('#date').val('');
<?php } ?>

    });
</script>