<?php

class Consignee extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $this->load->library('user_agent');
        $data = array();
        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 0, array());

        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }
        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function delete() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');

        $query = "DELETE FROM `writer_payment_period` WHERE `writer_payment_period`.`id` = " . $_GET['id'];
        $this->db->query($query);

        $query = "DELETE FROM `writer_payment_period_detail` WHERE `writer_payment_period_id` = " . $_GET['id'];
        $this->db->query($query);

        $message = '<b>' . 'Payment advice deleted successfully' . '</b>';
        $this->session->set_flashdata('danger', $danger);

        redirect("consignee/entry_list");
    }

    function get_writer_amount() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $get = $this->input->get();
        $period = isset($get['date']) ? $get['date'] : '';
        $datearray = explode('-', $period);
        $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
        $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
        $firstday = $sdate->format("d");
        $sd = $sdate->format("Y-m-d");
        if ($firstday == 01) {
            $ed = $edate->format("d");
            $lastday = date('t', strtotime($sd));
            if ($ed == $lastday) {
                $result = $this->gm->get_data_list('writer', array('status' => 1), array(), array('id' => 'desc'), 'id,name,code');
                if (isset($result) && is_array($result) && count($result) > 0) {
                    foreach ($result as $rkey => $rvalue) {
                        $writer_data['writer'] = $rvalue['id'];
                        $writer_data['period'] = $period;
                        $amount = $this->calculate_payment($writer_data);
                        $data['payment_data'][$rvalue['id']]['grandtotal'] = $amount;
                    }
                }

                $this->load->helper('pdf_helper');
                $pdfname = 'media/pdfs/writer_workdone' . rand(000, 999) . '.pdf';
                $data['writerlist'] = $result;

                $data['sdate'] = $sdate->format("d M, Y");
                $data['edate'] = $edate->format("d M, Y");
                $path = save_pdf('writer_payment_info', $data, $pdfname);
                redirect(base_url() . $path);
            } else {
                $this->session->set_flashdata('danger', 'Date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
                redirect(base_url('customer/report'));
            }
        } else {
            $this->session->set_flashdata('danger', 'Date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018');
            redirect(base_url('customer/report'));
        }
    }

    function calculate_payment($writer_data = array()) {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $grandtotal = 0;

        $writerid = $writer_data['writer'];
        $period = $writer_data['period'];
        $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $writerid));

        $result = $this->gm->get_data_list('writer_language_map', array('writer_id' => $writerid), array(), array('id' => 'desc'), '*', 3000, array());
        foreach ($result as $key => $value) {
            $data['writer_language_map'][$value['language_id']] = $value;
        }
        $writer_language_map = $data['writer_language_map'];
        $query = "SELECT * FROM `jobcard` WHERE `writer_id` = '" . $data['writer']['code'] . "'";
        $query_exec = $this->db->query($query);
        $jobcardlist = $query_exec->result_array();
        if (isset($jobcardlist) && is_array($jobcardlist) && count($jobcardlist) > 0) {

            $jobcardids = array();
            foreach ($jobcardlist as $key => $value) {
                $jobcardids[] = $value['jobregister_id'];
            }

            $datearray = explode('-', $period);

            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            if (isset($jobcardids) && is_array($jobcardids) && count($jobcardids) > 0) {
                $query = "SELECT * FROM `jobregister` WHERE `id` IN (" . implode(',', $jobcardids) . ") AND `date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'";
                $query_exec = $this->db->query($query);
                $jobregister = $query_exec->result_array();
            }
            if (isset($jobregister) && is_array($jobregister) && count($jobregister) > 0) {
                $typearray = array(
                    1 => 'perunitcharges',
                    2 => 'checkingcharges',
                    3 => 'btcharges',
                    4 => 'btcheckingcharges',
                    5 => 'advertisingcharges',
                    6 => 'lumsumcharges'
                );


                $finaljobs = array();
                foreach ($jobregister as $key => $value) {
                    $finaljobs[] = $value['id'];
                }

                $result = array_flip($finaljobs);
                $finalamount = 0;
                foreach ($jobcardlist as $key => $value) {
                    if (array_key_exists($value['jobregister_id'], $result)) {
                        $temp = array(
                            'amount' => $value['unit'] * $writer_language_map[$value['language_id']][$typearray[$value['type']]]
                        );
                        $finalamount = $finalamount + $temp['amount'];
                    }
                }
                $grandtotal = round($finalamount);
            }
        }
        return $grandtotal;
    }

    function update_writer_payment($id = '') {
        $this->load->model('Global_model', 'gm');
        if (is_numeric($id)) {
            $writer_payment_id = $id;
            $writer_payment_data = $this->gm->get_selected_record('writer_payment_period', '*', $where = array('id' => $writer_payment_id));

            if (isset($writer_payment_data) && is_array($writer_payment_data) && count($writer_payment_data) > 0) {
                $writerid = $writer_payment_data['writer_id'];
                $start_date = isset($writer_payment_data['start_date']) ? date('m/d/Y', strtotime($writer_payment_data['start_date'])) : '';
                $end_date = isset($writer_payment_data['end_date']) ? date('m/d/Y', strtotime($writer_payment_data['end_date'])) : '';

                $period = $start_date . '-' . $end_date;
                $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $writerid));

                $result = $this->gm->get_data_list('writer_language_map', array('writer_id' => $writerid), array(), array('id' => 'desc'), '*', 3000, array());

                foreach ($result as $key => $value) {
                    $data['writer_language_map'][$value['language_id']] = $value;
                }
                $writer_language_map = $data['writer_language_map'];
                $query = "SELECT * FROM `jobcard` WHERE `writer_id` = '" . $data['writer']['code'] . "'";
                $query_exec = $this->db->query($query);
                $jobcardlist = $query_exec->result_array();

                if (isset($jobcardlist) && is_array($jobcardlist) && count($jobcardlist) > 0) {

                    $jobcardids = array();
                    foreach ($jobcardlist as $key => $value) {
                        $jobcardids[] = $value['jobregister_id'];
                    }

                    $datearray = explode('-', $period);

                    $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
                    $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

                    if (isset($jobcardids) && is_array($jobcardids) && count($jobcardids) > 0) {
                        $query = "SELECT * FROM `jobregister` WHERE `id` IN (" . implode(',', $jobcardids) . ") AND `date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'";
                        $query_exec = $this->db->query($query);
                        $jobregister = $query_exec->result_array();
                    }

                    if (isset($jobregister) && is_array($jobregister) && count($jobregister) > 0) {

                        $typearray = array(
                            1 => 'perunitcharges',
                            2 => 'checkingcharges',
                            3 => 'btcharges',
                            4 => 'btcheckingcharges',
                            5 => 'advertisingcharges',
                            6 => 'lumsumcharges'
                        );

                        $finaljobs = array();
                        foreach ($jobregister as $key => $value) {
                            $finaljobs[] = $value['id'];
                        }
                        $result = array_flip($finaljobs);

                        $finalamount = 0;
                        foreach ($jobcardlist as $key => $value) {
                            if (array_key_exists($value['jobregister_id'], $result)) {
                                /*
                                 * Check record exist
                                 */
                                $temp = array(
                                    'writer_payment_period_id' => $id,
                                    'jobregister_id' => $value['jobregister_id'],
                                    'language_id' => $value['language_id'],
                                    'jobcard_id' => $value['id'],
                                    'amount' => $value['unit'] * $writer_language_map[$value['language_id']][$typearray[$value['type']]]
                                );
                                $detail_exist = $this->gm->get_selected_record('writer_payment_period_detail', 'id', $where = array('writer_payment_period_id' => $id, 'jobregister_id' => $value['jobregister_id'], 'jobcard_id' => $value['id']));
                                if (isset($detail_exist) && is_array($detail_exist) && count($detail_exist) > 0) {
                                    $this->gm->update('writer_payment_period_detail', $temp, '', array('id' => $detail_exist['id']));
                                } else {
                                    $this->gm->insert('writer_payment_period_detail', $temp);
                                }

                                $finalamount = $finalamount + $temp['amount'];
                            }
                        }

                        $finalamount = round($finalamount);
                        $this->gm->update('writer_payment_period', array('final_amount' => $finalamount), '', array('id' => $id));
                        $gst_amount = 0;
                        $tds_amount = 0;
                        $tdscal = $finalamount + $writer_payment_data['performance_charges'];
                        $finalamount = $finalamount + $writer_payment_data['performance_charges'];
                        $finalamount = round($finalamount);
                        $grandtotal = $finalamount;
                        $after_gst_amount = 0;
                        $after_tds_amount = 0;

                        if ($finalamount > 0) {
                            if ($writer_payment_data['gst'] == 'on') {
                                if ($writer_payment_data['gstpercentage'] > 0) {
                                    $gst_amount = ($finalamount * $writer_payment_data['gstpercentage']) / 100;
                                    $gst_amount = round($gst_amount);
                                    $finalamount = $finalamount + $gst_amount;
                                    $after_gst_amount = $finalamount;
                                    $grandtotal = $finalamount;
                                }
                            }
                            if ($writer_payment_data['tds'] == 'on') {
                                if ($writer_payment_data['tdspercentage'] > 0) {
                                    $tds_amount = (($tdscal * $writer_payment_data['tdspercentage']) / 100);
                                    $tds_amount = round($tds_amount);
                                    $grandtotal = $finalamount - $tds_amount;
                                    $after_tds_amount = $finalamount - $tds_amount;
                                }
                            }
                        }
                        $grandtotal = ($grandtotal - $writer_payment_data['deductable']);
                        $grandtotal = round($grandtotal);
                        $tdata = array(
                            'after_gst_amount' => (int) $after_gst_amount,
                            'after_tds_amount' => (int) $after_tds_amount,
                            'gst_amount' => (int) $gst_amount,
                            'tds_amount' => (int) $tds_amount,
                            'grandtotal' => (int) $grandtotal
                        );

                        $this->gm->update('writer_payment_period', $tdata, '', array('id' => $id));
                    }
                }
            }
        }
    }

    function insert() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');
        $writerid = $this->input->post('writer');
        $period = $this->input->post('period');
        $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $writerid));

        $result = $this->gm->get_data_list('writer_language_map', array('writer_id' => $writerid), array(), array('id' => 'desc'), '*', 3000, array());

        foreach ($result as $key => $value) {
            $data['writer_language_map'][$value['language_id']] = $value;
        }
        $writer_language_map = $data['writer_language_map'];
        $query = "SELECT * FROM `jobcard` WHERE `writer_id` = '" . $data['writer']['code'] . "'";
        $query_exec = $this->db->query($query);
        $jobcardlist = $query_exec->result_array();

        if (isset($jobcardlist) && is_array($jobcardlist) && count($jobcardlist) > 0) {

            $jobcardids = array();
            foreach ($jobcardlist as $key => $value) {
                $jobcardids[] = $value['jobregister_id'];
            }

            $datearray = explode('-', $period);

            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

            if (isset($jobcardids) && is_array($jobcardids) && count($jobcardids) > 0) {
                $query = "SELECT * FROM `jobregister` WHERE `id` IN (" . implode(',', $jobcardids) . ") AND `date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'";

                $query_exec = $this->db->query($query);
                $jobregister = $query_exec->result_array();
            }
            if (isset($jobregister) && is_array($jobregister) && count($jobregister) > 0) {

                $temp = array(
                    'writer_id' => $writerid,
                    'start_date' => $sdate->format("Y-m-d"),
                    'end_date' => $edate->format("Y-m-d"),
                    'payment_method' => $_POST['payment_method'],
                    'payment_meta1' => $_POST['payment_meta1'],
                    'gst' => $_POST['gst'],
                    'gstpercentage' => $_POST['gstpercentage'],
                    'tds' => $_POST['tds'],
                    'tdspercentage' => $_POST['tdspercentage'],
                    'performance_charges' => $_POST['performance_charges'],
                    'estimatecompany' => $_POST['estimatecompany'],
                    'deductable' => $_POST['deductable']
                );
                $temp['created_date'] = date('Y-m-d H:i:s');
                $temp['created_by'] = $sessiondata['id'];

                $writer_payment_period_id = $this->gm->insert('writer_payment_period', $temp);

                $typearray = array(
                    1 => 'perunitcharges',
                    2 => 'checkingcharges',
                    3 => 'btcharges',
                    4 => 'btcheckingcharges',
                    5 => 'advertisingcharges',
                    6 => 'lumsumcharges'
                );
                /*
                 * calculation part
                 */
//                echo '<pre>';
//                print_r($data);
//                echo '**************************************' . '<br>';
//                print_r($jobcardlist);
//                echo '**************************************' . '<br>';
//                print_r($_POST);
//                exit;
//                echo '<pre>';
//                print_r($jobcardlist);
//                print_r($jobregister);

                $finaljobs = array();
                foreach ($jobregister as $key => $value) {
                    $finaljobs[] = $value['id'];
                }
//               
                $result = array_flip($finaljobs);
//                echo '<pre>';
//                print_r($result);
//                echo '====================';
//                print_r($jobcardlist);
//                exit;
                $finalamount = 0;
                foreach ($jobcardlist as $key => $value) {
                    if (array_key_exists($value['jobregister_id'], $result)) {
                        $temp = array(
                            'writer_payment_period_id' => $writer_payment_period_id,
                            'jobregister_id' => $value['jobregister_id'],
                            'language_id' => $value['language_id'],
                            'jobcard_id' => $value['id'],
                            'amount' => $value['unit'] * $writer_language_map[$value['language_id']][$typearray[$value['type']]]
                        );
                        $finalamount = $finalamount + $temp['amount'];
                        $this->gm->insert('writer_payment_period_detail', $temp);
                    }
                }

                $finalamount = round($finalamount);
                $this->gm->update('writer_payment_period', array('final_amount' => $finalamount), $id, array('id' => $writer_payment_period_id));
                $gst_amount = 0;
                $tds_amount = 0;
                $tdscal = $finalamount + $_POST['performance_charges'];
                $finalamount = $finalamount + $_POST['performance_charges'];
                $finalamount = round($finalamount);
                $grandtotal = $finalamount;
                $after_gst_amount = 0;
                $after_tds_amount = 0;

                if ($finalamount > 0) {
                    if ($_POST['gst'] == 'on') {
                        if ($_POST['gstpercentage'] > 0) {
                            $gst_amount = ($finalamount * $_POST['gstpercentage']) / 100;
                            $gst_amount = round($gst_amount);
                            $finalamount = $finalamount + $gst_amount;
                            $after_gst_amount = $finalamount;
                            $grandtotal = $finalamount;
                        }
                    }
                    if ($_POST['tds'] == 'on') {
                        if ($_POST['tdspercentage'] > 0) {
                            $tds_amount = (($tdscal * $_POST['tdspercentage']) / 100);
                            $tds_amount = round($tds_amount);
                            $grandtotal = $finalamount - $tds_amount;
                            $after_tds_amount = $finalamount - $tds_amount;
                        }
                    }
                }
                $grandtotal = ($grandtotal - $_POST['deductable']);
                $grandtotal = round($grandtotal);
                $tdata = array(
                    'after_gst_amount' => (int) $after_gst_amount,
                    'after_tds_amount' => (int) $after_tds_amount,
                    'gst_amount' => (int) $gst_amount,
                    'tds_amount' => (int) $tds_amount,
                    'grandtotal' => (int) $grandtotal
                );

                $this->gm->update('writer_payment_period', $tdata, '', array('id' => $writer_payment_period_id));
                //print_r($this->db->last_query());exit;
                redirect('consignee/entry_list');
            } else {
                $this->session->set_flashdata('error', $data['writer']['name'] . ' not completed any jobs in period ' . $period);
                redirect('consignee/entry');
            }
        } else {
            $this->session->set_flashdata('error', $data['writer']['name'] . ' not completed any jobs in period ' . $period);
            redirect('consignee/entry');
        }
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $this->load->library('pagination');
        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $where = "";
        $count = array();
        $sql = "SELECT * FROM `writer_payment_period`";
        $writer = $this->input->get('writer');
        if ($writer != '') {
            $where .= " WHERE writer_id = " . $writer;
        }

        $order = " ORDER BY ID DESC";
        $finalsql = $sql . $where . $order;

        $query_exec = $this->db->query($finalsql);
        $result = $query_exec->result_array();
        $data['list'] = $result;

        $result = $this->gm->get_data_list('writer', array(), array(), array('id' => 'desc'), '*', 3000, array());

        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }
        $this->load->library('pagination');

        $config['base_url'] = site_url('odv/odv_list');
        $config['total_rows'] = isset($count['id']) ? $count['id'] : 0;

        $config['per_page'] = 20;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';
//        echo '<pre>';
//        print_r($data);
//        exit;

        $this->_display($view, $data);
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function payment_card() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');

        /*
         * Get Payment Data of latest updated job card entry
         */
        $writer_payment_period_id = $_GET['id'];
        $this->update_writer_payment($writer_payment_period_id);

        $data['writer_payment_period'] = $this->gm->get_selected_record('writer_payment_period', '*', $where = array('id' => $_GET['id']));
        $data['writer_payment_period_detail'] = $this->gm->get_data_list('writer_payment_period_detail', $where = array('writer_payment_period_id' => $_GET['id']), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $data['writer_payment_period']['writer_id']));

        foreach ($data['writer_payment_period_detail'] as $key => $value) {
            $jobcard_id[] = $value['jobcard_id'];
            $jobregister_id[] = $value['jobregister_id'];
        }
        $sql = "SELECT * FROM `jobcard` WHERE id IN(" . implode(',', $jobcard_id) . ")";
        $query_exec = $this->db->query($sql);
        $temp = $query_exec->result_array();
        foreach ($temp as $key => $value) {
            $data['jobcard'][$value['id']] = $value;
        }

        $sql = "SELECT * FROM `jobregister` WHERE id IN(" . implode(',', $jobregister_id) . ")";
        $query_exec = $this->db->query($sql);
        $temp = $query_exec->result_array();
        foreach ($temp as $key => $value) {
            $data['jobregister'][$value['id']] = $value;
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $result = $this->gm->get_data_list('writer_language_map', array('writer_id' => $data['writer_payment_period']['writer_id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer_language_map'][$value['language_id']] = $value;
        }

        $data['writer'] = $this->gm->get_selected_record('writer', '*', $where = array('id' => $data['writer_payment_period']['writer_id']));

//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->load->helper('notoword_helper');
        if (isset($_GET['email'])) {
            echo $this->load->view('payment', $data);
            echo '<a style="
    text-align: center;
    font-size: 24px;
    background: aliceblue;
    display: block;
    text-decoration: none;
    border: 1px solid;
" href="' . base_url('consignee/payment_card?id=' . $_GET['id'] . '&send=1') . '">Send Email</a>';
        } elseif (isset($_GET['send'])) {
            $this->load->helper('email');
            $body = $this->load->view('payment', $data, TRUE);
            $response = send_email($data['writer']['email'], 'Payment Card' . ' : ' . $data['writer']['name'], $body, 'kesen@kesen.in', $reply_email = "", $cc_email = "", $attachment = array());
            redirect(base_url('consignee/entry_list'));
        } else {

            $this->load->helper('pdf_helper');
            $this->load->helper('url');

            $pdfname = 'media/pdfs/write-payment-card-' . $data['writer']['id'] . rand(000, 999) . '.pdf';
            $path = save_pdf('payment', $data, $pdfname);
            redirect(base_url() . $path);
        }
    }

    function error() {
        $this->load->helper('url');
        $data = array();
        $this->_display('error', $data);
    }

    function test() {
        $string = 'email id is kanchan @o rggen.com';
        $pattern = "/[^@s]*@[^@s]*.[^@s]*/";
        $replacement = "";
        $string = preg_replace($pattern, $replacement, $string);
        echo $string;
    }

    function get_staus() {
        $this->load->model('Global_model', 'gm');


        $datearray = explode('-', $_POST['period']);

        $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
        $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

        $sql = "SELECT * FROM `writer_payment_period` WHERE `writer_id` = " . $_POST['writer'] . " and ((`start_date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "') OR (`end_date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "') OR ('" . $sdate->format("Y-m-d") . "' BETWEEN `start_date` AND `end_date`) OR ('" . $edate->format("Y-m-d") . "' BETWEEN `start_date` AND `end_date`))";
        //$sql = "SELECT * FROM `writer_payment_period` WHERE `writer_id` = '" . $_POST['writer'] . "' and ((`start_date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") .  "') OR (`end_date` BETWEEN '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "'))";

        $query_exec = $this->db->query($sql);
        $result = $query_exec->result_array();


        if (isset($result[0]['id']) && $result[0]['id'] > 0) {
            http_response_code(401);
        } else {
            http_response_code(200);
        }
    }

    function check_date_period() {
        $datearray = explode('-', $_POST['period']);

        $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
        $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));

        $sd = $sdate->format("Y-m-d");


        $firstday = $sdate->format("d");
        if ($firstday == 01) {
            $ed = $edate->format("d");
            $lastday = date('t', strtotime($sd));
            if ($ed == $lastday) {
                http_response_code(200);
            } else {
                http_response_code(401);
            }
        } else {
            http_response_code(401);
        }
    }

}
