<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Payment</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('consignee/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">


                            <label class="control-label col-md-2 col-sm-2" for="writer-name">Writer Name<span class="required">*</span></label>

                        </div>
                        <div class="form-group select_fields">


                            <div class="col-md-2 col-sm-2">
                                <select name="writer" class="searchselected " tabindex="3" id="writer-name">
                                    <?php
                                    foreach ($writer as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key; ?>" ><?php echo $value['name']; ?></option>>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <input type="submit" value="Search">
                            <a tabindex="7" class="btn btn-default view-btn" title="View All Clients" href="<?php echo base_url('consignee/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="8" class="btn btn-default add-btn" href="<?php echo site_url('consignee'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Payment</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Payment List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Records : <?php //echo $total;        ?></span>
                </div>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">                                         
                    <thead>
                        <tr class="table_heading">
                            <th>id</th>
                            <th>Writer Name</th>
                            <th>Period</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $key => $value) {
                            ?> 
                            <tr>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $writer[$value['writer_id']]['name']; ?></td>
                                <td><?php
                                    echo date('d M, Y', strtotime($value['start_date'])) . ' - ';
                                    echo date('d M, Y', strtotime($value['end_date']));
                                    ?>
                                </td>
                                <td>
                                    <a target="_blank" href="<?php echo site_url('consignee/payment_card?id=') . $value['id']; ?>"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                    <a target="_blank"  href="<?php echo site_url('consignee/payment_card?id=') . $value['id']; ?>"><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>
                                    <a href="<?php echo site_url('consignee/payment_card?id=') . $value['id'] . '&email=1'; ?>"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Email&nbsp;</a>
                                    <a title="delete" onclick="del('Cancel', 'btn-red', 'Kindly Confirm', 'Are you sure you want to delete?', '<?php echo base_url('consignee/delete?id=') . $value['id']; ?>');"><i class="fa fa-times fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });
</script>

