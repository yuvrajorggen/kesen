<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Payment</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('consignee/update') : site_url('consignee/insert') ?>"> 
                        <?php
                        $error = $this->session->flashdata('error');
                        if ($error != '') {
                            ?>
                            <div class="alert alert-danger">
                                <strong>Error!</strong> <?php echo $error; ?>
                            </div>
                            <?php
                        }
                        ?>


                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Writer's Name<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select required="required" name="writer" class="searchselected " tabindex="1" autofocus="on" id="writer-name">
                                        <option value="NULL">Select Writer's Name</option>
                                        <?php
                                        foreach ($writer as $key => $value) {
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value['name'] . '(' . $value['code'] . ')'; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="period">Period<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="period" class="date-picker form-control col-md-7" name="period" required="required" type="text" tabindex="2" name=""  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Payment Method<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    Online&nbsp;<input type="radio" name="payment_method" required="required" value="1"><br>
                                    cheque&nbsp;<input type="radio" name="payment_method" required="required" value="2"><br>
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="period">Online-REF no / Cheque No <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="" class="form-control col-md-7" required="required" type="text" tabindex="2" name="payment_meta1"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Metrix<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <select name="estimatecompany" class="searchselected" tabindex="4" id="language">
                                        <?php
                                        $estimates = $this->config->item('estimate');
                                        foreach ($estimates as $key => $value) {
                                            ?>
                                            <option <?php
                                            if (isset($jobregister['estimatecompany']) && $key == $jobregister['estimatecompany']) {
                                                echo 'selected';
                                            }
                                            ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                    </select>
                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="period">Performance Charges </label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="" class="form-control col-md-7" type="text" tabindex="2" name="performance_charges"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Apply GST<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="gstcheckbox" class="form-control col-md-7" type="checkbox" tabindex="2" name="gst"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                    <input id="gstpercentage" class="form-control col-md-7 hidden" type="text" tabindex="2" name="gstpercentage"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 

                                </div>
                                <label class="control-label col-md-1 col-sm-1" for="period">Deductable </label>
                                <div class="col-md-2 col-sm-2">
                                    <input id="" class="form-control col-md-7" type="text" tabindex="2" name="deductable"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Apply TDS<span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2">
                                    <input  id="tdscheckbox" class="form-control col-md-7" type="checkbox" tabindex="2" name="tds"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                    <input id="tdspercentage"  class="form-control col-md-7 hidden" type="text" tabindex="2" name="tdspercentage"  <?php echo isset($mode) && $mode == 'update' ? '' : ''; ?> > 
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn"tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {

        $('.date-picker').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $("#gstcheckbox").click(function () {
            $("#gstpercentage").toggleClass("hidden");
        });
        $("#tdscheckbox").click(function () {
            $("#tdspercentage").toggleClass("hidden");
        });
    });

</script>

<script>
    jQuery(document).ready(function () {



        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("digitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("codeonly", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        });
        jQuery("#employee_add").validate({
            ignore: [],
            rules: {
                'writer': {
                    required: true,
                },
                'tdspercentage': {
                    required: function () {
                        if ($("#tdscheckbox").is(':checked')) {

                            return true;
                        }
                        else
                        {

                            return false;
                        }
                    },
                    rangelength: [1, 2],
                    min: 1,
                    max: 30,
                },
                'gstpercentage': {
                    required: function () {

                        if ($("#gstcheckbox").is(':checked')) {

                            return true;
                        }
                        else
                        {

                            return false;
                        }
                    },
                    rangelength: [1, 2],
                    min: 1,
                    max: 30,
                }
            },
            messages: {
                'writer': {
                    required: "Name Required",
                    lettersonly: "Please enter correct Name without space"
                },
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            }, submitHandler: function (form, event) {

                $("#loading").addClass("loading");

                $.ajax({
                    url: '<?php echo base_url() . 'consignee/check_date_period'; ?>',
                    type: 'POST',
                    data: $(form).serialize(),
                    success: function (response) {
                        
                        $.ajax({
                            url: '<?php echo base_url() . 'consignee/get_staus'; ?>',
                            type: 'POST',
                            data: $(form).serialize(),
                            success: function (response) {
                                form.submit();
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $("#loading").removeClass("loading");
                                event.preventDefault();
                                $.dialog({
                                    title: 'Error Message',
                                    content: 'You have already paid to writer for selected period.',
                                    closeIcon: true,
                                    buttons: {
                                        cancelAction: function () {
                                            alert('fsd');
                                        }
                                    }
                                });
                            }
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#loading").removeClass("loading");
                        event.preventDefault();
                        $.dialog({
                            title: 'Error Message',
                            content: 'date should 1st and last date of respective month. e.g. 01/12/2018 - 31/12/2018',
                            closeIcon: true,
                            buttons: {
                                cancelAction: function () {
                                    alert('fsd');
                                }
                            }
                        });
                    }
                });


            }
        });
    });
</script>

