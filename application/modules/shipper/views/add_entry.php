<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Card</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('shipper/update') : site_url('shipper/insert'); ?>">
                        <input type="hidden" name="jobregister_id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1" for="client">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <h4><?php echo $client['name']; ?></h4>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="headline">Job Card No.<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <h4><?php echo $register['id']; ?></h4>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="headline">Headline<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="3" value="<?php echo $register['headline']; ?>" name="jobregister[headline]" id="headline">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="protocol">Protocol No.<span class="required">*</span></label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="5" value="<?php echo $register['protocolno']; ?>" name="jobregister[protocolno]"  id="protocol">
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1" for="version">Version No.</label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="7" value="<?php echo isset($register['versionno']) ? $register['versionno'] : ''; ?>" name="jobregister[versionno]" id="version">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="date1">Version Date
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date1" class="<?php echo isset($register['versiondate']) && $register['versiondate'] == "0000-00-00" ? 'dateempty' : ''; ?> date-picker form-control col-md-7" name="jobregister[versiondate]" type="text" tabindex="8" readonly value="<?php echo isset($register['versiondate']) && $register['versiondate'] != "0000-00-00" ? date('d/m/y', strtotime($register['versiondate'])) : ''; ?>"> 
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="po">P.O No.</label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="9" value="<?php echo isset($register['pono']) ? $register['pono'] : ''; ?>" name="jobregister[pono]" id="po">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="date2">P.O Informed Date
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date2" class="<?php echo isset($register['poinformeddate']) && $register['poinformeddate'] == "0000-00-00" ? 'dateempty' : ''; ?> date-picker form-control col-md-7" name="jobregister[poinformeddate]" type="text" tabindex="10" readonly value="<?php echo isset($register['poinformeddate']) && $register['poinformeddate'] != "0000-00-00" ? date('d/m/y', strtotime($register['poinformeddate'])) : ''; ?>""> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-1"  for="date3">Delivery Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input id="date3" class="date-picker form-control col-md-7" required="required" type="text" tabindex="11" name="jobregister[deliverydate]" readonly value="<?php echo (isset($register['deliverydate']) && $register['deliverydate'] != "0000-00-00") ? date('d/m/y', strtotime($register['deliverydate'])) : ''; ?>" >  
                                        </div>

                                        <label class="control-label col-md-1 col-sm-1" for="notice">Old Job No.<span class="required"></span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" name="jobregister[oldjobno]" tabindex="13" id="notice" value="<?php echo isset($register['oldjobno']) && $register['oldjobno'] != 0 ? $register['oldjobno'] : ''; ?>">
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="remarks">Remarks<span class="required"></span>
                                        </label>
                                        <div class="col-md-2 col-sm-2">
                                            <textarea id="remarks" class="form-control" name="jobregister[remarks]" tabindex="16"><?php echo isset($register['remarks']) ? $register['remarks'] : ''; ?></textarea>
                                        </div>
                                        <label class="control-label col-md-1 col-sm-1" for="po">Words / Units</label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="text" class="form-control" tabindex="9" value="<?php echo isset($register['wordsunit']) ? $register['wordsunit'] : ''; ?>" name="jobregister[wordsunit]" id="po">
                                        </div>
                                    </div>
                                </div>
                                <?php if ($_SESSION['user']['role'] == 3) { ?>
                                    <div>
                                        <label class="control-label col-md-1 col-sm-1" for="po">Checked with Operator</label>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="radio" value="1" 
                                            <?php
                                            if ($register['checkedwithoperator'] == 1) {
                                                echo 'checked';
                                            }
                                            ?>
                                                   name="jobregister[checkedwithoperator]">Yes
                                            <input <?php
                                            if ($register['checkedwithoperator'] == 2) {
                                                echo 'checked';
                                            }
                                            ?>
                                                type="radio" value="2"  name="jobregister[checkedwithoperator]">No
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-12 list-table">
                            <table class="table table-bordered">
                                <tr>
                                    <th colspan="2">Language</th>
                                    <th>Task</th>
                                    <th>Unit</th>
                                    <th>Writer Code</th>
                                    <th>Employee Code</th>
                                    <th>PD</th>
                                    <th>CR</th>
                                    <th>C/NC</th>
                                    <th>DV</th>
                                    <th>F/QC</th>
                                    <th>Sent Date</th>
                                </tr>
                                <?php
                                $count = 1;
                                foreach ($jobreglang as $key => $value) {
                                    ?>
                                    <tr>
                                        <td colspan="1" rowspan="4"><?php echo $language[$value]; ?></td>
                                        <td>T
                                            <input type="hidden" value="1" name="type<?php echo '[' . $value . ']'; ?>[]">
                                            <input type="hidden" value="<?php echo isset($jobcard[$value][1]['id']) ? $jobcard[$value][1]['id'] : '0'; ?>" name="job_card_id<?php echo '[' . $value . ']'; ?>[]">
                                        </td>
                                        <td><input value="1" <?php echo isset($jobcard[$value][1]['task']) && $jobcard[$value][1]['task'] == 1 ? 'checked' : ''; ?> type="checkbox" class="form-control" name="task<?php echo '[' . $value . ']'; ?>[0]" tabindex="17"></td>
                                        <td><input value="<?php echo isset($jobcard[$value][1]['unit']) && $jobcard[$value][1]['unit'] != 0 ? $jobcard[$value][1]['unit'] : ''; ?>" type="number" class="form-control" name="unit<?php echo '[' . $value . ']'; ?>[]" tabindex="17"></td>
                                        <td><input onkeypress="get_suggestion('suggestin_<?php echo $count; ?>');" id="suggestin_<?php echo $count; ?>" data-lang="<?php echo $value; ?>" value="<?php echo isset($jobcard[$value][1]['writer_id']) ? $jobcard[$value][1]['writer_id'] : ''; ?>" type="text" class="form-control autocomplete-writer" name="writer_id<?php echo '[' . $value . ']'; ?>[]" maxlength="3" tabindex="18"></td>
                                        <td><input type="text" value="<?php echo isset($jobcard[$value][1]['employeecode']) ? $jobcard[$value][1]['employeecode'] : ''; ?>"  name="employeecode<?php echo '[' . $value . ']'; ?>[]"  class="form-control autocomplete-employee" ></td>
                                        <td><input value="<?php echo isset($jobcard[$value][1]['pd']) && $jobcard[$value][1]['pd'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][1]['pd'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][1]['pd']) && $jobcard[$value][1]['pd'] != NULL) ? '' : 'dateempty'; ?> date-picker form-control col-md-7"  name="pd<?php echo '[' . $value . ']'; ?>[]"   type="text" tabindex="8" > </td>
                                        <td><input  value="<?php echo isset($jobcard[$value][1]['cr']) && $jobcard[$value][1]['cr'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][1]['cr'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][1]['cr']) && $jobcard[$value][1]['cr'] != NULL) ? '' : 'dateempty'; ?> date-picker form-control col-md-7"  name="cr<?php echo '[' . $value . ']'; ?>[]"  type="text" tabindex="8" > </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on" name="cnc<?php echo '[' . $value . ']'; ?>[]">
                                                <option value="NULL"></option>
                                                <option value="1"  <?php echo (isset($jobcard[$value][1]['cnc']) && $jobcard[$value][1]['cnc'] == 1 ) ? 'selected' : ''; ?>>C</option>
                                                <option value="2"  <?php echo (isset($jobcard[$value][1]['cnc']) && $jobcard[$value][1]['cnc'] == 2 ) ? 'selected' : ''; ?>>NC</option>
                                            </select></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][1]['dv']) ? $jobcard[$value][1]['dv'] : ''; ?>" type="text" class="form-control" tabindex="23" name="dv<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][1]['fqc']) ? $jobcard[$value][1]['fqc'] : ''; ?>"  type="text" class="form-control" tabindex="24" name="fqc<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input value="<?php echo isset($jobcard[$value][1]['sentdate']) && $jobcard[$value][1]['sentdate'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][1]['sentdate'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][1]['sentdate']) && $jobcard[$value][1]['sentdate'] != NULL) ? '' : 'dateempty'; ?> date-picker form-control col-md-7" name="sentdate<?php echo '[' . $value . ']'; ?>[]" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <?php $count++; ?>
                                    <tr>

                                        <td>V
                                            <input type="hidden" value="2" name="type<?php echo '[' . $value . ']'; ?>[]">
                                            <input type="hidden" value="<?php echo isset($jobcard[$value][2]['id']) ? $jobcard[$value][2]['id'] : '0'; ?>" name="job_card_id<?php echo '[' . $value . ']'; ?>[]">
                                        </td>
                                        <td><input  value="1" <?php echo isset($jobcard[$value][2]['task']) && $jobcard[$value][2]['task'] == 1 ? 'checked' : ''; ?> type="checkbox" class="form-control" name="task<?php echo '[' . $value . ']'; ?>[1]" tabindex="17"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][2]['unit']) && $jobcard[$value][2]['unit'] != 0 ? $jobcard[$value][2]['unit'] : ''; ?>" type="number" class="form-control" name="unit<?php echo '[' . $value . ']'; ?>[]" tabindex="17"></td>
                                        <td><input onkeypress="get_suggestion('suggestin_<?php echo $count; ?>');" id="suggestin_<?php echo $count; ?>" data-lang="<?php echo $value; ?>"  value="<?php echo isset($jobcard[$value][2]['writer_id']) ? $jobcard[$value][2]['writer_id'] : ''; ?>"  type="text" class="form-control autocomplete-writer" name="writer_id<?php echo '[' . $value . ']'; ?>[]" maxlength="3" tabindex="18"></td>
                                        <td><input type="text" value="<?php echo isset($jobcard[$value][2]['employeecode']) ? $jobcard[$value][2]['employeecode'] : ''; ?>"  name="employeecode<?php echo '[' . $value . ']'; ?>[]"  class="form-control autocomplete-employee" ></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][2]['pd']) && $jobcard[$value][2]['pd'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][2]['pd'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][2]['pd']) && $jobcard[$value][2]['pd'] != NULL ) ? '' : 'dateempty '; ?> date-picker form-control col-md-7"  name="pd<?php echo '[' . $value . ']'; ?>[]"  type="text" tabindex="8" > </td>
                                        <td><input  value="<?php echo isset($jobcard[$value][2]['cr']) && $jobcard[$value][2]['cr'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][2]['cr'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][2]['cr']) && $jobcard[$value][2]['cr'] != NULL ) ? '' : 'dateempty '; ?> date-picker form-control col-md-7"  name="cr<?php echo '[' . $value . ']'; ?>[]"   type="text" tabindex="8" > </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on" name="cnc<?php echo '[' . $value . ']'; ?>[]">
                                                <option value="NULL"></option>
                                                <option value="1"   <?php echo (isset($jobcard[$value][2]['cnc']) && $jobcard[$value][2]['cnc'] == 1 ) ? 'selected' : ''; ?>>C</option>
                                                <option value="2"  <?php echo (isset($jobcard[$value][2]['cnc']) && $jobcard[$value][2]['cnc'] == 2 ) ? 'selected' : ''; ?>>NC</option>
                                            </select></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][2]['dv']) ? $jobcard[$value][2]['dv'] : ''; ?>"  type="text" class="form-control" tabindex="23" name="dv<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][2]['fqc']) ? $jobcard[$value][2]['fqc'] : ''; ?>"  type="text" class="form-control" tabindex="24" name="fqc<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input value="<?php echo isset($jobcard[$value][2]['sentdate']) && $jobcard[$value][2]['sentdate'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][2]['sentdate'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][2]['sentdate']) && $jobcard[$value][2]['sentdate'] != NULL) ? '' : 'dateempty '; ?> date-picker form-control col-md-7" name="sentdate<?php echo '[' . $value . ']'; ?>[]" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <?php $count++; ?>
                                    <tr>

                                        <td>BT
                                            <input type="hidden" value="3" name="type<?php echo '[' . $value . ']'; ?>[]">
                                            <input type="hidden" value="<?php echo isset($jobcard[$value][3]['id']) ? $jobcard[$value][3]['id'] : '0'; ?>" name="job_card_id<?php echo '[' . $value . ']'; ?>[]">
                                        </td>
                                        <td><input  value="1" <?php echo isset($jobcard[$value][3]['task']) && $jobcard[$value][3]['task'] == 1 ? 'checked' : ''; ?> type="checkbox" class="form-control" name="task<?php echo '[' . $value . ']'; ?>[2]" tabindex="17"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][3]['unit']) && $jobcard[$value][3]['unit'] != 0 ? $jobcard[$value][3]['unit'] : ''; ?>" type="number" class="form-control" name="unit<?php echo '[' . $value . ']'; ?>[]" tabindex="17"></td>
                                        <td><input   onkeypress="get_suggestion('suggestin_<?php echo $count; ?>');" id="suggestin_<?php echo $count; ?>" data-lang="<?php echo $value; ?>" value="<?php echo isset($jobcard[$value][3]['writer_id']) ? $jobcard[$value][3]['writer_id'] : ''; ?>" type="text" class="form-control autocomplete-writer" name="writer_id<?php echo '[' . $value . ']'; ?>[]" maxlength="3" tabindex="18"></td>
                                        <td><input type="text" value="<?php echo isset($jobcard[$value][3]['employeecode']) ? $jobcard[$value][3]['employeecode'] : ''; ?>"  name="employeecode<?php echo '[' . $value . ']'; ?>[]"  class="form-control autocomplete-employee" ></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][3]['pd']) && $jobcard[$value][3]['pd'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][3]['pd'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][3]['pd']) && $jobcard[$value][3]['pd'] != NULL) ? '' : ' dateempty '; ?>date-picker form-control col-md-7"  name="pd<?php echo '[' . $value . ']'; ?>[]"   type="text" tabindex="8" > </td>
                                        <td><input  value="<?php echo isset($jobcard[$value][3]['cr']) && $jobcard[$value][3]['cr'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][3]['cr'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][3]['cr']) && $jobcard[$value][3]['cr'] != NULL) ? '' : ' dateempty '; ?> date-picker form-control col-md-7"  name="cr<?php echo '[' . $value . ']'; ?>[]"  type="text" tabindex="8" > </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on" name="cnc<?php echo '[' . $value . ']'; ?>[]">
                                                <option value="NULL"></option>
                                                <option value="1"   <?php echo (isset($jobcard[$value][3]['cnc']) && $jobcard[$value][3]['cnc'] == 1 ) ? 'selected' : ''; ?>>C</option>
                                                <option value="2"   <?php echo (isset($jobcard[$value][3]['cnc']) && $jobcard[$value][3]['cnc'] == 2 ) ? 'selected' : ''; ?>>NC</option>
                                            </select></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][3]['dv']) ? $jobcard[$value][3]['dv'] : ''; ?>"  type="text" class="form-control" tabindex="23" name="dv<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][3]['fqc']) ? $jobcard[$value][3]['fqc'] : ''; ?>"  type="text" class="form-control" tabindex="24" name="fqc<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input value="<?php echo isset($jobcard[$value][3]['sentdate']) && $jobcard[$value][3]['sentdate'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][3]['sentdate'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][3]['sentdate']) && $jobcard[$value][3]['sentdate'] != NULL) ? '' : ' dateempty '; ?> date-picker form-control col-md-7" name="sentdate<?php echo '[' . $value . ']'; ?>[]" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <?php $count++; ?>
                                    <tr>

                                        <td>BTV
                                            <input type="hidden" value="4" name="type<?php echo '[' . $value . ']'; ?>[]">
                                            <input type="hidden" value="<?php echo isset($jobcard[$value][4]['id']) ? $jobcard[$value][4]['id'] : '0'; ?>" name="job_card_id<?php echo '[' . $value . ']'; ?>[]">
                                        </td>
                                        <td><input  value="1" <?php echo isset($jobcard[$value][4]['task']) && $jobcard[$value][4]['task'] == 1 ? 'checked' : ''; ?> type="checkbox" class="form-control" name="task<?php echo '[' . $value . ']'; ?>[3]" tabindex="17"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][4]['unit']) && $jobcard[$value][4]['unit'] != 0 ? $jobcard[$value][4]['unit'] : ''; ?>" type="number" class="form-control" name="unit<?php echo '[' . $value . ']'; ?>[]" tabindex="17"></td>
                                        <td><input   onkeypress="get_suggestion('suggestin_<?php echo $count; ?>');" id="suggestin_<?php echo $count; ?>" data-lang="<?php echo $value; ?>" value="<?php echo isset($jobcard[$value][4]['writer_id']) ? $jobcard[$value][4]['writer_id'] : ''; ?>" type="text" class="form-control autocomplete-writer" name="writer_id<?php echo '[' . $value . ']'; ?>[]" maxlength="3" tabindex="18"></td>
                                        <td><input type="text" value="<?php echo isset($jobcard[$value][4]['employeecode']) ? $jobcard[$value][4]['employeecode'] : ''; ?>"  name="employeecode<?php echo '[' . $value . ']'; ?>[]"  class="form-control autocomplete-employee" ></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][4]['pd']) && $jobcard[$value][4]['pd'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][4]['pd'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][4]['pd']) && $jobcard[$value][4]['pd'] != NULL) ? '' : 'dateempty '; ?>date-picker form-control col-md-7"  name="pd<?php echo '[' . $value . ']'; ?>[]" type="text" tabindex="8" > </td>
                                        <td><input  value="<?php echo isset($jobcard[$value][4]['cr']) && $jobcard[$value][4]['cr'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][4]['cr'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][4]['cr']) && $jobcard[$value][4]['cr'] != NULL) ? '' : 'dateempty '; ?> date-picker form-control col-md-7"  name="cr<?php echo '[' . $value . ']'; ?>[]"   type="text" tabindex="8" > </td>
                                        <td><select class="chosen-select" tabindex="1" autofocus="on" name="cnc<?php echo '[' . $value . ']'; ?>[]">
                                                <option value="NULL"></option>
                                                <option value="1"   <?php echo (isset($jobcard[$value][4]['cnc']) && $jobcard[$value][4]['cnc'] == 1 ) ? 'selected' : ''; ?>>C</option>
                                                <option value="2"   <?php echo (isset($jobcard[$value][4]['cnc']) && $jobcard[$value][4]['cnc'] == 2 ) ? 'selected' : ''; ?>>NC</option>
                                            </select></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][4]['dv']) ? $jobcard[$value][4]['dv'] : ''; ?>"  type="text" class="form-control" tabindex="23" name="dv<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input  value="<?php echo isset($jobcard[$value][4]['fqc']) ? $jobcard[$value][4]['fqc'] : ''; ?>"  type="text" class="form-control" tabindex="24" name="fqc<?php echo '[' . $value . ']'; ?>[]"></td>
                                        <td><input value="<?php echo isset($jobcard[$value][4]['sentdate']) && $jobcard[$value][4]['sentdate'] != "0000-00-00" ? date('d/m/y', strtotime($jobcard[$value][4]['sentdate'])) : ''; ?>" id="date" class="<?php echo (isset($jobcard[$value][4]['sentdate']) && $jobcard[$value][4]['sentdate'] != NULL) ? '' : 'dateempty '; ?> date-picker form-control col-md-7"name="sentdate<?php echo '[' . $value . ']'; ?>[]" type="text" tabindex="8" readonly> </td>
                                    </tr>
                                    <?php $count++; ?>

                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                </div>
                <div class="row">
                    <div class="submit">
                        <button id="submitButtonId" type="submit" class="btn btn-default submit-btn pull-right" tabindex="87">Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>

<link rel="stylesheet" href="<?php echo CSS_PATH_FRONTEND; ?>jquery-ui.css">
<!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery-ui.js"></script>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>

<script>
                                        $(document).ready(function () {
                                            $('.date-picker').daterangepicker({
                                                "singleDatePicker": true,
                                                "showDropdowns": true,
                                                "showWeekNumbers": true,
                                                "showISOWeekNumbers": true,
                                                "autoUpdateInput": true,
                                                "autoApply": true,
                                                "alwaysShowCalendars": true,
                                                ranges: {
                                                    'Today': [moment(), moment()],
                                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                                },
                                                locale: {
                                                    format: 'DD/MM/YYYY'
                                                }
                                            });
<?php if (!isset($mode)) { ?>
                                                $('.date-picker').val('');
<?php } ?>
                                            $('.dateempty').val('');

                                        });

                                        function get_suggestion(id) {

                                            $(".autocomplete-writer").autocomplete({
                                                source: function (request, response) {

                                                    $.ajax({
                                                        url: '<?php echo base_url('customer/get_writers'); ?>',
                                                        dataType: "json",
                                                        beforeSend: function () {
                                                            $("#loading").addClass("loading");
                                                        },
                                                        data: {
                                                            term: request.term,
                                                            lang: $('#' + id).data("lang")
                                                        },
                                                        success: function (data) {
                                                            $("#loading").removeClass("loading");
                                                            response(data);
                                                        },
                                                        error: function (data) {
                                                            $("#loading").removeClass("loading");
                                                        }
                                                    });
                                                },
                                                change: function (event, ui) {
                                                    $(this).val((ui.item ? ui.item.value : ""));
                                                },
                                                minLength: 1,
                                            });

                                        }

                                        $(".autocomplete-employee").autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    url: '<?php echo base_url('employee/get_employee'); ?>',
                                                    dataType: "json",
                                                    beforeSend: function () {
                                                        $("#loading").addClass("loading");
                                                    },
                                                    data: {
                                                        term: request.term,
                                                    },
                                                    success: function (data) {
                                                        $("#loading").removeClass("loading");
                                                        response(data);
                                                    },
                                                    error: function (data) {
                                                        $("#loading").removeClass("loading");
                                                    }
                                                });
                                            },
                                            change: function (event, ui) {
                                                $(this).val((ui.item ? ui.item.value : ""));
                                            },
                                            minLength: 1,
                                        });

</script>

<script>
    jQuery(document).ready(function () {



        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("digitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("codeonly", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        });
        jQuery("#odv_add").validate({
            ignore: [],
            rules: {
                'jobregister[headline]': {
                    required: true,
                    rangelength: [3, 500],
                },
                'jobregister[protocolno]': {
                },
                'jobregister[versionno]': {
                },
                'jobregister[versiondate]': {
                },
                'jobregister[pono]': {
                },
                'jobregister[poinformeddate]': {
                },
                'jobregister[deliverydate]': {
                    required: true
                },
                'jobregister[oldjobno]': {
                    digitonly: true,
                },
                'jobregister[remarks]': {
                    rangelength: [0, 500],
                }
            },
            messages: {
                'client[email]': {
                    required: "Name Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'jobregister[versionno]': {
                    required: "Required",
                    lettersonly: "Please enter correct Name without space",
                    codeonly: "Space not allowed"
                },
                'jobregister[versiondate]': {
                    required: "Required",
                },
                'contact_no': {
                    required: "Contact No Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'txtmessage': {
                    required: "Message Required",
                    lettersonly: "Please enter correct Name without space"
                },
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            }
        });
    });
</script>


