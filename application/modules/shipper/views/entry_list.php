<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Job Card</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('shipper/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-1 col-sm-1">Job No.<span class="required">*</span></label>
                            <label class="control-label col-md-1 col-sm-1">Date<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Handled By<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Writer's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Client's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Protocol No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2">Headline<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">

                            <div class="col-md-1 col-sm-1">
                                <input type="text" class="form-control" tabindex="1" autofocus="on" name="jobcode" value="<?php echo isset($_GET['jobcode']) ? $_GET['jobcode'] : ''; ?>">
                            </div>
                            <div class="col-md-1 col-sm-1">                  
                                <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="2" readonly name="date"> 
                            </div>


                            <div class="col-md-2 col-sm-2">
                                <select class="active 3col" tabindex="3" name="handled_by[]" multiple="">
                                    <option>Select Handled By</option>
                                    <?php
                                    foreach ($employee as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo ucwords($value['name']); ?></option>>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <div class="col-md-2 col-sm-2">
                                <select class="active 3col" tabindex="4" name="writers[]" multiple="">

                                    <?php
                                    foreach ($writer as $key => $value) {
                                        ?>
                                        <option value="<?php echo $value['code']; ?>"><?php echo ucwords($value['name']); ?></option>>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <div class="col-md-2 col-sm-2">
                                <select class="client active 3col" tabindex="4" name="client[]" multiple="">

                                    <?php
                                    foreach ($client as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo ucwords($value); ?></option>>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>


                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on"  name="protocol" value="<?php echo isset($_GET['protocol']) ? $_GET['protocol'] : ''; ?>">
                            </div>


                            <div class="col-md-2 col-sm-2">
                                <input type="text" class="form-control" tabindex="1" autofocus="on"  name="headline" value="<?php echo isset($_GET['headline']) ? $_GET['headline'] : ''; ?>">
                            </div>

                        </div>

                        <div class="form-group pull-right">
                            <input type="submit" value="Search" class="btn btn-default submit-btn">
                            <a tabindex="7" class="btn btn-default view-btn" title="View All Job Card" href="<?php echo base_url('shipper/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a target="_blank" tabindex="7" class="btn btn-default view-btn" title="View All Job Card" href="<?php echo base_url('shipper/entry_list?print=1&') . $_SERVER['QUERY_STRING']; ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Print</a>
                        </div>

                    </form>
                </div>
                <div class="table_content">
                    <h3>Job Card List</h3>
                    <div class="total">
                        <span>Total Display : <?php echo count($list); ?></span>
                        <span>Total Job Cards : <?php echo $total; ?></span>
                    </div>
                    <?php echo $this->session->flashdata('message'); ?>
                    <?php echo $this->pagination->create_links(); ?>
                    <div class="list-table">
                        <table class="table table-bordered view-table">
                            <thead>
                                <tr class="table_heading">
                                    <th>Job Card No.</th>
                                    <th>Date</th>
                                    <th>Protocol No.</th>
                                    <th>Client Name</th>
                                    <th>Description</th>
                                    <th>Handled By</th>
                                    <th>Bill No.</th>
                                    <th>Bill Date</th>
                                    <th>informedto</th>
                                    <th>invoicedate</th>
                                    <th width="53%">Actions</th>
                                </tr>
                            </thead>
                            <?php
                            foreach ($list as $key => $value) {
                                ?>
                                <tbody>
                                    <tr>
                                        <td><?php echo $value['id']; ?></td>
                                        <td><?php echo date('d M Y', strtotime($value['date'])); ?></td>
                                        <td><?php echo $value['protocolno']; ?></td>
                                        <td><?php echo $client[$value['client_id']]; ?></td>
                                        <td><?php echo $value['remarks']; ?></td>
                                        <td><?php echo $employee[$value['handledby']]['name']; ?></td>
                                        <td><?php echo ($value['billno'] != 0) ? $value['billno'] : $value['billno']; ?></td>
                                        <td><?php echo ($value['billsentdate'] != 0000 - 00 - 00) ? date('d M Y', strtotime($value['billsentdate'])) : ''; ?></td>
                                        <td><?php echo $value['informedto']; ?></td>
                                        <td><?php echo ($value['invoicedate'] != 0000 - 00 - 00) ? date('d M Y', strtotime($value['invoicedate'])) : ''; ?></td>
                                        <td>
                                            <a  target="_blank" title="View Job Card" href="<?php echo site_url('shipper/job_card/print?id=') . $value['id']; ?>"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View</a>
                                            <a title="Update Job Card" href="<?php echo site_url('shipper/edit?id=') . $value['id']; ?>"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                            <a target="_blank"  title="Print Job Card" href="<?php echo site_url('shipper/job_card/print?id=') . $value['id']; ?>"><i class="fa fa-print fa-lg" aria-hidden="true"></i>&nbsp;Print&nbsp;</a>

                                            <a title="Send Confirmation Letter" href="<?php echo site_url('shipper/send_confirmation?id=') . $value['id']; ?>"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Send Confirmation Letter&nbsp;</a>
                                            <a title="Send Follow Up Letter" href="<?php echo site_url('shipper/send_follow_up_letter?id=') . $value['id']; ?>"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;Send Follow Up Letter&nbsp;</a>
                                            <a title="View Payment" href="<?php echo site_url('shipper/payment?id=') . $value['id']; ?>"><i class="fa fa-inr fa-lg" aria-hidden="true"></i>&nbsp;Payment</a>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "timePickerIncrement": 1,
            "alwaysShowCalendars": true,
            "opens": "center",
            "drops": "down",
            "buttonClasses": "btn btn-sm",
            "applyClass": "btn-success",
            "cancelClass": "btn-default",
            locale: {
                cancelLabel: 'Clear'
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $("#birthday1").val('');
        $(".client1").select2({
            ajax: {
                url: "<?php echo base_url() . 'commodity/get_client'; ?>",
                type: 'POST',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, page) {
//                    console.log(data);
//                    console.log(page);
//                    results: $.map(data, function (obj) {
//                        return {id: obj.id, text: obj.text};
//                    });
                    $(".client").html('');
                    $.each($.parseJSON(data), function (key, value) {
                        $(".client").select2({data: [{id: key, text: value}]});
                    });
                },
                //cache: true
            },
            minimumInputLength: 2,
        });

    });
</script>
<!-- /Select2 -->