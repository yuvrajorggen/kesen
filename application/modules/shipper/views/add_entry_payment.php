<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Card Payment</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('shipper/payment_update') : site_url('shipper/payment_update'); ?>">
                        <input type="hidden" name="id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <h4><?php echo $client['name']; ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6">Invoice Sent Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="3" readonly name="jobregister[billsentdate]" value="<?php echo ($register['billsentdate'] != 0000 - 00 - 00) ? date('d/m/y', strtotime($register['billsentdate'])) : ''; ?>" > 
                                        </div>
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-sm-offset-5">Invoice No<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="5" value="<?php echo $register['billno'] ?>" required="required"  name="jobregister[billno]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <?php echo $register['id']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2">Informed To<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" required="" class="form-control" tabindex="4" value="<?php echo ($register['informedto']); ?>"   name="jobregister[informedto]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2">Invoice Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="6" readonly    name="jobregister[invoicedate]"   value="<?php echo ($register['invoicedate'] != 0000 - 00 - 00) ? date('d/m/y', strtotime($register['invoicedate'])) : ''; ?>"  > 
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-sm-offset-2" for="writer-name">Metrix<span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2">
                                <select disabled name="jobregister[billcompany]" class="searchselected" tabindex="4" id="language">
                                    <?php
                                    $estimates = $this->config->item('estimate');
                                    foreach ($estimates as $key => $value) {
                                        ?>
                                    <option  <?php
                                        if (isset($register['estimatecompany']) && $key == $register['estimatecompany']) {
                                            echo 'selected';
                                        }
                                        ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php
                                        }
                                        ?>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn" tabindex="13">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "autoUpdateInput": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
        $('.date-picker').val();
    });

</script>

<script>

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("digitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
        return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
    });
    jQuery.validator.addMethod("codeonly", function (value, element) {
        return value.indexOf(" ") < 0 && value != "";
    });
    $("#odv_add").validate({
        ignore: [],
        rules: {
            'obregister[informedto]': {
                required: true,
            },
            'jobregister[clientcontacts_id]': {
                required: true,
            },
            'jobregister[handledby]': {
                required: true,
            },
            'jobregister[writer_id]': {
                required: true,
            }, 'jobregister[otherdetails]': {
            },
            'jobreglang[]': {
                required: true,
            }, 'jobregister[estimatecompany]': {
                required: true,
            }, 'jobregister[estimateno]': {
                required: true,
            },
            'jobregister[headline]': {
                required: true,
                rangelength: [3, 500],
            },
            'jobregister[protocolno]': {
                required: true,
                rangelength: [3, 500]
            }
        },
        messages: {
            'employee[name]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }, 'employee[contactno]': {
                required: "Name Required",
                rangelength: "Minimum 3 & Maximum 50 Character Required",
                lettersonly: "Enter Correct Name without space"
            }
        },
        errorElement: 'p',
        errorPlacement: function (error, element) {
            jQuery(element).parent().append(error);
        }
    });
</script>