<?php

class Shipper extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['register']['clientcontacts_id']));
        $jobreglang = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }
        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['code']] = '"' . $value['code'] . '"';
        }

        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function payment() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $view = 'add_entry_payment';
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->_display($view, $data);
    }

    function insert() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');

        $jobregister_id = $this->input->post('jobregister_id');
        $type = $this->input->post('type');
        $unit = $this->input->post('unit');
        $writer_id = $this->input->post('writer_id');
        $task = $this->input->post('task');
        $pd = $this->input->post('pd');
        $cr = $this->input->post('cr');
        $cnc = $this->input->post('cnc');
        $dv = $this->input->post('dv');
        $fqc = $this->input->post('fqc');
        $sentdate = $this->input->post('sentdate');


        $jobregister = $this->input->post('jobregister');
        $jobregister['versiondate'] = $this->df($jobregister['versiondate']);
        $jobregister['poinformeddate'] = $this->df($jobregister['poinformeddate']);
        $jobregister['deliverydate'] = $this->df($jobregister['deliverydate']);
        $jobregister['duedate'] = $this->df($jobregister['duedate']);

        $this->gm->update('jobregister', $jobregister, $id = 0, $where = array('id' => $jobregister_id));


        $sessiondata = $this->session->userdata('user');

        foreach ($type as $key => $value) {
            for ($count = 0; $count < 4; $count++) {
                $temp = array(
                    'jobregister_id' => $jobregister_id,
                    'language_id' => $key,
                    'type' => $value[$count],
                    'unit' => $unit[$key][$count],
                    'writer_id' => $writer_id[$key][$count],
                    'task' => $task[$key][$count],
                    'pd' => $this->df($pd[$key][$count]),
                    'cr' => $this->df($cr[$key][$count]),
                    'cnc' => $cnc[$key][$count],
                    'dv' => $dv[$key][$count],
                    'fqc' => $fqc[$key][$count],
                    'sentdate' => $this->df($sentdate[$key][$count])
                );
                $this->gm->insert('jobcard', $temp);
            }
        }



        redirect(site_url('shipper/entry_list'));
    }

    function send_confirmation() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['register']['clientcontacts_id']));
        $jobreglang = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $data['jobcard'] = array();
        $jobcard = $this->gm->get_data_list('jobcard', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($jobcard as $key => $value) {
            $data['jobcard'][$value['language_id']][$value['type']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value['code'];
            //$data['writercode'][$value['code']] = $value;
        }


        $result = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
            //$data['employeecode'][$value['code']] = $value;
        }


        if (isset($_GET['send']) && $_GET['send']) {
            $this->load->helper('email');
            $body = $this->load->view('send_confirmation_letter', $data, TRUE);

            $response = send_email($data['clientcontacts']['email'], 'Job Confirmation Letter' . ' : ' . $data['client']['name'], $body, 'kesen@kesen.in', $reply_email = "", $cc_email = "", $attachment = array());
            if ($response) {
                $this->gm->update('jobregister', array('send_confirmation_status' => 2), 0, array('id' => $data['register']['id']));
            }

            $message = 'Email send successfully on email id : ' . $data['clientcontacts']['email'];
            $this->session->set_flashdata('success', $message);

            redirect('shipper/entry_list');
        } else {
            echo $this->load->view('send_confirmation_letter', $data, TRUE);
            if ($data['register']['send_confirmation_status'] == 1) {
                echo '<a style="
    text-align: center;
    font-size: 24px;
    background: aliceblue;
    display: block;
    text-decoration: none;
    border: 1px solid;
" href="' . base_url('shipper/send_confirmation?id=' . $_GET['id'] . '&send=1') . '">Send Email</a>';
            }
        }
    }

    function entry_list() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $this->load->model('Global_model', 'gm');


        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * PER_PAGE;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $count = array();

        $query = "SELECT * FROM `jobregister` WHERE `status` = 1  ";

        $jobno = $this->input->get('jobcode');
        $date = $this->input->get('date');
        $handled_by = $this->input->get('handled_by');
        $writers = $this->input->get('writers');
        $client = $this->input->get('client');
        $protocol = $this->input->get('protocol');
        $headline = $this->input->get('headline');

        $datearray = explode('-', $date);

        if (isset($datearray[1]) && is_array($datearray)) {
            $sdate = DateTime::createFromFormat('m/d/Y', trim($datearray[0]));
            $edate = DateTime::createFromFormat('m/d/Y', trim($datearray[1]));
        }
        $where = "";
        if ($jobno != '') {
            $where .= " AND `id` = '" . $jobno . "' ";
        }
        if ($date != '') {
            $where .= " AND `date` BETWEEN  '" . $sdate->format("Y-m-d") . "' AND '" . $edate->format("Y-m-d") . "' ";
        }
        if (is_array($handled_by) && count($handled_by) > 0) {
            $where .= " AND `handledby` IN (" . implode(',', $handled_by) . ") ";
        }

        if (is_array($writers) && count($writers) > 0) {

            foreach ($writers as $fruit) {
                $fruitList[] = "'" . $fruit . "'";
            }
            $wquery = "SELECT * FROM `jobcard` WHERE `writer_id` IN (" . implode(',', $fruitList) . ") ";

            $wquery_exec = $this->db->query($wquery);
            $wresult = $wquery_exec->result_array();
            if (isset($wresult) && is_array($wresult) && count($wresult) > 0) {
                foreach ($wresult as $key => $value) {
                    $jobregister_id[] = $value['jobregister_id'];
                }
                $where .= " AND `id` IN (" . implode(',', $jobregister_id) . ") ";
            }
        }
        if (is_array($client) && count($client) > 0) {
            $where .= " AND `client_id` IN (" . implode(',', $client) . ") ";
        }
        if ($protocol != '') {
            $where .= " AND `protocolno` LIKE '%" . $protocol . "%' ";
        }
        if ($headline != '') {
            $where .= " AND `headline` LIKE '%" . $headline . "%' ";
        }
        $perpage = 20;
        if (isset($_GET['print'])) {
            $perpage = 25000;
        }
        $order = "ORDER BY `id`  DESC limit " . $perpage . " offset " . $offset . "";
        $final_query = $query . $where . $order;

        $query_exec = $this->db->query($final_query);
        $result = $query_exec->result_array();
        $data['list'] = $result;

        foreach ($result as $key => $value) {
            $client_ids[] = $value['client_id'];
        }

        $result = $this->gm->get_data_list('client', array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value['name'];
        }
        $cquery = "SELECT count(id) as id FROM `jobregister` WHERE `status` = 1 " . $where;

        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->row_array();


        $data['total'] = isset($count['id']) ? $count['id'] : 0;
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('shipper/entry_list');
        $config['total_rows'] = isset($count['id']) ? $count['id'] : 0;

        $config['per_page'] = PER_PAGE;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';
        $result = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 3000, array());

        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 3000, array());

        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value;
        }
        //pagination end
        if (isset($_GET['print'])) {
            $this->prints($data);
        } else {
            $this->_display($view, $data);
        }
    }

    function prints($data) {
        $this->load->helper('pdf_helper');
        $this->load->helper('url');

        $result = $this->gm->get_data_list('client', array(), $like = array(), $order_by = array(), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['client'][$value['id']] = $value;
        }
        $result = $this->gm->get_data_list('clientcontacts', $where = array(), $like = array(), $order_by = array('id' => 'asc'), $select = '*', $limit = 0, array());
        foreach ($result as $key => $value) {
            $data['clientcontacts'][$value['id']] = $value;
        }


        $pdfname = 'media/pdfs/report-' . rand(0000, 9999) . '.pdf';
        if ($_GET['bill'] == 1) {
            $view = "job_register_list_print";
        } else {
            $view = "job_register_list_print";
        }


        $path = save_pdf($view, $data, $pdfname, 'landscape');
        redirect(base_url() . $path);
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function update() {
        $this->load->helper('url');
        $this->load->library('user_agent');
        $data = array();
        $sessiondata = $this->session->userdata('user');
        $this->load->model('Global_model', 'gm');
        $jobregister_id = $this->input->post('jobregister_id');
        $type = $this->input->post('type');
        $unit = $this->input->post('unit');
        $writer_id = $this->input->post('writer_id');
        $employeecode = $this->input->post('employeecode');
        $task = $this->input->post('task');
        $pd = $this->input->post('pd');
        $cr = $this->input->post('cr');
        $cnc = $this->input->post('cnc');
        $dv = $this->input->post('dv');
        $fqc = $this->input->post('fqc');
        $sentdate = $this->input->post('sentdate');

        $jobregister = $this->input->post('jobregister');
        $jobregister['versiondate'] = $this->df($jobregister['versiondate']);
        $jobregister['poinformeddate'] = $this->df($jobregister['poinformeddate']);
        $jobregister['deliverydate'] = $this->df($jobregister['deliverydate']);


        $this->gm->update('jobregister', $jobregister, $id = 0, $where = array('id' => $jobregister_id));

        /*
         * Comment by kanchan bcz job_id deleted here affect payment advice report generated bofore 
         */
        //$query = 'delete from jobcard  where jobregister_id =' . $jobregister_id;
        //$query_exec = $this->db->query($query);
        $job_card_id = $this->input->post('job_card_id');
        foreach ($type as $key => $value) {
            for ($count = 0; $count < 4; $count++) {
                $temp = array(
                    'jobregister_id' => $jobregister_id,
                    'language_id' => $key,
                    'type' => $value[$count],
                    'unit' => $unit[$key][$count],
                    'writer_id' => $writer_id[$key][$count],
                    'employeecode' => $employeecode[$key][$count],
                    'task' => $task[$key][$count],
                    'pd' => $this->df($pd[$key][$count]),
                    'cr' => $this->df($cr[$key][$count]),
                    'cnc' => $cnc[$key][$count],
                    'dv' => $dv[$key][$count],
                    'fqc' => $fqc[$key][$count],
                    'sentdate' => $this->df($sentdate[$key][$count])
                );

                if (isset($job_card_id[$key][$count]) && $job_card_id[$key][$count] > 0) {
                    //update job card
                    $this->gm->update('jobcard', $temp, '', array('id' => $job_card_id[$key][$count]));
                } else {
                    //insert new job card
                    $this->gm->insert('jobcard', $temp);
                }
            }
        }

        redirect("shipper/entry_list");
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['register']['clientcontacts_id']));
        $jobreglang = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $jobcard = $this->gm->get_data_list('jobcard', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($jobcard as $key => $value) {
            $data['jobcard'][$value['language_id']][$value['type']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['code']] = '"' . $value['code'] . '"';
        }


        $result = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['code']] = $value['code'];
        }

        $data['mode'] = "update";
//        echo '<pre>';
//        print_r($data);
//        exit;
        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function delete($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        //$shipper = $this->gm->get_selected_record('shipper', 'name', $where = array('id' => $id));
        $set = array(
            'modified_by' => $sessiondata['id'],
            'modified_date' => date('Y-m-d H:i:s'),
            'status' => 2
        );
        $this->gm->update('shipper', $set, $id, array());
        $this->gm->update('rel_shipper', $set, '', array('shipper_id' => $id));

        $message = '<b>' . 'Record deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("shipper/entry_list");
    }

    function job_card() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['register']['clientcontacts_id']));
        $jobreglang = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $data['jobcard'] = array();
        $jobcard = $this->gm->get_data_list('jobcard', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($jobcard as $key => $value) {
            $data['jobcard'][$value['language_id']][$value['type']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value['code'];
            //$data['writercode'][$value['code']] = $value;
        }

        $result = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
            //$data['employeecode'][$value['code']] = $value;
        }
//        echo '<pre>';
//        print_r($data);
//        exit;

        $this->load->helper('pdf_helper');
        $this->load->helper('url');
        $pdfname = 'media/pdfs/job-register---' . $data['register']['id'] . '---' . rand(000, 999) . '.pdf';
        $path = save_pdf('job_card', $data, $pdfname);
        redirect(base_url() . $path);
    }

    function df($date) {
        if (!empty($date)) {
            $temp = DateTime::createFromFormat('d/m/Y', trim($date));
            return $temp->format("Y-m-d");
        } else {
            return NULL;
        }
    }

    function payment_update() {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $jobregister = $this->input->post('jobregister');
        $jobregister_id = $this->input->post('id');
        $jobregister['billsentdate'] = $this->df($jobregister['billsentdate']);
        $jobregister['invoicedate'] = $this->df($jobregister['invoicedate']);
        $this->gm->update('jobregister', $jobregister, $id = 0, $where = array('id' => $jobregister_id));

        redirect("shipper/entry_list");
    }

    function send_follow_up_letter() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['register'] = $this->gm->get_selected_record('jobregister', '*', $where = array('id' => $_GET['id']));
        $data['client'] = $this->gm->get_selected_record('client', '*', $where = array('id' => $data['register']['client_id']));
        $data['clientcontacts'] = $this->gm->get_selected_record('clientcontacts', '*', $where = array('id' => $data['register']['clientcontacts_id']));
        $jobreglang = $this->gm->get_data_list('jobreglang', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());

        foreach ($jobreglang as $key => $value) {
            $data['jobreglang'][] = $value['language_id'];
        }
        $result = $this->gm->get_data_list('language', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['language'][$value['id']] = $value['name'];
        }

        $data['jobcard'] = array();
        $jobcard = $this->gm->get_data_list('jobcard', array('jobregister_id' => $data['register']['id']), array(), array('id' => 'asc'), '*', 0, array());
        foreach ($jobcard as $key => $value) {
            $data['jobcard'][$value['language_id']][$value['type']] = $value;
        }

        $result = $this->gm->get_data_list('writer', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['writer'][$value['id']] = $value['code'];
            //$data['writercode'][$value['code']] = $value;
        }


        $result = $this->gm->get_data_list('employee', array(), array(), array('name' => 'asc'), '*', 0, array());
        foreach ($result as $key => $value) {
            $data['employee'][$value['id']] = $value;
            //$data['employeecode'][$value['code']] = $value;
        }
//        echo '<pre>';
//        print_r($data);
//        exit;
        if (isset($_GET['send']) && $_GET['send']) {
            $this->load->helper('email');
            $body = $this->load->view('send_follow_up_letter', $data, TRUE);
            $response = send_email($data['employee'][$data['register']['admin']]['email'], 'FOLLOW-UP AFTER JOB COMPLETION' . ' : ' . $data['client']['name'], $body, 'kesen@kesen.in', $reply_email = "", $cc_email = "", $attachment = array());
            if ($response) {
                $this->gm->update('jobregister', array('send_follow_up_letter_status' => 2), 0, array('id' => $data['register']['id']));
            }
            $message = 'Email send successfully on email id : ' . $data['employee'][$data['register']['admin']]['email'];
            $this->session->set_flashdata('success', $message);
            redirect('shipper/entry_list');
        } else {
            echo $this->load->view('send_follow_up_letter', $data, TRUE);
            if ($data['register']['send_follow_up_letter_status'] == 1) {
                echo '<a style="
    text-align: center;
    font-size: 24px;
    background: aliceblue;
    display: block;
    text-decoration: none;
    border: 1px solid;
" href="' . base_url('shipper/send_follow_up_letter?id=' . $_GET['id'] . '&send=1') . '">Send Email</a>';
            }
        }
    }

}
