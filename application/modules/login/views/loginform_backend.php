<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <form role="form" method="post"  name="myForm"  id="admin_login" action="">
        <div class="modal-header">
            <img src="<?php echo IMAGE_PATH_FRONTEND; ?>kesen-logo.png" class="img-responsive">
            <h2 class="modal-title" id="myModalLabel2">Login</h2>
            <span><i class="fa fa-user" aria-hidden="true"></i></span>
        </div>
        <div class="modal-body login-modal">
            <div id='social-icons-conatainer2'>
                <div class='modal-body-left'>
                    <div class="form-group inner-addon right-addon">
                        
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </span>
                            
                            <input type="text" id="ecode" placeholder="Employee Code" value="" maxlength="5" class="form-control login-field" name="login_email">
                        </div>

                    </div>
                    <div class="form-group inner-addon right-addon">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>

                            <input type="password" id="login-pass" placeholder="Password" value="" class="form-control login-field" name="login_password">
                        </div>
                        <a class="pull-right forgot-pswd hidden" href="<?php echo site_url('login/show_forgot_password/'); ?>">Forgot Password?</a>
                    </div>

                </div>

            </div>
            <div class="actions">
                <input type="submit" class="btn btn-success modal-login-btn " value="Login"/>
            </div>
        </div>


        <div class="modal-header">
            <!--  <a href="javascript:void(0);"  id="forgot">Lost your password?</a>-->
        </div>


        <div class="clearfix"></div>

    </form>
    <div class="login-logo ">
        <div class="login-header orggen">
            <a class="text-center"href="http://www.ogcrm.com/" title="" target="_blank">Powered By :<span> OrgGen Technologies</span></a>

        </div>
    </div>
</div>

<link href="<?php echo CSS_PATH_FRONTEND; ?>font-awesome.min.css" rel="stylesheet">

<?php /* Custom Theme Style */ ?>
<link href="<?php echo CSS_PATH_FRONTEND; ?>custom.min.css" rel="stylesheet">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#admin_login").validate({
            rules:
                    {
                        login_email: {
                            required: true,
                            minlength: 3,
                            maxlength: 3
                        },
                        login_password: {
                            required: true,
                            minlength: 2
                        }
                    },
            messages: {
                login_email: {
                    required: "Code Required",
                    email: "Enter Valid Email ID"
                },
                login_password: {
                    required: "Password Required",
                    minlength: "Minimum 7 Character Required"
                }
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {

                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('login/uvf_login'); ?>",
                    data: $(form).serialize(),
                    success: function (data) {
                        window.location = "<?php echo site_url('odv/odv_list'); ?>";
                    },
                    error: function (data) {
                        alert('The email and password you entered dont match!');
                    }
                });
                return false;
            }

        });
        var input = document.getElementById('ecode');

        input.onkeyup = function () {
            this.value = this.value.toUpperCase();
        }

    });

</script>

