<?php

class Login extends MX_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
    }

    function index() {


        $this->load->helper('url');
        $this->load->library('user_agent');

        if ($this->agent->browser() != 'Chrome') {
            echo 'Kindly Open this software in chrome browser only';
            exit;
        } else {
            if (isset($_SESSION['user']['id']) && $_SESSION['user']['id'] > 0) {
                redirect('odv/odv_list');
            } else {
                $this->show_form();
            }
        }
    }

    function uvf_login() {

        $this->load->model('Global_model', 'gm');
        $username = $this->input->post('login_email');
        $password = $this->input->post('login_password');
        if (isset($username) && !empty($username) && isset($password) && !empty($password)) {
            if ($this->_check_login($username, $password)) {
                http_response_code(200);
            } else {
                http_response_code(401);
            }
        }
    }

    function _check_login($username, $password) {
        if (!empty($username) && !empty($password)) {
            $username = addslashes(trim(strip_tags($username)));
            $password = trim(strip_tags($password));
            $saltpass = md5(trim(strip_tags($password)));
            $query = "select * from `employee` where `status` = 1 AND `password` ='" . $saltpass . "' AND (`code` = '" . $username . "' OR `email` = '" . $username . "')";
            //echo $query;
            //file_put_contents('test', $query );
            $query_exec = $this->db->query($query);
            $row = $query_exec->row_array();

            if (isset($row) && is_array($row) && count($row) > 0) {
                $session_data = array(
                    'user' => array(
                        'id' => $row['id'],
                        'name' => $row['name'],
                        'email' => $row['email'],
                        'firstname' => $row['firstname'],
                        'lastname' => $row['lastname'],
                        'code' => $row['code'],
                        'department' => $row['department'],
                        'logged_in' => TRUE,
                        'role' => $row['role']
                    )
                );
                $this->session->set_userdata($session_data);

                return TRUE;
            } else {
                return FALSE;
            }
        } else {

            return FALSE;
        }
    }

    function show_form() {
        $this->load->helper('url');
        if ($this->session->userdata('RememberURL')) {
            redirect($this->session->userdata('RememberURL'));
        } else {
            $data = array();
            $this->_display('loginform_backend', $data);
        }
    }

    /* function uvf_login() {
      $this->load->model('Global_model', 'gm');
      $username = $this->input->post('username');
      $password = $this->input->post('password');


      if (isset($username) && !empty($username) && isset($password) && !empty($password)) {

      if ($this->_check_login($username, $password)) {
      http_response_code(200);
      } else {
      // 401-unauhorized,403 -access forbidden
      http_response_code(401);
      }
      } else {
      redirect(site_url());
      }
      }
     */

    function _display($view, $data) {
        $this->load->view('backend_header_login');
        $this->load->view($view, $data);
        $this->load->view('backend_footer_login');
    }

    function logout() {
        $this->load->helper('url');
        $this->session->unset_userdata('user');

        redirect(base_url());
    }

    function forgot_password() {

        /*
         * load modal
         */
        $this->load->model('Global_model', 'gm');

        /*
         * get post data i.e. username and password
         */
        $post_email = $this->input->post('email');


        /*
         * remove space from post data
         */
        $email = addslashes(trim(strip_tags($post_email)));


        /*
         * get email id and password to be send for password recovery
         */
        $query = "select id,email,password from `user` where `email` ='" . $email . "'";
        $query_exec = $this->db->query($query);
        $row = $query_exec->row_array();

        if (isset($row['email']) && !empty($row['email'])) {

            $this->load->library('email');

            $config['protocol'] = "smtp";
            $config['mailtype'] = "html";
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = "utf-8";
            $config['smtp_host'] = "www.gajanan-jewellers.com";
            $config['smtp_user'] = "noreply@gajanan-jewellers.com";
            $config['smtp_pass'] = "noreply123";
            $config['smtp_port'] = "587";

            $this->email->initialize($config);
            $this->email->from('noreply@itut.com', 'ITUT');
            $this->email->to($row['email'], 'ITUT');
            $this->email->subject('ITUT Login Details : ' . date('d M Y,h:i:s A'));
            $this->email->message('<b>Email ID:</b>  ' . $row['email'] . '<br>  <b>Password :</b> ' . $row['password']);
            $this->email->send();
            print_r($this->email->print_debugger());
        } else {
            echo 'Email not Exist!';
        }
    }

    function _is_logged_in() {

        $user = $this->session->userdata('user');
        if ($user['id'] > 0) {
            return TRUE;
        } else {
            $last_url = $this->session->userdata('LastUrl');
            $this->session->set_userdata('RememberURL', $last_url);
            return FALSE;
        }
    }

}
