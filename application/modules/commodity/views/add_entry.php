<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Client</h2>
                </div>
                <div class="x_content">
                    <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('commodity/update') : site_url('commodity/insert') ?>"> 
                        <br>
                        <div class="col-md-6 col-sm-6">

                            <input type="hidden" name="id" value="<?php echo isset($client['id']) ? $client['id'] : ''; ?>">
                            <div class="row">
                                <div class="text-center">
                                    <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="name">Client's Name<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="name" value="<?php echo isset($client['name']) ? $client['name'] : ''; ?>" name="client[name]">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="email">E-mail</label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="email" class="form-control"  tabindex="2" id="email" name="client[email]"  value="<?php echo isset($client['email']) ? $client['email'] : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="contact">Contact No.</label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "number" class = "form-control" tabindex="3" id="contact"  name="client[contactno]"  value="<?php echo isset($client['contactno']) ? $client['contactno'] : ''; ?>">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="Landline">Landline</label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "number" class = "form-control" tabindex="4" id="Landline"  name="client[landine]"  value="<?php echo isset($client['landine']) ? $client['landine'] : ''; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="address" >Address</label>
                                    <div class="col-md-4 col-sm-4">
                                        <textarea id="address" class="form-control" tabindex="5" name="client[address]"> <?php echo isset($client['address']) ? $client['address'] : ''; ?></textarea>
                                    </div>

                                    <label class="control-label col-md-2 col-sm-2" for="estimate">Metrix 
                                    </label>
                                    <div class="col-md-4 col-sm-4">
                                        <select name="client[estimatecompany]" class="searchselected" tabindex="4" id="language">
                                            <?php
                                            $estimates = $this->config->item('estimate');
                                            foreach ($estimates as $key => $value) {
                                                ?>
                                                <option <?php
                                                if (isset($client['estimatecompany']) && $key == $client['estimatecompany']) {
                                                    echo 'selected';
                                                }
                                                ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                                ?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2 " >Type<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input <?php echo isset($client['type']) && $client['type'] == 1 ? 'checked' : ''; ?> type="radio" name="client[type]" value="1"> Protocol<br>
                                        <input <?php echo isset($client['type']) && $client['type'] == 2 ? 'checked' : ''; ?> type="radio" name="client[type]" value="2"> Non Protocol (advertising)
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2 hidden" for="city">City<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4 hidden">
                                        <select class="chosen-select" tabindex="8" id="city">
                                            <option>Select City</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>

                                </div>
                            </div>




                        </div>
                        <div class="col-md-6 col-sm-6">
                            <table class="table table-bordered" id="table-data">
                                <tr>
                                    <th>Client Contact Person Name<span class="required">*</span></th>
                                    <th>Contact No.</th>
                                    <th>Landline No.</th>
                                    <th>Email</th>
                                    <th>Designation</th>
                                    <th width="18%">Action</th>
                                </tr>
                                <?php
                                if (isset($clientcontacts) && count($clientcontacts) && is_array($clientcontacts)) {
                                    foreach ($clientcontacts as $key => $value) {
                                        ?>
                                        <tr class="tr_clone">
                                        <input type="hidden" class="form-control" name="clientcontactsids[]" value="<?php echo $value['id']; ?>" tabindex="9">
                                        <td><input type="text" class="form-control" name="clientcontacts[name][]" value="<?php echo $value['name']; ?>" tabindex="9"></td>
                                        <td><input type="number" class="form-control"  name="clientcontacts[contactno][]"  value="<?php echo $value['contactno']; ?>" tabindex="10"></td>
                                        <td><input type="number" class="form-control"  name="clientcontacts[landline][]"  value="<?php echo $value['landline']; ?>" tabindex="11"></td>
                                        <td><input type="text" class="form-control"  name="clientcontacts[email][]"  value="<?php echo $value['email']; ?>" tabindex="11"></td>
                                        <td><input type="text" class="form-control"  name="clientcontacts[designation][]" value="<?php echo $value['designation']; ?>"   tabindex="12"></td>
                                        <td>
                                            <a title="Update Job Card" href="javascript:void(0);" id="add" class="tr_clone_add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>&nbsp;Add</a>
                                            <!--<a title="Delete Job Card" href="javascript:void(0);" onclick="$(this).closest('tr').remove();"><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete</a></td>-->
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>

                                    <tr class="tr_clone">
                                        <td><input type="text" class="form-control" name="clientcontacts[name][]" value="" tabindex="9"></td>
                                        <td><input type="number" class="form-control"  name="clientcontacts[contactno][]" tabindex="10"></td>
                                        <td><input type="number" class="form-control"  name="clientcontacts[landline][]" tabindex="11"></td>
                                        <td><input type="text" class="form-control" name="clientcontacts[email][]" value="" tabindex="9"></td>
                                        <td><input type="text" class="form-control"  name="clientcontacts[designation][]"  tabindex="12"></td>
                                        <td>
                                            <a title="Update Job Card" href="javascript:void(0);" id="add" class="tr_clone_add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>&nbsp;Add</a>
                                            <a title="Delete Job Card" href="javascript:void(0);" onclick="$(this).closest('tr').remove();"><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>


                            </table>
                        </div>
                        <div class = "col-md-12 col-sm-12">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn" tabindex="9">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>

<script>
                                            var table = $('#table-data')[0];

                                            $(table).delegate('.tr_clone_add', 'click', function () {
                                                var thisRow = $(this).closest('tr')[0];
                                                $(thisRow).clone().insertAfter(thisRow).find('input').val('');
                                            });

</script>

<script>
    jQuery(document).ready(function () {



        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("digitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("lettersdigitonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z0-9,""\s]+$/i.test(value);
        });
        jQuery.validator.addMethod("codeonly", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        });
        jQuery("#employee_add").validate({
            ignore: [],
            rules: {
                'client[name]': {
                    required: true,
                    lettersdigitonly: true,
                    rangelength: [3, 50],
                },
                'client[email]': {
                    email: true,
                },
                'client[contactno]': {
                    digitonly: true,
                    rangelength: [10, 10],
                },
                'client[landine]': {
                    lettersdigitonly: true,
                    rangelength: [6, 12],
                },
                'client[admin]': {
                    required: true,
                },
                'client[accountant]': {
                    required: true,
                },
                'client[address]': {
                },
                'clientcontacts[name][]': {
                    required: true,
                },
                'clientcontacts[email][]': {
                    email: true,
                },
                'clientcontacts[contactno][]': {
                    rangelength: [10, 10],
                    digits: true,
                },
                'clientcontacts[landline][]': {
                    rangelength: [6, 12],
                    digits: true,
                },
                'clientcontacts[designation][]': {
                    lettersdigitonly: true,
                },
                'writer[advertisingcharges]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'writer[lumsumcharges]': {
                    required: true,
                    rangelength: [1, 3],
                    digits: true,
                },
                'client[address]':{
                    rangelength: [0, 500],
                },
                        'client[type]': {
                            required: true,
                        },
            },
            messages: {
                'client[email]': {
                    required: "Name Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'client[landine]': {
                    required: "Contact No Required",
                    lettersonly: "Please enter correct Name without space",
                    codeonly: "Space not allowed"
                },
                'writer[email]': {
                    required: "Email Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'contact_no': {
                    required: "Contact No Required",
                    lettersonly: "Please enter correct Name without space"
                },
                'txtmessage': {
                    required: "Message Required",
                    lettersonly: "Please enter correct Name without space"
                },
            },
            errorElement: 'p',
            errorPlacement: function (error, element) {
                jQuery(element).parent().append(error);
            }
        });
    });
</script>

