<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title hidden">
                    <h3>Search Client</h3>
                </div>
                <div class="x_content search_content ">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('commodity/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Client's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="contact">Contact No.<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12 hidden" for="email">Email<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2 hidden">
                                <select class="chosen-select" tabindex="1" autofocus="on" id="name">
                                    <option value="NULL">Select Employee's Name</option>
                                    <option value="NULL">abc</option>
                                    <option value="NULL">xyz</option>
                                    <option value="NULL">pqr</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" name="name" value="<?php echo $_GET['name']; ?>" class="form-control" tabindex="2" id="contact">
                            </div>
                            <div class="col-md-2 col-sm-2 hidden">
                                <input type="email" class="form-control" tabindex="3" id="email">
                            </div>
                            <!--<input id="btn-search" tabindex="4" type="submit" class="btn btn-default submit-btn" value="Search">
                            <input tabindex="4" type="reset" value="Clear" class="btn btn-default clear-btn">-->
                            
                            <input type="submit"  value="search" class="btn btn-default submit-btn">
                            <a tabindex="5" class="btn btn-default view-btn" title="View All Clients" href="<?php echo base_url('commodity/entry_list'); ?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="6" class="btn btn-default add-btn"  title="Add Client" href="<?php echo site_url('commodity'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Client</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Client List</h3>
                <div class="total">
                    <span>Total Display : <?php echo count($list); ?></span>
                    <span>Total Clients : <?php echo $total; ?></span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered view-table">
                    <thead>
                        <tr class="table_heading">
                            <th>ID</th>
                            <th>Client's Name</th>
                            <th>Client's Type</th>
                            <th>Email</th>
                            <th>Contact No.</th>
                            <th>Landline</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($list as $key => $value) {
                            ?>

                            <tr>
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo ($value['type'] == 1) ? 'Protocol' : 'Non Protocol (advertising)'; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['contactno']; ?></td>
                                <td><?php echo $value['landine']; ?></td>

                                <td><?php echo $value['address']; ?></td>
                                <td>
                                    <a class="" title="View Client"href="<?php echo base_url() . 'commodity/preview?id=' . $value['id']; ?>"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i>&nbsp;View&nbsp;</a>
                                    <a title="Update Client" href="<?php echo base_url() . 'commodity/edit?id=' . $value['id']; ?>" class=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>


                                </td>
                            </tr>  
                            <?php
                        }
                        ?>
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

