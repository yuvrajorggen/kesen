<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">

            <div class="table_content">
                <h3>Client Detail Information</h3>

                <?php echo $this->session->flashdata('message'); ?>

                <p>name :<?php echo $client['name']; ?></p>
                <p>email :<?php echo $client['email']; ?></p>
                <p>contactno :<?php echo $client['contactno']; ?></p>
                <p>landine :<?php echo $client['landine']; ?></p>
                <p>address :<?php echo $client['address']; ?></p>
                <p>Metrix :<?php  $estimates = $this->config->item('estimate');
                
                echo $estimates[$client['estimatecompany']]; ?></p>
                <p>Type :<?php
                    if ($client['type'] == 1) {
                        echo 'Protocol';
                    } else {
                        echo 'Non Protocol (advertising)';
                    }
                    ?></p>

                <table class="table table-bordered view-table">
                    <thead>
                        <tr class="table_heading">
                            <th>Client Contact Person Name</th>
                            <th>Contact No.</th>

                            <th>Landline</th>
                            <th>Email</th>
                            <th>Designation</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($clientcontacts as $key => $value) {
                            ?>
                            <tr>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $value['contactno']; ?></td>
                                <td><?php echo $value['landline']; ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['designation']; ?></td>

                            </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table> 

            </div>
        </div>
    </div>
</div>

