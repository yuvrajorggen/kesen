<?php

class Db_backup extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $this->load->dbutil();

        $this->load->model('Global_model', 'gm');
        
        $prefs = array(
          'format' => 'zip',
          'filename' => 'my_db_backup.sql'
        );

        $backup = $this->dbutil->backup($prefs);

        $db_name = 'backup-on-' . date("Y-m-d-H-i-sa") . '.zip';
        $dir_name = 'db_backup/';
        if (!file_exists($dir_name)) {
            mkdir($dir_name, 0777);
        }
        $save = $dir_name . $db_name;

        $this->load->helper('file');
        write_file($save, $backup);
        $end_time = date('Y-m-d H:i:s');
       
    }

}

?>