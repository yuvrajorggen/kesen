<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function send_sms($contactno, $message) {

    $CI = & get_instance();
    $username = "klanguagebureau@gmail.com";
    $hash = "56eb600e05c0f9c5cec8c67eced8328969daa98f90873d788da705633224905c";

// Config variables. Consult http://api.textlocal.in/docs for more info.
    $test = "0";

// Data for text message. This is the text message data.
    $sender = "KESENN"; // This is who the message appears to be from.
    $numbers = "91" . $contactno; // A single number or a comma-seperated list of numbers
    //enter Your Message 
// 612 chars or less
// A single number or a comma-seperated list of numbers
    
//    exit;

    $message = urlencode($message);
    $data = "username=" . $username . "&hash=" . $hash . "&message=" . $message . "&sender=" . $sender . "&numbers=" . $numbers . "&test=" . $test;
//echo $data;exit;

    $ch = curl_init('http://api.textlocal.in/send/?');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $sms_log = curl_exec($ch); // This is the result from the API
    $response = json_decode($sms_log);
	//print_r($response);exit;
    if ($response == NULL || $response->status == 'failure') {
        $array['content'] = urldecode($message);
        $array['status'] = $response->status;
        $array['created_date'] = date('Y-m-d H:i:s');
    } else {
        $array['balance'] = $response->balance;
        $array['batch_id'] = $response->batch_id;
        $array['cost'] = $response->cost;
        $array['num_messages'] = $response->num_messages;
        $array['num_parts'] = $response->message->num_parts;
        $array['sender'] = $response->message->sender;
        $array['content'] = $response->message->content;
        $array['receipt_url'] = $response->receipt_url;
        $array['custom'] = $response->custom;
        $array['messages_id'] = $response->messages['0']->id;
        $array['recipient'] = $response->messages['0']->recipient;
        $array['status'] = $response->status;
        $array['created_date'] = date('Y-m-d H:i:s');
    }

    $CI->db->insert('sms_log', $array);
    return $CI->db->insert_id();
}

?>