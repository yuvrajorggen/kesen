<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;

function save_pdf($view, $data, $pdfname, $paper = 'portrait') {


    $CI = &get_instance();

    /*
     * include doompdf files
     */

    $path = FCPATH . 'dompdf-master/autoload.inc.php';
    require_once($path);

//
//    echo '<pre>';
//    print_r($data);
//    exit;
    $pdfdata = $CI->load->view($view, $data, true);
//    echo '<pre>';
//    print_r($pdfdata);
//    exit;
    /*
     * show html
     */

    $dompdf = new Dompdf();
    $contxt = stream_context_create([
        'ssl' => [
            'verify_peer' => FALSE,
            'verify_peer_name' => FALSE,
            'allow_self_signed' => TRUE
        ]
    ]);
    $options = new Options();

    $dompdf->setHttpContext($contxt);
    $dompdf->set_option('enable_css_float', TRUE);
    $dompdf->set_option('isRemoteEnabled', TRUE);
    $dompdf->set_option('debugKeepTemp', TRUE);
    $dompdf->set_option('isHtml5ParserEnabled', TRUE);

    $options->set("isPhpEnabled", true);

    $dompdf->loadHtml($pdfdata);
// (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', $paper);

// Render the HTML as PDF
    $dompdf->render();
    $output = $dompdf->output();



    $path = FCPATH . $pdfname;


    file_put_contents($path, $output);
    return $pdfname;
}

?>