<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function send_email($recipient_email, $subject, $body, $from_email, $reply_email = "", $cc_email = "", $attachment = array()) {

    $CI = &get_instance();

    $from_email = 'kesen@kesen.in';
    require_once(FCPATH . "sendgrid-php/sendgrid-php.php");
    // var_dump($subject);exit;
    $email = new \SendGrid\Mail\Mail();
    $email->setFrom("kesen@kesen.in", "kesen.in");
    $email->setSubject($subject);
    $email->addTo($recipient_email, $recipient_email);
    $email->addContent("text/html", $body);
    $sendgrid = new \SendGrid('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');

    $status = 1;
    try {
        $response = $sendgrid->send($email);
        
        $statuscode =  $response->statusCode() . "\n";
        //echo '<pre>';
        //print_r($body);
        //echo $statuscode;exit;
        if($statuscode == 202){
            $status = 2;
        }
        //print_r($response->headers());
        //print $response->body() . "\n";
    } catch (Exception $e) {
        echo 'Caught exception: '. $e->getMessage() ."\n";
        exit;
    }
    
    $data = array(
        'from_address' => isset($from_email) ? $from_email : SMTP_FROM_EMAIL,
        'to_address' => $recipient_email,
        'cc_address' => $cc_email,
        'subject' => $subject,
        'message' => $body,
        'mail_date' => date("Y-m-d H:i:s"),
        'attachment' => '',
        'status' => isset($status) && $status == TRUE ? 2 : 1
    );
    $CI->db->insert('email_log', $data);
    $CI->email->clear(TRUE);
    return $returnvalue;

}
