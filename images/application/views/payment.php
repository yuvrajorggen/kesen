<html lang="en"><head>

        <title>Invoice</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <style>   

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:6px;}

            .table-bordered{border:1px solid #000;}

            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border:1px solid #000;}

            .list-inline>li{    display: inline-block;

                                padding-right: 35px;

                                padding-left: 35px;

                                padding-top: 10px;

                                font-weight: 700;}

            body{font-size:13px;    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}

            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:5px 0 5px 10px;}

            .goods-table td{border:1px solid #000;}

            thead:before, thead:after { display: none; }

            tbody:before, tbody:after { display: none; }

            tbody:before, tbody:after { display: none; }

            .invoice tr td{}

            .product_invoice td{padding:4px 5px !important;}

            li{padding: 5px 0 5px 0;}


            .rupee{margin: 5px 0 0 0;}
        </style>

    </head>

    <body>

        <table width="100%" class="table goods-table" style="margin-bottom: 0;   font-size: 13px; border-collapse: collapse;">

            <tbody>

                <tr>
                    <td style="padding: 10px 0;margin: 0;font-size: 40px;font-weight:bolder;text-align: center;border-top: 1px solid #000;border-bottom: none;/* background: rgba(0, 128, 0, 0.1); */color: green;text-decoration: underline;" class="text-center" colspan="5">KeSen Language Bureau</td>
                </tr>
                <tr>
                    <td style="padding: 5px 0 0 0;margin: 0;font-size: 13px;text-align: center;border-top: none;border-bottom: none;" class="text-center" colspan="5">101-102 Darshan Apartments, 21 Raghunath Dadaji Street, Fort, Mumbai 400 001</td>
                </tr>

                <tr>
                    <td style="padding: 5px 0 0 0;    border-bottom: none;border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="5">
                        Phone : <strong>002 4034 8888,4034 8844 to 8865</strong>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 0 5px 0;    border-bottom: none;border-top: none;margin: 0;font-size: 13px;text-align: center;" class="text-center" colspan="5">
                        Fax : <strong>22674618</strong>
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 0 5px 0;margin: 0;font-size: 18px;font-weight: bold;background-color: #eee;text-transform: uppercase; text-align: center;border: 1px solid #000;" class="text-center" colspan="7">Payment Card</td>
                </tr>
                <tr style="border:1px solid #000;">

                    <td colspan="3" rowspan="5">

                        <!--StartFragment-->

                        <p style="padding-top:10px;margin: 0;font-size: 18px;font-weight:700;"><span style="
                                                                                                     color: #2f4f4f;
                                                                                                     ">Udayan Bhattacharya</span></p>

                        <p style="padding-top:10px;margin: 0;font-size: 13px;">An Electronic payment has been done into your account being translation charges for the jobs listed below :</p>





                    </td>




                </tr>


                <tr style="border:1px solid #000;">


                    <td style="vertical-align:middle;" colspan="2">
                        Bill Date <span style="font-weight: 700;margin-left: 18px;">: 30/04/2017</span>
                    </td>

                </tr>

                <tr style="border:1px solid #000;">

                    <td style="line-height: 1;vertical-align:middle;" colspan="2">
                        Payment <span style="font-weight: 700;margin-left: 15px;">: 26/07/2017</span>
                    </td>


                </tr>

                <tr style="border:1px solid #000;">

                    <td style="line-height: 1;vertical-align:middle;" colspan="2">
                        Chq No. <span style="font-weight: 700;margin-left: 18px;">: NEFT</span>
                    </td>


                </tr>

                <tr style="border:1px solid #000;">

                    <td style="line-height: 1;vertical-align:middle;" colspan="2">
                        Amount <span style="font-weight: 700;margin-left: 21px;">: INR 8880.00</span>
                    </td>


                </tr>

                <tr style="border-bottom: none;background: #eee;">

                    <td colspan="1" width="20%" style="font-weight: bold;font-size: 13px;">Month</td>



                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;">Register No.</td>

                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Units</td>

                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Rate</td>

                    <td colspan="1" width="20%" style="font-weight: 700;font-size: 13px;border-bottom: none;">Amount<br><span style="float: left">Rs.</span></td>

                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="vertical-align: top;">

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">Mar-17</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">22123</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">23</td>

                    <td style="font-size: 13px;background-color: #fff;border-bottom: none;">60</td>
                    <td colspan="1" style="font-size: 13px;background-color: #fff;border-bottom: none;">805</td>
                </tr>
                <tr style="border: none;">
                    <td colspan="3" style="font-weight: 700;font-size: 13px;background-color: #fff;"></td>
                    <td colspan="1" style="font-weight: 700;font-size: 13px;background-color: #fff;">Total</td>
                    <td colspan="1" style="background-color: #fff;"><span style="display:inline-block;">8880.00</span></td>

                </tr>

                <tr>
                    <td colspan="5" style="border-right-style: solid;font-style: italic;font-weight: bold;font-size: 15px;color: #2c6d2c;text-align: right;">Rupees : <span style="display:inline-block;font-size:14px;">Eight thousand eight hundred and eight only.</span></td>

                </tr>


            </tbody>
        </table>



    </body></html>