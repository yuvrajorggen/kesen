<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo IMAGE_PATH_BACKEND; ?>blank-profile-picture-973460_640.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?php echo ucwords($this->session->userdata['admin_user']['name']); ?></p>

        </div>
    </div>

    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->

        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Journal Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('journal/journal_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('journal/journal_admin/add'); ?>"><i class="fa fa-circle-o"></i>Add</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('journal/journal_admin/publish_list'); ?>"><i class="fa fa-circle-o"></i>Published</a></li>
                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Article Management</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('article/article_admin'); ?>"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('article/article_admin/add'); ?>"><i class="fa fa-circle-o"></i>Add</a></li>
                </ul>
            </li>



            <li class="treeview">
                <a href="<?php echo site_url(); ?>login/admin_login/logout">Log out</a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper" style="min-height: 946px;">