<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
        <meta http-equiv="Cache-Control" content="no-store" />
        <meta name="robots" content="NoArchive">
        <meta name="robots" content="NoSnippet">
        <meta name="robots" content="NoODP">
        <meta name="robots" content="NoImageIndex">
        <meta name="robots" content="NoTranslate">
        <title>Kesen</title>
        
        <?php /* Bootstrap */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>bootstrap.min.css" rel="stylesheet">
        <?php /* Font Awesome */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>font-awesome.min.css" rel="stylesheet">
        <?php /* custom */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>custom.min.css" rel="stylesheet">
        <?php /* Select2 */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>select2.min.css" rel="stylesheet">
        <?php /* bootstrap-daterangepicker */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>daterangepicker.css" rel="stylesheet">

        <?php /* jQuery */ ?>
        <script  src="<?php echo JS_PATH_FRONTEND; ?>jquery.min.js"></script>
        <?php /* Bootstrap */ ?>
        <script src="<?php echo JS_PATH_FRONTEND; ?>bootstrap.min.js"></script>
        <script src="<?php echo JS_PATH_FRONTEND; ?>functions.js"></script>
        <?php /* Timepicker */ ?>
        <link href="<?php echo CSS_PATH_FRONTEND; ?>bootstrap-timepicker.min.css" rel="stylesheet">
        
        <?php /* Chosen(select plugin) */ ?>

        <link rel="stylesheet" href="<?php echo CSS_PATH_FRONTEND; ?>prism.css">
        <link rel="stylesheet" href="<?php echo CSS_PATH_FRONTEND; ?>chosen.css">
        <script src="<?php echo JS_PATH_FRONTEND; ?>bootstrap-timepicker.min.js"></script>
        <script src="<?php echo JS_PATH_FRONTEND; ?>canvasjs.min.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="top-header">
                    <div class="navbar-brand col-sm-2 sol-md-2">
                        <a href="<?php echo site_url(); ?>"><img src="<?php echo IMAGE_PATH_FRONTEND; ?>kesen-logo.png" class="logo img-responsive"></a>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <span id="crm_ctrl_panel_invoice_edit_search" class="crm-search-block col-md-3 pull-left" data-toggle="tooltip" data-placement="bottom" >
                            <form class="crm-search" id="crm-search" action="<?php echo site_url('search/show_search_results'); ?>" method="post">
                                <span class="crm-search-btn fa fa-search" id="search_get_input"></span> <span class="crm-search-inp-wrap">
                                    <input id="crm_ctrl_panel_invoice_edit_search_input" class="crm-search-inp" name="search" type="text" onkeyup="crm_ajax_live(this.value);" autocomplete="off"placeholder="Search Job Card No: 1234" value=""/>
                                </span>
                                <input type="hidden" name="where" value="crm">
                            </form>
                        </span> 

                        <nav class="nav navbar-nav tab">
                            <ul>
                                <li class="active">
                                    <a href="">Job Register</a>
                                    <ul class=" employee_submenu">
                                        <div class="row submenu">                                        
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('odv/add'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Create Job Register</a></li>
                                                <li><a href="<?php echo site_url('odv/odv_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Register List</a></li>
                                            </div>

                                        </div> 

                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Job Card</a>
                                    <ul class=" employee_submenu">
                                        <div class="row submenu">
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('shipper/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Create Job Card</a></li>
                                                <li><a href="<?php echo site_url('shipper/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Job Card List</a></li>
                                                <!--<li><a href="<?php echo site_url('shipper/payment'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Payment</a></li>-->
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Employee</a>
                                    <ul class="employee_submenu">
                                        <div class="row submenu">
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('employee/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Employee</a></li>
                                                <li><a href="<?php echo site_url('employee/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Employees</a></li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Writer</a>
                                    <ul class="employee_submenu">
                                        <div class="row submenu">
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('customer/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Writer</a></li>
                                                <li><a href="<?php echo site_url('customer/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Writers</a></li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Client</a>
                                    <ul class=" employee_submenu">
                                        <div class="row submenu">                                        
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('commodity/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Client</a></li>
                                                <li><a href="<?php echo site_url('commodity/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> View All Clients</a></li>
                                            </div>

                                        </div> 

                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Writer's Payment</a>
                                    <ul class=" employee_submenu">
                                        <div class="row submenu">                                        
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href="<?php echo site_url('consignee/entry'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Add Payment</a></li>
                                                <li><a href="<?php echo site_url('consignee/entry_list'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Payment List</a></li>
                                            </div>

                                        </div> 

                                    </ul>
                                </li>
                                <li class="menu">
                                    <a href="">Reports</a>
                                    <ul class="employee_submenu">
                                        <div class="row submenu">
                                            <div class="col-sm-12 col-md-12">
                                                <li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i> Job Card</a> </li>


                                                <li><a href="<?php echo site_url('consignor/entry_list'); ?>">&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i> Job Billed</a></li>
                                                <li><a href="<?php echo site_url('consignor/entry_list'); ?>">&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i> Job Unbilled</a></li>




                                                <li><a href="<?php echo site_url('consignor/timeline_report'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> Timeline</a></li>
                                            </div>
                                        </div>
                                    </ul>
                                </li>
                                <!--<li class="menu">
                                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;User</a>
                                </li>-->
                                <li class="menu" style="">
                                    <a href="#"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;Logout</a>
                                </li>  
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>