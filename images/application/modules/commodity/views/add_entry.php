<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Client</h2>
                </div>
                <div class="x_content">
                    <br>
                    <div class="col-md-6 col-sm-6">
                        <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                            <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                            <div class="row">
                                <div class="text-center">
                                    <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="name">Client's Name<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="name">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="email">E-mail<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type="email" class="form-control"  tabindex="2" id="email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="contact">Contact No.<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="3" id="contact">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="Landline">Landline<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="4" id="Landline">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="city">Admin<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="3" id="city">
                                            <option>Select Admin</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                    <label class = "control-label col-md-2 col-sm-2" for="country">Accountant<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="4" id="country">
                                            <option>Select Accountant</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="address">Address<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <textarea id="address" class="form-control" tabindex="5"></textarea>
                                    </div>
                                    <label class = "control-label col-md-2 col-sm-2" for="country">Country<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="6" id="country">
                                            <option>Select Country</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="country">State<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="7" id="country">
                                            <option>Select State</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="city">City<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="8" id="city">
                                            <option>Select City</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>

                                </div>
                            </div>



                        </form>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <table class="table table-bordered">
                            <tr>
                                <th>Client Contact Person Name</th>
                                <th>Contact No.</th>
                                <th>Landline No.</th>
                                <th>Designation</th>
                                <th width="18%">Action</th>
                            </tr>
                            <tr>


                                <td><input type="text" class="form-control" tabindex="9"></td>
                                <td><input type="text" class="form-control" tabindex="10"></td>
                                <td><input type="text" class="form-control" tabindex="11"></td>
                                <td><input type="text" class="form-control" tabindex="12"></td>
                                <td>
                                    <a title="Update Job Card" href="javascript:void(0)" id="add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i>&nbsp;Add</a>
                                    <a title="Delete Job Card" href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete</a></td>
                            </tr>

                        </table>
                    </div>
                    <div class = "col-md-12 col-sm-12">
                        <div class = "submit">
                            <button type = "submit" class = "btn btn-default submit-btn" tabindex="9">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>

<script>
            var i = 1;
            $("#add").click(function() ​​​{
    $("table tr:first").clone().find("input").each(function() {
    $(this).val('').attr('id', function(_, id) { return id + i });
    }).end().appendTo("table");
            i++;
    })​; ​

</script>