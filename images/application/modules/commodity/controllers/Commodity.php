<?php

class Commodity extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');

        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if ($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1) {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function insert() {
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');
        //echo '<pre>';
        //print_r($_POST);
        //exit;
        $this->load->model('Global_model', 'gm');
        $data = $this->input->post('commodity');
        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');
        $data['name']=  strtoupper($data['name']);
        $data['code']=  strtoupper($data['code']);
       
        $this->gm->insert('commodity', $data);
        redirect(site_url('commodity/entry_list'));
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->model('Global_model', 'gm');
        $this->load->library('user_agent');
        $data = array();
        //$data['commoditylist'] = $this->gm->get_data_list('commodity', array(), array(), array('id' => 'desc'), '*', 30);
        //pagination (start)

        $page = $this->uri->segment(3);

        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * 5;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        if ($_GET) {// created for searching from the list of data 
            $sql = "SELECT * FROM `commodity` where";
            $name = $this->input->get('name');
            $code = $this->input->get('code');
            if ($code == '' && $name != '') {
                $sql = $sql . " name like '%" . $name . "%'";
            } elseif ($name == '' && $code != '') {
                $sql = $sql . " code like '%" . $code . "%'";
            } else {
                $sql = $sql . " name like '%" . $name . "%' AND code like '" . $code . "%'";
            }
            $query_exec = $this->db->query($sql);
            $result = $query_exec->result_array();

            $cquery = "SELECT count(id) as id FROM `commodity` WHERE status='1' AND  id=" . $result[0][id];
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        } else {
            $query = "SELECT * FROM `commodity` WHERE status='1' ORDER BY `id`  DESC limit 5 offset " . $offset . "";
            $query_exec = $this->db->query($query);
            $result = $query_exec->result_array();

            $cquery = "SELECT count(id) as id FROM `commodity` WHERE status='1' ";
            $cquery_exec = $this->db->query($cquery);
            $count = $cquery_exec->row_array();
        }
        $data['commoditylist'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;

        $this->load->library('pagination');

        $config['base_url'] = site_url('commodity/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = 5;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);

        $this->pagination->initialize($config);
        $view = 'entry_list';
        //pagination end
        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');
        $id = $this->input->post('commodityid');
        $data = $this->input->post('commodity');
        $data['modified_by']=$sessiondata['id'];
        $data['modified_date']=  date('Y-m-d H:i:s');
        
        $data['name']=  strtoupper($data['name']);
        $data['code']=  strtoupper($data['code']);

        $this->gm->update('commodity', $data, $id);


        $message = '<b>' . $data['name'] . ' ' . 'updated successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("commodity/entry_list");
    }

    function edit($id) {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['commodity'] = $this->gm->get_selected_record('commodity', '*', $where = array('id' => $id));
        $data['mode'] = "update";

        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function delete($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');
        $agent = $this->gm->get_selected_record('commodity', 'name', $where = array('id' => $id));
        $set=array(
            'modified_by' => $sessiondata['id'],
            'modified_date'=> date('Y-m-d H:i:s'),
            'status' => 2
        );
        $data['agent'] = $this->gm->update('commodity', $set, $id);

        $message = '<b>' . $agent['name'] . ' ' . 'deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("commodity/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function upload_commodity() {
        $this->load->helper('url');
        $data = array();
        $this->_display('commodity_upload', $data);
    }

    function commodity_upload() {
        $this->load->model('Global_model', 'gm');
        $file_name = 'csv_import_' . date('Y-m-d_H-i-s', time());
        $config['upload_path'] = './media/csv/bulk_import/';
        $config['allowed_types'] = 'application/octet-stream|CSV|csv';
        $config['file_name'] = $file_name;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data = array('upload_data' => $this->upload->data());

            $row = 1;
            if (($handle = fopen($data['upload_data']['full_path'], "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $num = count($data);
                    //echo '<pre>';
                    //print_r($data);
                    unset($insert);
                    if ($row != 1) {
                        $insert = array(
                            "name" => $data[0],
                            "code" => $data[1]
                        );

                        $this->gm->insert('commodity', $insert);
                    }
                    $row++;
                }
                fclose($handle);
            }
        }

        $this->load->helper('url');
        redirect(site_url('commodity/entry_list'));
    }

}
