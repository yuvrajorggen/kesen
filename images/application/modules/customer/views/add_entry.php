<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Writer</h2>
                </div>
                <div class="x_content">
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                            <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                            <div class="row">
                                <div class="text-center">
                                    <h5 class="pull-left">Information</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="name">Writer's Name<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="name">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="code">Code<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="2" id="code">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="email">E-mail<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="email" class="form-control" id="email" tabindex="4">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="contact">Contact No.<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="text" class="form-control" id="contact" tabindex="5">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="landline">Landline No.<span class = "required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="6" id="landline">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="language">Language<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <select multiple="multiple" class=" form-control 3col active" tabindex="7" id="language">
                                            <option>Hindi</option>
                                            <option>Marathi</option>
                                            <option>English</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="address">Address<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <textarea id="address" class="form-control" tabindex="5"></textarea>
                                    </div>
                                    <label class = "control-label col-md-2 col-sm-2" for="country">Country<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="6" id="country">
                                            <option>Select Country</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class = "control-label col-md-2 col-sm-2" for="country">State<span class = "required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="7" id="country">
                                            <option>Select State</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="city">City<span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-4">
                                        <select class="chosen-select" tabindex="8" id="city">
                                            <option>Select City</option>
                                            <option>Agartala</option>
                                            <option>Agatti</option>
                                            <option>Agra</option>
                                            <option>Ahemdabad</option>
                                            <option>Aizawal</option>
                                            <option>Allahbad</option>
                                            <option>Amritsar</option>
                                            <option>Anand</option>
                                            <option>Aurangabad</option>
                                            <option>Bagdogra</option>
                                            <option>Banglore</option>
                                            <option>Belgaum</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <form id="employee_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('employee/update') : site_url('employee/insert') ?>"> 
                            <input type="hidden" name="empid" value="<?php echo isset($employee['id']) ? $employee['id'] : ''; ?>">
                            <div class="row">
                                <div class="text-center">
                                    <h5 class="pull-left">Charges</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="name">Per Unit Charges<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="1" autofocus="on" id="name">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="code">Code<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="2" id="code">
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="role">Checking Charges<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="email" class="form-control" id="email" tabindex="4">
                                    </div>
                                    <label class="control-label col-md-2 col-sm-2" for="email">BT Charges<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="email" class="form-control" id="email" tabindex="4">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="contact">BT Checking Charges<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="text" class="form-control" id="contact" tabindex="5">
                                    </div>
                                    <label class = "control-label col-md-2 col-sm-2" for="landline">Advertising Charges<span class = "required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type = "text" class = "form-control" tabindex="6" id="landline">
                                    </div>
                                </div>
                            </div>

                            <div id="password_wrapper" class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2" for="pwd">Lump sum Charges<span class="required">*</span></label>
                                    <div class = "col-md-4 col-sm-4">
                                        <input type="password" class="form-control" id="pwd" tabindex="7" name="" >
                                    </div>

                                </div>
                            </div>


                        </form>
                    </div>
                    <div class = "row">
                        <div class = "col-md-12 pull-right">
                            <div class = "submit">
                                <button type = "submit" class = "btn btn-default submit-btn pull-right" tabindex="11">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src = "<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>

