<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h3>Search Writer</h3>
                </div>
                <div class="x_content search_content">
                    <form id="demo-form2" class="form-horizontal" action="<?php echo site_url('employee/entry_list'); ?>" method="GET">
                        <div class="form-group label_fields">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Writer's Name<span class="required">*</span></label>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Language<span class="required">*</span></label>
                        </div>
                        <div class="form-group select_fields">
                            <div class="col-md-2 col-sm-2">
                                <select class="chosen-select" tabindex="1" autofocus="on">
                                    <option value="NULL">Select Writer's Name</option>
                                    <option value="NULL">abc</option>
                                    <option value="NULL">xyz</option>
                                    <option value="NULL">pqr</option>
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <select multiple class="chosen-select" tabindex="2">
                                    <option value="NULL">Marathi</option>
                                    <option value="NULL">Hindi</option>
                                    <option value="NULL">English</option>
                                </select>
                            </div>
                            <a tabindex="5" class="btn btn-default submit-btn" title="Search Writer" href=""><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Search</a>
                            <a tabindex="6" class="btn btn-default view-btn" title="View All Writers" href=""><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;View All</a>
                            <a tabindex="7" class="btn btn-default add-btn"  title="Add Writer" href="<?php echo site_url('employee'); ?>"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add Writer</a>
                            <a tabindex="8" class="btn btn-default btn-info"  title="Print" target="_blank" href="<?php echo site_url('customer/writer_list'); ?>"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table_content">
                <h3>Writer List</h3>
                <div class="total">
                    <span>Total Records : 12</span>
                    <span>Total Display : 12</span>
                </div>
                <?php echo $this->session->flashdata('message'); ?>
                <?php echo $this->pagination->create_links(); ?>
                <table class="table table-bordered">
                    <thead>
                        <tr class="table_heading">
                            <th>Name</th>
                            <th>Code</th>
                            <th>Role</th>
                            <th>Email</th>
                            <th>Contact No.</th>
                            <th>Address</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>John Doe</td>
                            <td>1234</td>
                            <td>Staff</td>
                            <td>John@123.com</td>
                            <td>9968532400</td>
                            <td>Mumbai</td>
                            <td>
                                <a title="Update Writer" href=""><i class="fa fa-pencil fa-lg" aria-hidden="true"></i>&nbsp;Edit&nbsp;</a>
                                <a title="Delete Writer"href=""><i class="fa fa-trash fa-lg" aria-hidden="true"></i>&nbsp;Delete&nbsp;</a>
                            </td>
                        </tr> 
                    </tbody>
                </table> 
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
</div>

