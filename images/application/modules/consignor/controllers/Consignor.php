<?php

class Consignor extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $this->_display($view, $data);
    }
    function insert() {
        $this->load->helper('url');
        //echo '<pre>';
        //print_r($_POST);
        //exit;
        $this->load->model('Global_model', 'gm');
        $data = $this->input->post('consignor');
        $this->gm->insert('consignor', $data);        
        redirect(site_url('consignor/entry_list'));
    }

    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
         $this->load->model('Global_model', 'gm');
          $data['list'] = $this->gm->get_data_list('consignor', array(), array(), array('id' => 'desc'), '*', 30,array());
        $view = 'entry_list';
        $this->load->library('pagination');
        $this->_display($view, $data);
    }
    
    function entry_list_unbilled() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
         $this->load->model('Global_model', 'gm');
          $data['list'] = $this->gm->get_data_list('consignor', array(), array(), array('id' => 'desc'), '*', 30,array());
        $view = 'entry_list_unbilled';
        $this->load->library('pagination');
        $this->_display($view, $data);
    }
    
    function timeline_report() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
         $this->load->model('Global_model', 'gm');
          $data['list'] = $this->gm->get_data_list('consignor', array(), array(), array('id' => 'desc'), '*', 30,array());
        $view = 'timeline_report';
        $this->load->library('pagination');
        $this->_display($view, $data);
    }
    
    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }
    function job_billed(){
        $this->load->view('job_billed');
    }
    function timeline(){
        $this->load->view('timeline');
    }
}
