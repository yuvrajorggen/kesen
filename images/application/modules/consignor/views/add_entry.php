<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Consignor</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="consignor_add" class="form-horizontal add_form" method="post" action="<?php echo site_url('consignor/insert'); ?>">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-3">Name<span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="text" class="form-control" tabindex="1" autofocus="on" name="consignor[name]" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn">Save</button>
                                <input type="reset" value="Clear" class="btn btn-default clear-btn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[^-\s][a-zA-Z,""\s]+$/i.test(value);
        });
        /*
         * validation
         */
        $("#consignor_add").validate({
            rules: {
                'consignor[name]': {
                    required: true,
                    rangelength: [3, 20],
                    lettersonly: true
                }
            },
            messages: {
                'consignor[name]': {
                    required: "Name Required",
                    rangelength: "Minimum 3 & Maximum 40 Character Required",
                    lettersonly: "Enter Correct Name without space"
                }
            },
        });

    });
  </script>