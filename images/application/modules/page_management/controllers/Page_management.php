<?php

class Page_management extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
//echo 'dd';exit;
        $this->show_form();
    }

    function show_form() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        
        $this->load->library('user_agent');
        $data = array();
        $view = 'homepage';
        $this->_display($view, $data);
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

}
