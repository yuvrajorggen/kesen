<?php

class Employee extends MX_Controller {

    function __construct() {
        parent::__construct();
       // $this->output->enable_profiler(TRUE);       
        $this->load->helper('url');
        $sessiondata = $this->session->userdata('user');         
        if (isset($sessiondata) && is_array($sessiondata) && count($sessiondata) > 0) {
            if($sessiondata['id'] < 1 && $sessiondata['logged_in'] != 1 ){
                redirect(site_url());
            }
        }else{
            redirect(site_url());
        }
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $data['airlinelist'] = $this->gm->get_data_list('states', array(), array(), array('id' => 'desc'), 'name,id', 0, array());
        
        $view = 'add_entry';


        $this->_display($view, $data);
    }

    function insert() {
        $this->load->helper('url');
        //echo '<pre>';
        //print_r($_POST);
        //exit;
        $this->load->model('Global_model', 'gm');
        $sessiondata = $this->session->userdata('user');
        $data = $this->input->post('employee');
        
        $data['password'] = md5($data['password']);
        $data['created_by'] = $sessiondata['id'];
        $data['created_date'] = date('Y-m-d H:i:s');
        unset($data['confirmpassword']);
        $this->gm->insert('employee', $data);
        redirect(site_url('employee/entry_list'));
    }

    function entry_list() {

        $this->load->helper('url');
        $page = $this->uri->segment(3);


        if ($page == 0) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * 5;
        }
        if ($offset < 0) {
            $offset = 0;
        }
        $query = "SELECT * FROM `employee` WHERE status='1' ORDER BY `id`  DESC limit 5 offset " . $offset . "";
        $query_exec = $this->db->query($query);
        $result = $query_exec->result_array();



        $cquery = "SELECT count(id) as id FROM `employee` WHERE status='1' ";
        $cquery_exec = $this->db->query($cquery);
        $count = $cquery_exec->row_array();


        $data['list'] = $result;
        $data['total'] = $count['id'];
        $data['offset'] = $offset;


        $this->load->library('pagination');

        $config['base_url'] = site_url('employee/entry_list');
        $config['total_rows'] = $count['id'];

        $config['per_page'] = 5;
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        }
        $config['num_links'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination-custom">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = 'FIRST';
        $config['first_tag_open'] = '&nbsp;<span title="Go to first page">';
        $config['first_tag_close'] = '</span>';
        $config['last_link'] = 'LAST';
        $config['last_tag_open'] = '&nbsp;<span title="Go to last page">';
        $config['last_tag_close'] = '</span>';
        $config['next_link'] = 'NEXT';
        $config['next_tag_open'] = '&nbsp;<span title="Go to next page">';
        $config['next_tag_close'] = '</span>&nbsp;';
        $config['prev_link'] = 'PREVIOUS';
        $config['prev_tag_open'] = '&nbsp;<span title="Go to previous page">';
        $config['prev_tag_close'] = '</span>&nbsp;';
        $config['cur_tag_open'] = '&nbsp;<span class="current-page" title="Current page">';
        $config['cur_tag_close'] = '</span>&nbsp;';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $view = 'entry_list';
        $this->_display($view, $data);
    }

    function update() {

        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $sessiondata = $this->session->userdata('user');
        $data = array();
        $this->load->model('Global_model', 'gm');
        $id = $this->input->post('empid');
        $data = $this->input->post('employee');
        if($data['password']!=''){
        $data['password']=  md5($data['password']);
         }else{
         unset($data['password']);   
         }
        unset($data['confirmpassword']);   
        unset($data['check_password']); 
        $data['modified_by'] = $sessiondata['id'];
        $data['modified_date'] = date('Y-m-d H:i:s');
       
        $this->gm->update('employee', $data, $id);        
        $message = '<b>' . $data['firstname'] . ' ' . $data['lastname'] . ' ' . 'updated successfully' . '</b>';
        $this->session->set_flashdata('message', $message);
       
        redirect("employee/entry_list");
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }

    function edit() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $data['airlinelist'] = $this->gm->get_data_list('states', array(), array(), array('id' => 'desc'), 'name,id', 0,array());
        $data['employee'] = $this->gm->get_selected_record('employee', '*', $where = array('id' => $this->uri->segment(3)),array());
        $data['mode'] = "update";

        $view = 'add_entry';
        $this->_display($view, $data);
    }

    function delete($id) {

        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $employee = $this->gm->get_selected_record('employee', 'firstname,lastname', $where = array('id' => $id),array());
         $set=array(
            'modified_by' => $sessiondata['id'],
            'modified_date'=> date('Y-m-d H:i:s'),
            'status' => 2
        );
        $data['employee'] = $this->gm->update('employee',$set, $id);
        
        $message = '<b>' . $employee['firstname'] . ' ' . $employee['lastname'] . ' ' . 'deleted successfully' . '</b>';
        $this->session->set_flashdata('message', $message);

        redirect("employee/entry_list");
    }

    function get_city() {
        $this->load->helper('url');
        $this->load->model('Global_model', 'gm');
        $state_id = $this->input->post('state_id');
        $city_data = $this->gm->get_data_list('cities', array('state_id' => $state_id), array(), array('id' => 'desc'), 'id,name',0,array());
       
        echo json_encode($city_data);
    }

}
