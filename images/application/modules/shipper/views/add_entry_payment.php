<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Card Payment</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('odv/update') : site_url('odv/insert'); ?>">
                        <input type="hidden" name="odvid" value="<?php echo isset($odv['id']) ? $odv['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <h4>John Doe</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6">Bill Sent Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="3" readonly> 
                                        </div>
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-sm-offset-5">Bill No./Invoice No.<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="5" value="" name="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2">Job No.<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="2">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2">Informed To<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="4">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2">Invoice Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="birthday1" class="date-picker form-control col-md-7" required="required" type="text" tabindex="6" readonly> 
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn" tabindex="13">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });
    });

</script>
