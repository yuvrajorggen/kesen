<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Create Job Register</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo isset($mode) && $mode == 'update' ? site_url('odv/update') : site_url('odv/insert'); ?>">
                        <input type="hidden" name="odvid" value="<?php echo isset($odv['id']) ? $odv['id'] : ''; ?>">
                        <div class="row">
                            <div class="text-center">
                                <p class="note">The fields marked as <span>*</span> are mandatory.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <p class="note2">NOTE : Job Card No. will be automatically created.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="clientname" class="control-label col-md-2 col-sm-2 col-sm-offset-6" form="client">Client Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select id="clientname" class="chosen-select" tabindex="1" autofocus="on" id="client">
                                                <option value="NULL">Select Client Name</option>
                                                <option value="NULL">abc</option>
                                                <option value="NULL">xyz</option>
                                                <option value="NULL">pqr</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="clientname" class="control-label col-md-2 col-sm-2 col-sm-offset-6" form="client">Client Contact Person Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select id="clientname" class="chosen-select" tabindex="1" autofocus="on" id="client">
                                                <option value="NULL">Select Client Contact Person Name</option>
                                                <option value="NULL">abc</option>
                                                <option value="NULL">xyz</option>
                                                <option value="NULL">pqr</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="handled">Handled By<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="chosen-select" tabindex="3" id="handled">
                                                <option>Select Handled By</option>
                                                <option>Agartala</option>
                                                <option>Agatti</option>
                                                <option>Agra</option>
                                                <option>Ahemdabad</option>
                                                <option>Aizawal</option>
                                                <option>Allahbad</option>
                                                <option>Amritsar</option>
                                                <option>Anand</option>
                                                <option>Aurangabad</option>
                                                <option>Bagdogra</option>
                                                <option>Banglore</option>
                                                <option>Belgaum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-3 col-sm-3 col-sm-offset-5" for="writer">Writer's Name<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="chosen-select" tabindex="5" id="writer">
                                                <option>Select Writer's Name</option>
                                                <option>Agartala</option>
                                                <option>Agatti</option>
                                                <option>Agra</option>
                                                <option>Ahemdabad</option>
                                                <option>Aizawal</option>
                                                <option>Allahbad</option>
                                                <option>Amritsar</option>
                                                <option>Anand</option>
                                                <option>Aurangabad</option>
                                                <option>Bagdogra</option>
                                                <option>Banglore</option>
                                                <option>Belgaum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-sm-offset-6" for="details">Other Details<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <textarea class="form-control" tabindex="9" id="details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row">
                                    <div class="form-group ">
                                        <label class="control-label col-md-2 col-sm-2" for="language">Language<span class="required">*</span></label>
                                        <div class="col-md-4 col-sm-4">
                                            <select multiple="multiple" class=" form-control 3col active" tabindex="4" id="language">
                                                <option value="NULL">Marathi</option>
                                                <option value="NULL">Hindi</option>
                                                <option value="NULL">English</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="estimate">Estimate No. <span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <select class="chosen-select" tabindex="4" id="language">
                                                <option value="NULL">Select Estimate No.</option>
                                                <option value="NULL">KLS</option>
                                                <option value="NULL">KLB</option>
                                                <option value="NULL">KCP</option>
                                                <option value="NULL">LGS</option>
                                                <option value="NULL">FC</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" tabindex="14" id="billno">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Date<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input id="date" class="date-picker form-control col-md-7" required="required" type="text" tabindex="8" name="airwaybill_master[effectivedate]"  <?php echo isset($mode) && $mode == 'update' ? 'disabled' : ''; ?> readonly> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Headline<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="14" id="billno">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2" for="date">Protocol No.<span class="required">*</span>
                                        </label>
                                        <div class="col-md-4 col-sm-4">
                                            <input type="text" class="form-control" tabindex="14" id="billno">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn" tabindex="10">Create Job Card</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH_FRONTEND; ?>jquery.multiselect.css">
<script src="<?php echo JS_PATH_FRONTEND; ?>jquery.multiselect.js"></script>
<script>
    $(document).ready(function () {
        $('.date-picker').daterangepicker({
            //"singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "showISOWeekNumbers": true,
            "autoApply": true,
            "alwaysShowCalendars": true,
            "calender_style": "picker_4",
        });

        $.validator.addMethod("lettersdigitonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z0-9,""\s]+$/i.test(value);
        });
        $.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z,""\s]+$/i.test(value);
        });
        /*
         * validation
         */
        $("#odv_add").validate({
            messages: {
                'odv[name]': {
                    required: "Name Required",
                    rangelength: "Minimum 3 & Maximum 40 Character Required",
                    lettersonly: "Enter Correct Name"
                },
                'odv[pincode]': {
                    required: "Pincode Required",
                    rangelength: "Enter Correct Pincode",
                    digits: "Only Digits"
                },
                'odv[code]': {
                    required: "Pincode Required",
                    rangelength: "Minimum 3 & Maximum 4 Character Required",
                    lettersdigitonly: "Enter Correct code"
                }
            },
        });
    });

</script>
