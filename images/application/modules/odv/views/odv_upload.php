<div class="form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Upload Origin, Destination / Via.</h2>
                </div>
                <div class="x_content">
                    <br>
                    <form id="odv_add" class="form-horizontal add_form" method="post" action="<?php echo site_url('odv/odv_upload'); ?>" enctype="multipart/form-data">>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-sm-offset-3">Upload CSV:<span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2">
                                    <input type="file" class="form-control" tabindex="1" autofocus="on" name="userfile">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit">
                                <button type="submit" class="btn btn-default submit-btn">Submit</button>                                
                            </div>
                    </form>
                </div>                        
            </div>
        </div>
    </div>
</div>
</div>


