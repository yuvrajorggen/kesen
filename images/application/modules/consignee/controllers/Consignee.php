<?php

class Consignee extends MX_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->entry();
    }

    function entry() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $data = array();
        $view = 'add_entry';
        $this->_display($view, $data);
    }
    
    function insert() {
        $this->load->helper('url');

        $this->load->model('Global_model', 'gm');
        $data = $this->input->post('consignee');

        $this->gm->insert('consignee', $data);
        redirect(site_url('consignee/entry_list'));
    }
    
    function entry_list() {
        $this->load->helper('url');
        $this->load->helper('frontend_common_helper');
        $this->load->library('user_agent');
        $this->load->model('Global_model', 'gm');
        $data = array();
        $this->load->library('pagination');
        $data['consigneelist'] = $this->gm->get_data_list('consignee', array(), array(), array('id' => 'desc'), '*', 30,array());
        $view = 'entry_list';
        $this->_display($view, $data);
    }

    function _display($view, $data) {
        $this->load->view('frontend_header');
        $this->load->view($view, $data);
        $this->load->view('frontend_footer');
    }
    
    function payment_card(){
        $this->load->view('payment');
    }
    
    function error(){
        $this->load->helper('url');
          $data = array();
        $this->_display('error', $data);
       
    }
}
