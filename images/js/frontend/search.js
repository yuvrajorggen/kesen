$(document).ready(function(){
    $("#category_searched").keyup(function(){
 
        // Retrieve the input field text and reset the count to zero
        var category_searched = $(this).val(), count = 0;
 
        // Loop through the comment list
        $("#categories div").each(function(){
 
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(category_searched, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
 
        // Update the count
        var numberItems = count;
       // $("#filter-count").text("Number of Results = "+count);
    });
});